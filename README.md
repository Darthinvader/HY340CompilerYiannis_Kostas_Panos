#hy340 Project

## Introduction
This repository is the result of a collaborative effort
for the mandatory project in course CS-340 of CSD-UoC.

## Team
We are:

* Constantine Kalivas **3203** <csd3203@csd.uoc.gr>
* Yiannis-Christos Psaradakis **3294** <csd3294@csd.uoc.gr>
* Panos Koufopoulos **3306** <csd3306@csd.uoc.gr> 

## Log

- [x] Phase 1 <10-03-17> 8b068c
- [x] Phase 2 <03-27-17> dc28e3
- [x] Phase 3 <14-05-17> e9ce79
- [x] Phase 4-5 <27-05-17> Ongoing