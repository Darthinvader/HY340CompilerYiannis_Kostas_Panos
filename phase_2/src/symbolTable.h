#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum idflag {
	user_func,lib_func,local_var,glob_var,formal_var
} idflag_t;

static const char *idflag_str[] = {
	"user function", "lib function", "local var", "global var", "formal_var"
};

typedef struct node {
	char *name;
	idflag_t id_type;
	struct node *formals;
	int line;
	int scope;
	int active;
	struct node *next;
	struct node *scopeNext;
} node_t;

typedef struct symtable {
	long curr_scope, max_scope, alloc_scope;
	long curr_nest, alloc_nest;
	node_t **hash_tbl;
	node_t **scope_lst;
	long *func_scope;
} symtable_t;

symtable_t* init_symtable();
void free_symtable(symtable_t *st);

void deact_scope(symtable_t *st,int scope);
void advance_scope(symtable_t *st);
void retract_scope(symtable_t *st);
void allocate_scope(symtable_t *st);

long peek_nestscope(symtable_t *st);
void push_nestscope(symtable_t *st);
void pop_nestscope(symtable_t *st);
void allocate_nestscope(symtable_t *st);

void print_node(node_t *node);
void print_all_nodes(symtable_t *st);

void init_libfunc(symtable_t *st);
int is_lib_func(symtable_t *st,char *name);

node_t* create_node(char *name,idflag_t id_type,node_t *formals,int line,int scope,int active);
void free_node(node_t *node);

void ins_node(symtable_t *st,node_t *node);
node_t* add_node(symtable_t *st,char *name,idflag_t id_type,node_t *formals,int line,int scope,int active);

node_t* deact_node(symtable_t *st,char *name,int scope);
node_t* add_formal_lst(node_t *formals,node_t *formal);
node_t* find_node(symtable_t *st,char* name,int scope);
node_t* find_node_fromscope(symtable_t *st,char* name,int currScope);

int hash_fun(char *str);
