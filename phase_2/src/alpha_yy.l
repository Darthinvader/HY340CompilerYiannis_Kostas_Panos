%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "symbolTable.h"
#include "alpha_yy.tab.h"

#define STRBUF_INITSZ 32

int numberOfToken = 0;
char *str_buf;
size_t str_sz;
uint8_t str_valid;
int nested_ctr = 0;

static inline void add_to_buf(char c) {
	if(((str_sz+1) % STRBUF_INITSZ == 1) && ((str_sz+1) / STRBUF_INITSZ > 0)){
		// double str_buf size as needed
		str_buf = realloc(str_buf, STRBUF_INITSZ*(1+(str_sz / STRBUF_INITSZ)));
	}
	str_buf[str_sz++] = c;
	return;
}

%}

integer [0-9]+
/* finds integers (1,2,4,32 etc.) [0-9] means it find one number between 0-9 the plus on the end means it finds a continuous string of 0-9 numbers(eg. 1245642343223414) */
real    {integer}\.{integer}
/* finds a real number (3.14159) */
id      [a-zA-Z][a-zA-Z_0-9]*
/* finds a string that contains as the first character a letter and all the other numbers and letter */
space   [\r \t\v\n]
/* finds all spaces and tabs and newlines */
/* alpha_yytext is the current text */
/* alpha_yyleng is the current text length */
/* alpha_yylineno is the currnt line number */
%option prefix="alpha_yy"
%option yylineno
%option noyywrap

%x string
%x comment_sl
%x comment_ml

%%

if       {return IF;}
else     {return ELSE;}
while    {return WHILE;}
for      {return FOR;}
function {return FUNC;}
return   {return RT;}
break    {return BRK;}
continue {return CNT;}
and      {return AND;}
not      {return NOT;}
or       {return OR;}
local    {return LC;}
true     {return T;}
false    {return F;}
nil      {return NIL;}

"="  {return ASS;}
"+"  {return PLUS;}
"-"  {return MIN;}
"*"  {return MUL;}
"/"  {return DIV;}
"%"  {return MOD;}
"==" {return EQ;}
"!=" {return NEQ;}
"++" {return INC;}
"--" {return DEC;}
">=" {return GTEQ;}
">"  {return GT;}
"<=" {return LTEQ;}
"<"  {return LT;}

"{"  {return OPBRACE;}
"}"  {return CLBRACE;}
"["  {return OPBRACKET;}
"]"  {return CLBRACKET;}
"("  {return OPPAR;}
")"  {return CLPAR;}
";"  {return SEMCOL;}
","  {return COMMA;}
"::" {return GLBOP;}
":"  {return COLON;}
".." {return DDOT;}
"."  {return DOT;}

{integer} { alpha_yylval.intVal = atoi(yytext); return INT; }
{real}    { alpha_yylval.floatVal = atof(yytext); return REAL; }
{id}      { alpha_yylval.strVal = strdup(yytext); return ID;}
{space}   {}

"\"" {
	str_buf = malloc(STRBUF_INITSZ*sizeof(char));
	str_sz = 0;
	str_valid = 1;
	BEGIN(string);
}

<string>{
	"\"" {
		add_to_buf('\0');
		BEGIN(INITIAL);
		if(str_valid) {
			alpha_yylval.strVal = str_buf;
			return STR;
		}
		free(str_buf);
	}

	"\n" {
		fprintf(stderr, "al: found newline before end of string\n");
		free(str_buf);
		BEGIN(INITIAL);
	}

	"\\\"" {
		add_to_buf('\"');
	}

	"\\n" {
		add_to_buf('\n');
	}

	"\\\\" {
		add_to_buf('\\');
	}

	"\\t" {
		add_to_buf('\t');
	}

	"\\". {
		fprintf(stderr, "al: warning: illegal escape sequence \\%c\n", yytext[1]);
		add_to_buf('\\');
		add_to_buf(yytext[1]);
		str_valid = 0;
	}

	[^\\\n\"]+ {
		char *yptr = alpha_yytext;
		while(*yptr)
			add_to_buf(*yptr++);
	}

	<<EOF>> {
		fprintf(stderr, "al: warning: EOF before end of string");
		free(str_buf);
		BEGIN(INITIAL);
		return 0;
	}
}

"//" {
	BEGIN(comment_sl);
}

<comment_sl>{
	[^\n] {}

	"\n" {
		BEGIN(INITIAL);
	}

	<<EOF>> { BEGIN(INITIAL); return 0; }
}

"/*" {
	nested_ctr = 1;
	BEGIN(comment_ml);
}

<comment_ml>{
	"/*" { nested_ctr++; }

	"*/" {
		if(--nested_ctr == 0){
			BEGIN(INITIAL);
		}
	}

	. {} /* Consume everything else */

	<<EOF>> { BEGIN(INITIAL); return 0; }
}

<<EOF>> { return 0; }

. {} /* TODO: No error reporting here. */

%%
