#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "symbolTable.h"
#include "lex.alpha_yy.h"
#include "alpha_yy.tab.h"

void print_usage(const char *exec_name) {
	fprintf(stderr, "usage: %s [option] input-file\n", exec_name);
	fprintf(stderr, "\t -o output-file\tWrite parser output to file\n");
}

int main(int argc, char *argv[]) {
	int retval;
	const char *exec_name = *(argv++);
	int inputf_set=0, outputf_set=0;
	

	for(int i=0; i<argc-1; i++) {
		if(strlen(argv[i]) == 2 && (argv[i][0] == '-')) {
			switch(argv[i][1]) {
				case 'o':
					if(outputf_set) {
						fprintf(stderr, "%s: duplicate -o argument\n", exec_name);
						print_usage(exec_name);
						if(inputf_set) fclose(alpha_yyin);
						fclose(alpha_yyout);
						return EXIT_FAILURE;
					}

					if(argv[i+1] != NULL) {
						if((alpha_yyout = fopen(argv[++i], "w")) == NULL) {
							perror(exec_name);
							return EXIT_FAILURE;
						}
						outputf_set = 1;
					} else {
						fprintf(stderr, "%s: argument to -o is missing\n", exec_name);
						print_usage(exec_name);
						return EXIT_FAILURE;
					}
					break;
				default:
					fprintf(stderr, "%s: Invalid argument %s\n", exec_name, argv[i]);
					print_usage(exec_name);
					if(inputf_set) fclose(alpha_yyin);
					if(outputf_set) fclose(alpha_yyout);
					return EXIT_FAILURE;
					break;
			}
		} else {
			if(inputf_set) {
				fprintf(stderr, "%s: duplicate input file argument\n", exec_name);
				print_usage(exec_name);
				if(outputf_set) fclose(alpha_yyout);
				fclose(alpha_yyin);
				return EXIT_FAILURE;
			}

			if((alpha_yyin = fopen(argv[i], "r")) == NULL) {
				perror(exec_name);
				if(outputf_set) fclose(alpha_yyout);
				return EXIT_FAILURE;
			}
			inputf_set = 1;
		}
	}

	symtable_t *st = init_symtable();

	if(!inputf_set) alpha_yyin = stdin;
	if(!outputf_set) alpha_yyout = stdout;
	retval = alpha_yyparse(st);
	if(inputf_set) fclose(alpha_yyin);
	if(outputf_set) fclose(alpha_yyout);

	print_all_nodes(st);

	free_symtable(st);
	return retval;
}
