#include "symbolTable.h"

#define HASH_TBLSZ 6869
#define SCOPE_LSTSZ 50
#define NESTSCOPE_TBLSZ 10

symtable_t* init_symtable() {
	symtable_t *st =  malloc(sizeof(symtable_t));
	st->hash_tbl = malloc(HASH_TBLSZ*sizeof(node_t*));
	for(size_t i=0; i<HASH_TBLSZ; i++) st->hash_tbl[i] = NULL;
	st->scope_lst = malloc(SCOPE_LSTSZ*sizeof(node_t*));
	for(size_t i=0; i<SCOPE_LSTSZ; i++) st->scope_lst[i] = NULL;
	st->func_scope = malloc(NESTSCOPE_TBLSZ*sizeof(int));

	st->curr_scope = 0;
	st->max_scope = 0;
	st->alloc_scope = SCOPE_LSTSZ;

	st->curr_nest = 0;
	st->alloc_nest = NESTSCOPE_TBLSZ;
	
	init_libfunc(st);

	return st;
}

void free_symtable(symtable_t *st) {
	node_t *node, *tmp;
	for(long i=0; i<st->alloc_scope; i++) {
		node = st->scope_lst[i];
		while(node != NULL) {
			tmp = node;
			node = node->scopeNext;
			free_node(tmp);
		}
	}

	free(st->hash_tbl);
	free(st->scope_lst);
	free(st->func_scope);
	free(st);
}

node_t* create_node(char *name,idflag_t id_type,node_t *formals,int line,int scope,int active){
	node_t *node = malloc(sizeof(node_t));
	node->name = name;
	node->id_type = id_type;
	node->formals = formals;
	node->line = line;
	node->scope = scope;
	node->active = active;
	node->next = NULL;
	node->scopeNext = NULL;
	return node;
}

void free_node(node_t *node) {
	free(node->name);
	free(node);
}

void ins_node(symtable_t *st,node_t *node){
	int hash = hash_fun(node->name);
	node->next = st->hash_tbl[hash];
	node->scopeNext = st->scope_lst[node->scope];
	st->hash_tbl[hash] = node;
	st->scope_lst[node->scope] = node;
	return;
}

node_t* add_node(symtable_t *st,char *name,idflag_t id_type,node_t *formals,int line,int scope,int active){
	node_t *node = create_node(name, id_type, formals, line, scope, active);
	ins_node(st, node);
	return node;
}

node_t* find_node(symtable_t *st,char* name,int scope){
	int hash = hash_fun(name);
	node_t *node = st->hash_tbl[hash];
	while(node!=NULL){
		if(strcmp(node->name,name) ==0 && (node->scope == scope) && node->active == 1){
			return node;
		}
		node = node->next;
	}
	return NULL;
}
node_t *find_node_fromscope(symtable_t *st,char* name,int scope){
	node_t *curr =NULL;
	for(int i=scope;i>=0;i--){
		curr = find_node(st,name,i);
		if(curr!=NULL) break;
	}
	return curr;
}
node_t *deact_node(symtable_t *st,char *name,int scope){
	int hash = hash_fun(name);
	node_t *node = st->hash_tbl[hash];
	while(node!=NULL){
		if(strcmp(node->name,name) == 0 && node->scope == scope && node->active == 1){
			printf("%i",(*node).scope);fflush(stdout);
			node->active = 0;
			return node;
		}
		node = (*node).next;
	}
	return node;
}
int hash_fun(char *str){
    unsigned long hash = 5381;
    int c;
    while ((c = *(str++))){
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
	}
	hash = hash%HASH_TBLSZ;
    return hash;
}
void print_node(node_t *node) {
	printf("[name: %s, id_type: %s, ln: %i]\n", node->name, idflag_str[node->id_type], node->line);
	return;
}

void advance_scope(symtable_t *st) {
	if(st->curr_scope+1 > st->max_scope) st->max_scope++;
	if(++(st->curr_scope) >= st->alloc_scope) allocate_scope(st);
}

void retract_scope(symtable_t *st) {
	if(st->curr_scope > 0) st->curr_scope--;
}

void allocate_scope(symtable_t *st) {
	node_t **tmp = realloc(st->scope_lst, (HASH_TBLSZ+st->alloc_scope)*sizeof(node_t*));
	if(!tmp) ; // TODO:handle error
	else {
		for(size_t i=0; i<HASH_TBLSZ; i++) tmp[st->alloc_scope+i] = NULL;
		st->scope_lst = tmp;
		st->alloc_scope += HASH_TBLSZ;
	}
}

void deact_scope(symtable_t *st,int scope){
	node_t *node = st->scope_lst[scope];
	while(node){
		node->active = 0;
		node = node->scopeNext;
	}
}

node_t *add_formal_lst(node_t *formals,node_t *formal){
	formal->formals = formals;
	return formal;
};

void print_all_nodes(symtable_t *st){
	node_t *node;
	printf("Max Scope was %li\n", st->max_scope);
	for(int i=0;i<=st->max_scope;i++){
		printf("Scope %i\n", i);
		node = st->scope_lst[i];
		while(node!=NULL){
			printf("\t");
			print_node(node);
			node = node->scopeNext;
		}
		printf("\n");
	}
}

long peek_nestscope(symtable_t *st) {
	return st->curr_nest>0 ? st->func_scope[st->curr_nest-1] : 0;
}

void push_nestscope(symtable_t *st) {
	if(st->curr_nest+1 >= st->alloc_nest) allocate_nestscope(st);
	st->func_scope[++st->curr_nest-1] = st->curr_scope;
}

void pop_nestscope(symtable_t *st) {
	if(st->curr_nest > 0) st->curr_nest--;
}

void allocate_nestscope(symtable_t *st) {
	long *tmp = realloc(st->func_scope, (NESTSCOPE_TBLSZ+st->alloc_nest)*sizeof(long));
	if(!tmp) ; // TODO:handle error
	else {
		st->func_scope = tmp;
		st->alloc_nest += NESTSCOPE_TBLSZ;
	}
}

int is_lib_func(symtable_t *st,char *name){
	node_t *curr = find_node(st,name,0);
	if(curr!=NULL &&(curr->id_type == lib_func)){
		return 1;
	}
	else{
		return 0;
	}
}

void init_libfunc(symtable_t *st){
	add_node(st,strdup("print"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("input"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("objectmemberkeys"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("objecttotalmembers"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("objectcopy"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("totalarguments"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("argument"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("typeof"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("strtonum"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("sqrt"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("cos"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("sin"),lib_func,NULL,-1,0,1);
}



/*



int main(){
	Node *node = CreateAndInsert("silver",localVariable,NULL,1,1,1);
	int hash = hashFunction("silver");
	printNode(node);
	printNode(hashTable[hash]);
	Node *node2 = CreateAndInsert("silver",localVariable,NULL,1,2,1);
	hashTable[hash] = node2;
	Node *Nodeformal = CreateAndInsert("sk",formalVariable,NULL,1,1,1);
	Node *Nodeformal2 = CreateAndInsert("ska",formalVariable,NULL,1,1,1);
	addToFormalList(Nodeformal,Nodeformal2);
	Node *noder = Nodeformal2->formals;
	printf("%s",(*noder).name);
	deactivate("silver",1);
	FindNode("silver",1);
	printf("%s", node->name);
	printf("%i",node->idType);
	printf("%i",node->line);
	printf("%i",node->scope);
	printf("%i",node->active);
	return 0;

}*/
