
%{ /*User Code	*/
	#include <stdio.h>
	#include <stdlib.h>
	#include <assert.h>
	#include <string.h>
	#include "symbolTable.h"
	#include "lex.alpha_yy.h"

	char dfn[3] = "_fa";
	
	int yyerror(symtable_t *st, char *error_str);
%}

%define api.prefix {alpha_yy}

%parse-param {symtable_t *st}

%union {
	int intVal;
	float floatVal;
	char *strVal;
	node_t *symbol;
}
%token IF
%token ELSE
%token WHILE
%token FOR
%token FUNC
%token RT
%token BRK
%token CNT
%token AND
%token OR
%token NOT
%token LC
%token T
%token F
%token NIL
%token ASS
%token PLUS
%token MIN
%token MUL
%token DIV
%token MOD
%token EQ
%token NEQ
%token INC
%token DEC
%token GT
%token LT
%token GTEQ
%token LTEQ
%token OPBRACE
%token CLBRACE
%token OPBRACKET
%token CLBRACKET
%token OPPAR
%token CLPAR
%token SEMCOL
%token COMMA
%token GLBOP
%token COLON
%token DDOT
%token DOT

%token <intVal> 	INT
%token <floatVal> 	REAL
%token <strVal> 	STR ID

%type <symbol>		lvalue
%start program

%right ASS
%left OR
%left AND
%nonassoc EQ NEQ
%nonassoc GT GTEQ LT LTEQ
%left PLUS MIN
%left MUL DIV MOD
%right NOT INC DEC UMIN
%left DOT DDOT
%left OPBRACKET CLBRACKET
%left OPPAR CLPAR


%%
program: statements { printf("program->stmt statements\n");}
         ;

statements:                   {printf("statements -> empty \n");}
            | stmt statements {printf("statements -> stmt \n");}
            ;

stmt:   expr SEMCOL {printf("stmt->expr \n");}
      | ifstmt      {printf("stmt -> ifstmt \n");}
      | whilestmt   {printf("stmt-> whilestmt \n");}
      | forstmt     {printf("stmt -> forstmt \n");}
      | returnstmt  {printf("stmt ->returnstmt");}
      | BRK SEMCOL  {printf("stmt -> Break \n	");}
      | CNT SEMCOL  {printf("stmt -> Continue \n");}
      | block       {printf("stmt ->block \n");}
      | funcdef     {printf("stmt -> funcdef \n");}
      | SEMCOL      {printf("stmt -> ;\n");}
      ;

expr:   assignexpr   {printf("expr - > assignexpr \n");}
      | expr op term {printf("expr - >expr op expr \n");}
      | term         {printf("expr -> term \n");}
      ;

op:   PLUS {printf("expr->+ \n");}
    | MIN  {printf("op -> - \n");}
    | MUL  {printf("op -> *");}
    | DIV  {printf("op -> / \n");}
    | MOD  {printf("op -> %% \n");}
    | GT   {printf("op-> > \n");}
    | GTEQ {printf("op-> >= \n");}
    | LT   {printf("op -> < \n");}
    | LTEQ {printf("op -> <= \n");}
    | EQ   {printf("op -> == \n");}
    | NEQ  {printf("op -> != \n");}
    | AND  {printf("op -> and \n");}
    | OR   {printf("op -> or \n");}
    ;

term:     OPPAR expr CLPAR      {printf("term -> (expr) \n");}
        | MIN expr %prec UMIN {printf("term -> -expr \n");}
        | NOT expr            {printf("term -> not expr \n");}
        | INC lvalue {
        	printf("term -> ++lvalue \n");
        	if ($2->id_type == user_func || $2->id_type == lib_func){
        		char errorStr[63 + strlen($2->name)];
        		sprintf(errorStr, "error cannot increment user or library function with name %s \n",$2->name);
        		alpha_yyerror(NULL,errorStr);
        		//printf("error cannot increment user or library function at line %i with name %s",alpha_yylineno,$2->name);
        	}
        }

        | lvalue INC {
        	printf("term -> lvalue++ \n");
        	if ($1->id_type == user_func || $1->id_type == lib_func){
        		char errorStr[63 + strlen($1->name)];
        		sprintf(errorStr, "error cannot increment user or library function with name %s \n",$1->name);
        		alpha_yyerror(NULL,errorStr);
        		//printf("error cannot increment user or library function at line %i with name %s",alpha_yylineno,$1->name);
        	}
        }

        | DEC lvalue {
        	printf("term -> --lvalue \n");
        	if ($2->id_type == user_func || $2->id_type == lib_func){
        		char errorStr[63 + strlen($2->name)];
        		sprintf(errorStr, "error cannot decrement user or library function with name %s \n",$2->name);
        		alpha_yyerror(NULL,errorStr);
        		//printf("error cannot decrement user or library function at line %i with name %s",alpha_yylineno,$2->name);
        	}
        }

        | lvalue DEC {
        	printf("term -> lvalue-- \n");
        	if ($1->id_type == user_func || $1->id_type == lib_func){
        		char errorStr[63 + strlen($1->name)];
        		sprintf(errorStr, "error cannot decrement user or library function with name %s \n",$1->name);
        		alpha_yyerror(NULL,errorStr);
        		//printf("error cannot decrement user or library function at line %i with name %s",alpha_yylineno,$1->name);
        	}
        }

        | primary {printf("term -> primary \n");}
        ;

assignexpr: lvalue ASS expr {
            	printf("assignexpr -> lvalue = expr \n");
            	if ($1->id_type == user_func || $1->id_type == lib_func){
            		char errorStr[61 + strlen($1->name)];
            		sprintf(errorStr, "error cannot assign user or library function with name %s\n",$1->name);
            		alpha_yyerror(NULL,errorStr);
            		//printf("error cannot decrement user or library function at line %i with name %s",alpha_yylineno,$1->name);
            	}
            }
            ;

primary: lvalue                {printf("primary -> lvalue \n");}
         | call                {printf("primary -> call \n");}
         | objectdef           {printf("primary -> objectdef\n");}
         | OPPAR funcdef CLPAR {printf("primary -> (funcdef)\n");}
         | const               {printf("primary -> const\n");}
         ;

lvalue: ID {
        	printf("lvalue -> id \n");
        	node_t *node = find_node_fromscope(st,$1,st->curr_scope);
        	if(node==NULL){
        		idflag_t type = local_var;
        		if(st->curr_scope == 0) type = glob_var;
        		node = add_node(st,$1,type,NULL,alpha_yylineno,st->curr_scope,1);
        	}
        	else if(node->scope!=0 && node->scope<peek_nestscope(st)){
        		char errorStr[21 + strlen($1)];
        		sprintf(errorStr, "ID %s is out of scope\n",$1);
        		alpha_yyerror(NULL,errorStr);
        		//TODO print error with alpha_yyeror we are going to create
        	}
        	$$ = node;
        }

        | LC ID {
        	printf("lvalue -> local id \n");
        	if(is_lib_func(st,$2)){ //check for libfunc
        		char errorStr[30 + strlen($2)];
        		sprintf(errorStr, "ID %s is in use by lib_func\n",$2);
        		alpha_yyerror(NULL,errorStr);
        	}
        	else if(!($$ = find_node(st,$2,st->curr_scope))){
        		$$ = add_node(st,$2,local_var,NULL,alpha_yylineno,st->curr_scope,1);
        	}
        }

        | GLBOP ID {
        	printf("lvalue -> ::id \n");

        	if(!($$ = find_node(st,$2,0))){
        		char errorStr[21 + strlen($2)];
        		sprintf(errorStr, "ID %s is out of scope\n",$2);
        		alpha_yyerror(NULL,errorStr);
        		//TODO print error with alpha_yyerror
        	}
        }

        | member {
        	// Properly handle not-checking something.id cases
        	printf("lvalue ->member \n");
        	$$ = add_node(st,strdup("_dummy_member"),local_var, NULL,0,st->curr_scope,0);
        }
        ;

member:   lvalue DOT ID                   {printf("member -> lvalue.id \n");}
        | lvalue OPBRACKET expr CLBRACKET {printf("member -> lvalue[expr] \n");}
        | call DOT ID                     {printf("member -> call.id\n");}
        | call OPBRACKET expr CLBRACKET   {printf("member -> call[expr] \n");}
        ;

call:   call OPPAR elist CLPAR                {printf("call -> call(elist) \n");}
      | lvalue callsufix                      {printf("call -> lvalue callsufix \n");}
      | OPPAR funcdef CLPAR OPPAR elist CLPAR {printf("call -> (funcdef)(elist) \n");}
      ;

callsufix:   normcall  {printf("callsufix->normcall \n");}
           | methodcal {printf("callsufix -> methodcal \n");}
           ;

normcall: OPPAR elist CLPAR {printf("normcall -> (elist)\n");}
          ;

methodcal: DDOT ID OPPAR elist CLPAR {printf("methodcal -> ..id(elist) \n");}
           ;

elist:                    { printf("elist->{0}\n");}
       | expr elistoption { printf("elist->expr elistoption *\n"); }
       ;

elistoption:                          { printf("elistoption->{0}\n");}
             | COMMA expr elistoption { printf("elistoption->,expr elistoption\n");}
             ;

objectdef:   OPBRACKET elist CLBRACKET   {printf("objectdef -> [elist] \n");}
           | OPBRACKET indexed CLBRACKET {printf("objectdef -> [indexed]");}
           ;

indexed: indexedelem indexoption {printf("indexed->indexedelem indexoption \n");}
         ;

indexoption:                                 {printf("indexoption -> 0 \n");}
             | COMMA indexedelem indexoption {printf("indexoption -> ,indexedelem indexoption \n");}
             ;

indexedelem: OPBRACE expr COLON expr CLBRACE {printf("indexedelem -> {expr:expr} \n");}
             ;

block:  OPBRACE { advance_scope(st); }
        statements CLBRACE {
        	printf("block -> [stmt]");
        	deact_scope(st,st->curr_scope);
        	retract_scope(st);
        }
        ;

funcdef: FUNC funcdefoptions OPPAR {
         	advance_scope(st);
         	push_nestscope(st);
         }

         idlist CLPAR {
         	retract_scope(st);
         }
         
         block {
         	pop_nestscope(st);
         	printf("funcdef->function[id](idlist)block \n");
         }

funcdefoptions: {
                	printf("funcdefoptions -> 0 \n"); /*TODO More shit*/
                	add_node(st,strdup(dfn),user_func,NULL,alpha_yylineno,st->curr_scope,1);
                	dfn[2]++;
                }

                | ID {
                	printf("funcdefoptions -> id \n");
                	node_t *node = find_node(st,$1,st->curr_scope);
                	if(node!=NULL){
                		char errorStr[15 + strlen($1)];
                		sprintf(errorStr, "ID %s is in use\n",$1);
                		alpha_yyerror(NULL,errorStr);
                	}
                	else if(is_lib_func(st,$1)){//not a lib func TODOisalibfunc
                		//errors
                		char errorStr[30 + strlen($1)];
                		sprintf(errorStr, "ID %s is in use by lib_func\n",$1);
                		alpha_yyerror(NULL,errorStr);
                	}
                	else{
                		add_node(st,$1,user_func,NULL,alpha_yylineno,st->curr_scope,1);
                	}
                }
                ;

const:   INT  {printf("const->INT \n");}
       | REAL {printf("const -> REAL");}
       | STR  {printf("const->string \n");}
       | NIL  {printf("const -> nil \n");}
       | T    {printf("const -> T \n");}
       | F    {printf("const->F \n");}
       ;

idlist: {printf("idlist->0 \n");/*to add shit here*/}

        | ID idlistoption {
        	printf("idlist-> ID idlist \n");
        	node_t *node = find_node(st,$1,st->curr_scope);
        	if(node != NULL){
        		char errorStr[15 + strlen($1)];
        		sprintf(errorStr, "ID %s is in use\n",$1);
        		alpha_yyerror(NULL,errorStr);
        	}
        	else if(is_lib_func(st,$1)){//TODOisalibfunc
        		//error
        		char errorStr[30 + strlen($1)];
        		sprintf(errorStr, "ID %s is in use by lib_func\n",$1);
        		alpha_yyerror(NULL,errorStr);
        	}
        	else{
        		add_node(st,$1,formal_var,NULL,alpha_yylineno,st->curr_scope,1);
        	}
        }
        ;

idlistoption:   { printf("idlistoption->0\n"); }

                | COMMA ID idlistoption {
                	printf("idlist -> , id idlist");
                	node_t *node = find_node(st,$2,st->curr_scope);
                	if(node != NULL){
                		//error
                		char errorStr[15 + strlen($2)];
                		sprintf(errorStr, "ID %s is in use\n",$2);
                		alpha_yyerror(NULL,errorStr);
                	}
                	else if(is_lib_func(st,$2)){//TODOisalibfunc
                		//error
                		char errorStr[30 + strlen($2)];
                		sprintf(errorStr, "ID %s is in use by lib_func\n",$2);
                		alpha_yyerror(NULL,errorStr);
                	}
                	else{
                		add_node(st,$2,formal_var,NULL,alpha_yylineno,st->curr_scope,1);
                	}
                }
                ;

ifstmt:   IF OPPAR expr CLPAR stmt           {printf("ifstmt -> if (expr) stmt (no else) \n");}
        | IF OPPAR expr CLPAR stmt ELSE stmt {printf("ifstmt -> if(expr) stmt else stmt \n");}
        ;

whilestmt: WHILE OPPAR expr CLPAR stmt {printf("whilestmt -> while(expr) stmt \n");}
           ;

forstmt: FOR OPPAR elist SEMCOL expr SEMCOL elist CLPAR stmt {printf("forstmt -> for(elist;expr;elist) stmt \n");}
         ;

returnstmt:   RT SEMCOL      {printf("returnstmt -> 0 \n");}
            | RT expr SEMCOL {printf("returnstmt->return expr \n");}
            ;

%%


int alpha_yyerror(symtable_t *st, char *error_str){
	fprintf(stderr,"In Line %i %s",alpha_yylineno,error_str);
	exit(0);
}


/*main here and other functions*/
