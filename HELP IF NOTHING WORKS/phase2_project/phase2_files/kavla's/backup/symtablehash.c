#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define HASH_MULTIPLIER 65599
#define SIZE 509


/*Possible Types of Symbols*/
typedef enum SymbolTableType{
	GLOBAL, LOCAL, FORMAL,
	USERFUNC, LIBFUNC
}Type;

/*Arguments of functions*/
typedef struct arguments{
        char *name;
        struct arguments *next;
}Arguments;

/*Node for any symbol*/
typedef struct SymTableNode{

	const char *name;				//name
	Arguments Arg;       			//arguments
	Type type;			    		//type
	unsigned int scope;				//scope
	unsigned int line;				//line
	int IsActive;					//isActive
	struct SymTableNode *next;  	//next of symbol with same index
}SymbolNode;


/*The Symbol Table*/
typedef struct SymTable
{
	unsigned int length;
	SymbolNode *SymbolArray[SIZE];
}SymTable_T;

/*~~~~~~~~~~~~~~~~~~~*/ 
/*~~~~~~~~~~~~~~~~~~~*/ 
/*~~~~~~~~~~~~~~~~~~~*/
/*~~~~~~~~~~~~~~~~~~~*/ 
/*Return the index where the symbol is going to be inserted*/
static unsigned int SymTable_hash(const char *name)
{
    size_t i;
    unsigned int iHash;
    iHash = 0U;
    for (i = 0U; name[i] != '\0'; i++)
    iHash = iHash * HASH_MULTIPLIER + name[i];
    return iHash%SIZE;
}

/*Memory allocate for the SymbolTable*/
SymTable_T *SymTable_new(void)
{
	SymTable_T *oSymTable;
	size_t i;
	oSymTable = (SymTable_T*) malloc (sizeof(SymTable_T) );
	oSymTable->length = 0;

	for (i = 0U; i<SIZE; i++)
		oSymTable->SymbolArray[i] = NULL;

	return oSymTable;
}

/*Return the length of the SymbolTable*/
unsigned int SymTable_getLength(SymTable_T *oSymTable)
{
    	assert(oSymTable);
	return oSymTable->length;
}

/*insert a Symbol to the Symbol Table*/
int InsertToSymTable(SymTable_T *oSymTable, const char *name, Type type, int scope, int line)
{
	SymbolNode *NewNode, *root;
	size_t index;

	assert(oSymTable);
    	assert(name);
	/*check if already exists and return 0;*/
	
	index = SymTable_hash(name);

	NewNode = ( SymbolNode* ) malloc(sizeof (SymbolNode) );
	
	NewNode->name = strdup(name);
	NewNode->type = type;
	NewNode->scope = scope;
	NewNode->line = line;
	NewNode->IsActive = 1;
	if ( (NewNode->type != USERFUNC) && (NewNode->type != LIBFUNC) ){
		NewNode->Argnext = NULL;
	}
	else{
		NewNode->Argnext = NULL;/*WRONG*/
	}

	oSymTable->length++;

	root = oSymTable->SymbolArray[index];
	if(oSymTable->SymbolArray[index] == NULL)
	{
		oSymTable->SymbolArray[index] = NewNode;
		oSymTable->SymbolArray[index]->next = NULL;
	}
	else
	{
		NewNode->next = root;
		oSymTable->SymbolArray[index] = NewNode;
	}
	return 1;
}


/*Search by name in specific scope*/
int SymTable_LookUp(SymTable_T *oSymTable, const char *name, int scope)
{
	size_t index;
    SymbolNode *tmp;

    assert(oSymTable);
    assert(name);

    index = SymTable_hash(name);
	tmp = oSymTable->SymbolArray[index];

        while ( tmp != NULL )
        {
                if ( (!strcmp(tmp->name,name)) && ( tmp->scope == scope ) )
                        return 1;

                tmp = tmp->next;
        }

	return 0;
}

/*Prints the Symbol Table*/
void SymTable_Print(SymTable_T *oSymTable)
{
	size_t index;
	SymbolNode *tmp;

        assert(oSymTable);

	for (index=0U; index < SIZE; index++)
	{
		printf("Symbol Table index: %d\n", index);
		tmp = oSymTable->SymbolArray[index];
        	while (tmp != NULL)
        	{
       	        	printf("\tname: %s\t%d\n", tmp->name, tmp->scope);
					tmp = tmp->next;
        	}
	}
}


int main(void)
{
	SymTable_T *oSymTable;

	int integer1=321, integer2=111, k=1;

	oSymTable = SymTable_new();
        printf("%d\n", InsertToSymTable(oSymTable, "fuck", GLOBAL, 7, k++) );
	printf("%d\n", InsertToSymTable(oSymTable, "fuck", LOCAL, 4, k++) );
	printf("%d\n", InsertToSymTable(oSymTable, "fuck", GLOBAL, 3, k++) );
	printf("%d\n", InsertToSymTable(oSymTable, "this", FORMAL, 1, k++) ) ;
	printf("%d\n", InsertToSymTable(oSymTable, "shit", LOCAL, 2, k++) ) ;
	printf("%d\n", InsertToSymTable(oSymTable, "dude", FORMAL, 3, k++) );
        printf("%d\n", InsertToSymTable(oSymTable, "!!!", GLOBAL, 4, k++) ) ;


	printf("oSymTable exei %d stoixeia.\n", SymTable_getLength(oSymTable) );

	printf("\n\n\n");

	SymTable_Print(oSymTable);

	return 0;
}
