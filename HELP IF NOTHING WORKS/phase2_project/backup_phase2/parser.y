%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	
	int yyerror (char* yaccProvidedMessage);
	int yylex (void);
	
	extern int yylineno;
	extern char * yytext;
	extern FILE * yyin;
	int scope=0;
	
%}

%start program


%token 	IF
%token	ELSE
%token	WHILE
%token	FOR
%token	FUNCTION
%token	RETURN
%token	BREAK
%token	CONTINUE
%token	LOCAL
%token	TRUE
%token	FALSE
%token 	NIL
%token	NOT
%token	AND 
%token	OR

%token	ASSIGN
%token	PLUS
%token	MINUS
%token	MULTI
%token	DIV 
%token	MOD
%token	EQUAL
%token	NEQUAL
%token	PPLUS
%token	MMINUS
%token	GREATER
%token	LESS
%token	GREATER_EQUAL
%token	LESS_EQUAL

%token  NUMBER
%token  REALNUM
%token  ID 
%token  STRING

%token	LEFT_CBRACKET
%token	RIGHT_CBRACKET
%token	LEFT_SBRACKET
%token	RIGHT_SBRACKET
%token	LEFT_PARENTHES
%token	RIGHT_PARENTHES
%token	SEMICOLON
%token	COMMA
%token	COLON
%token	DCOLON
%token	DOT
%token	DOTS

%token	COMMENT
%token 	COMMENT2

%token	OTHER


%left	ASSIGN

%left	AND OR

%nonassoc	EQUAL NEQUAL

%nonassoc	GREATER GREATER_EQUAL LESS LESS_EQUAL

%left	PLUS MINUS

%left	MULTI DIV MOD

%right	NOT PPLUS MMINUS UMINUS

%left	DOT DOTS

%left	LEFT_SBRANCKET RIGHT_SBRANCKET

%left	LEFT_PARENTHES RIGHT_PARENTHES

%left	LEFT_CBRANCKET RIGHT_CBRANCKET
%nonassoc IF
%nonassoc ELSE

%union
{
	char* strVal;
	int intVal;
	double dbVal;
}

%%

program		:  statements 		{printf(" program ==> statements \n");}
		;

statements 	:  stmt statements 	{printf("statements ==> stmt statements\n");}
		| /*empty*/		{printf("statements ==> empty \n");}
		;

stmt		: expr SEMICOLON	{printf("stmt ==> expr; \n");}
		| ifstmt		{printf("stmt ==> ifstmt \n");}
		| whilestmt		{printf("stmt ==> whilestmt  \n");}
		| forstmt		{printf("stmt ==> forstmt \n");}
		| returnstmt		{printf("stmt ==> returnstmt \n");}
		| BREAK SEMICOLON	{printf("stmt ==> break; \n");}
		| CONTINUE SEMICOLON	{printf("stmt ==> continue; \n");}
		| block			{printf("stmt ==> block \n");}
		| funcdef		{printf("stmt ==> funcdef \n");}	
		| SEMICOLON		{printf("stmt ==> ; \n");}
		;

expr 		: assignexpr		{printf("expr ==> assignexpr  \n");}
		| expr PLUS expr	{printf("expr ==>  expr + expr  \n");}
		| expr MINUS expr	{printf("expr ==>  expr - expr  \n");}
		| expr MULTI expr	{printf("expr ==>  expr * expr  \n");}
		| expr DIV expr		{printf("expr ==>  expr div expr  \n");}
		| expr MOD expr		{printf("expr ==>  expr mod expr  \n");}
		| expr EQUAL expr	{printf("expr ==>  expr == expr  \n");}
		| expr NEQUAL expr	{printf("expr ==>  expr != expr  \n");}
		| expr GREATER expr	{printf("expr ==>  expr > expr  \n");}
		| expr LESS expr	{printf("expr ==>  expr < expr \n");}
		| expr GREATER_EQUAL expr 	{printf("expr ==> expr >= expr  \n");}
		| expr LESS_EQUAL expr		{printf("expr ==> expr <= expr  \n");}
		| term				{printf("expr ==> term  \n");}
		;

assignexpr	: lvalue ASSIGN expr	{printf("assignexpr ==> lvalue = expr \n");}
		;


term 		: LEFT_PARENTHES expr RIGHT_PARENTHES	{printf("term ==> (expr) \n");}
		| primary				{printf("term ==> primary\n");}
		| MINUS expr %prec UMINUS		{printf("term ==> -expr\n");}
		| PPLUS lvalue				{printf("term ==> ++lvalue\n");}
		| lvalue PPLUS				{printf("term ==> lvalue++\n");}
		| MMINUS lvalue				{printf("term ==> --lvalue\n");}
		| lvalue MMINUS				{printf("term ==> lvalue--\n");}
		| NOT expr				{printf("term ==> !expr\n");}
		;


primary		: lvalue					{printf("primary ==>  lvalue\n");}
		| call						{printf("primary ==>  call\n");}
		| objectdef					{printf("primary ==>  objectdef\n");}
		| LEFT_PARENTHES funcdef RIGHT_PARENTHES	{printf("primary ==>  (funcdef)\n");}
		| const						{printf("primary ==>  const\n");}
		;

const		: NUMBER  		{printf("const ==> number \n");}
		| REALNUM		{printf("const ==> realnum\n");}
		| STRING		{printf("const ==> string\n");}
		| TRUE			{printf("const ==> true\n");}
		| FALSE			{printf("const ==> false\n");}
		| NIL 			{printf("const ==> nil\n");}
		;


lvalue		: ID						{printf("lvalue ==> ID \n");}
		| LOCAL ID					{printf("lvalue ==> LOCAL ID \n");}
		| DCOLON ID					{printf("lvalue ==> DCOLON ID \n");}
		| member					{printf("lvalue ==> member \n");}
		;

member		: lvalue DOT ID					{printf("member ==> lvalue.id \n");}
		| lvalue LEFT_SBRACKET expr RIGHT_SBRACKET	{printf("member ==> lvalue[expr] \n");}
		| call DOT ID					{printf("member ==> call().id) \n");}
		| call LEFT_SBRACKET expr RIGHT_SBRACKET	{printf("member ==> call()[expr] \n");}
		;
call		: call LEFT_PARENTHES elist RIGHT_PARENTHES					{printf("call ==> call(elist)\n");}
		| lvalue callsuffix								{printf("call ==> lvalue callsuffix\n");}
		| LEFT_PARENTHES funcdef RIGHT_PARENTHES LEFT_PARENTHES elist RIGHT_PARENTHES	{printf("call ==> (funcdef)(elist)\n");}
		;

callsuffix	: normcall			{printf("callsuffix ==> normcall\n");}
		| methodcall			{printf("callsuffix ==> methodcall \n");}
		;
normcall	: LEFT_PARENTHES elist RIGHT_PARENTHES		{printf("normalcall ==> (elist)\n");}
		;
methodcall	: DOTS ID LEFT_PARENTHES elist RIGHT_PARENTHES {printf("methodcall ==> ..id (elist)\n");}
		;
elist		: expr cexprs	{printf("elist ==> expr cexprs\n");}
		|/*empty*/	{printf("elist ==> empty \n");}
		;

cexprs		:COMMA expr cexprs {printf("cexprs ==> COMMA expr exprs \n");}
		| /*empty*/	   {printf("elist ==> empty \n");}	
		;


objectdef	: LEFT_SBRACKET elist RIGHT_SBRACKET	{printf("objectdef ==> [elist]\n");}
		| LEFT_SBRACKET indexed RIGHT_SBRACKET   {printf("objectdef ==> [indexed]\n");}
		;

indexed		: indexedelem clindexedelem	{printf("indexed ==> indexedelem clindexedelem\n");}
		;
clindexedelem	: COMMA indexedelem clindexedelem	{printf("clindexedelem ==> ,indexedelem *\n");}
		| /*empty*/		{printf("clindexedelem ==> empty\n");}
		;

indexedelem	: LEFT_CBRACKET expr COLON expr RIGHT_CBRACKET		{printf("indexedelem ==> [expr : expr]\n");}
		;

block		: LEFT_CBRACKET RIGHT_CBRACKET		{printf("block ==> {}\n");}
		| LEFT_CBRACKET stmt statements RIGHT_CBRACKET		{printf("block ==> [stmt*]\n");}
		;

funcdef 	: FUNCTION LEFT_PARENTHES idlist RIGHT_PARENTHES block		{printf("funcdef ==> FUNCTION (idlist) {}\n");}
		| FUNCTION ID LEFT_PARENTHES idlist RIGHT_PARENTHES block	{printf("funcdef ==> FUNCTION ID (idlist) {}\n");}
		;

idlist		: ID cidlist	{printf("idlist ==> ID cidlist\n");}
		| /*empty*/	{printf("idlist ==> empty\n");}
		;
cidlist		: COMMA ID cidlist {printf("cidlist ==> , ID cidlist\n");}
		| /*empty*/	{printf("cidlist ==> empty\n");}
		;

ifstmt		: IF LEFT_PARENTHES expr RIGHT_PARENTHES stmt 	  {printf("ifstmt ==> if (expr) stmt\n");}
		| IF LEFT_PARENTHES expr RIGHT_PARENTHES stmt ELSE stmt  {printf("ifstmt ==> if (expr) stmt else stmt\n");}
		;
whilestmt	: WHILE LEFT_PARENTHES expr RIGHT_PARENTHES stmt	{printf("whilestmt ==> while (expr) stmt\n");}
		;
forstmt		: FOR LEFT_PARENTHES elist SEMICOLON expr SEMICOLON elist RIGHT_PARENTHES stmt	{printf("forstmt ==> for(elist; expr; elist) stmt\n");}
		;
returnstmt	: RETURN expr  SEMICOLON	{printf("returnstmt ==> return expr ;\n");}
		| RETURN SEMICOLON	{printf("returnstmt ==> return ;\n");}
		;



/*to indexed kai to objectdef 8elei kai to keno kanonika */

%%

int yyerror (char* yaccProvidedMessage)
{
	fprintf(stderr, "%s: at line %d, before token: '%s'\n", yaccProvidedMessage, yylineno, yytext);
	fprintf(stderr, "INPUT NOT VALID\n");
}

int main(int argc, char** argv)
{
	if (argc >1){
		if( !(yyin = fopen(argv[1], "r")) ) {
			fprintf(stderr, "Cannor read file: %s\n", argv[1]);
			return 1;
		}
	}
	else
		yyin = stdin;
		
	yyparse();
	
	return 0;
}

