#include "quad.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>


#define EXPAND_SIZE_I 1024
#define CURR_SIZE_I (totalInstr*sizeof(instruction))
#define NEW_SIZE_I (EXPAND_SIZE*sizeof(instruction)+CURR_SIZE_I)

 unsigned int currInstr,zoumpas = 0/** temp integer for currQuad*/;
 unsigned totalInstr=0;

typedef enum vmargs_t ///final code types
{
	label_a,    global_a,  formal_a, 
	local_a,    integer_a, double_a, 
	string_a,   bool_a,    nil_a, 
	userfunc_a, libfunc_a, retval_a, 
} vmarg_t;

typedef enum vmopcodes ///final code opcodes
{assign_v,	add_v,  	sub_v, 
	mul_v, 		div_v, 		mod_v, 
	uminus_v, 	and_v, 		or_v, 
	not_v, 		jeq_v, 		jne_v, 
	jgt_v, 		jlt_v, 		jge_v, 
	jle_v, 		jump_v, 	call_v, 
	funcenter_v, 	funcexit_v, 	newtable_v, 
	tablegetelem_v,	tablesetelem_v,	pusharg_v,
	nop_v, 	 


	

	

} vmopcode;

typedef struct vmarg ///arg of instruction
{
  vmarg_t type;
  unsigned val;
  SymbolNode *sym;
}vmarg;

typedef struct instruction ///instruction of final code
{
  vmopcode opcode;
  vmarg    *result;
  vmarg    *arg1;
  vmarg    *arg2;
  unsigned srcLine;
}instruction;

/** Jump Ret and Func  Lists for Patching*/
typedef struct incomplete_jumps
{
	unsigned int instrNo; /* The jump instruction number. */
	unsigned int iaddress; /* The i-code jump-target address */
	struct incomplete_jumps *next; /* A trivial linked list */
} incomplete_jump;

typedef struct returnList
{
	unsigned int label; /* to label pou einai i entoli */
	struct returnList *next;
} returnList;

typedef struct funcList
{
	SymbolNode *sym;
	struct funcList* next;
	returnList* returnHead;
} func;

/** end */

/** Tables for consts and functions */

typedef struct userFunc
{
  unsigned address;
  unsigned localSize;
  char*	   id;
}userFunc;

typedef struct libFunc
{
  char* id;
}libFunc;

typedef struct stringConst{
  char* value;
}stringConst;

typedef struct integerConst{
  int value;
}integerConst;

typedef struct doubleConst{
  double value;
}doubleConst;



/** end */


typedef void (*generator_func_t) (quad*);





vmarg* make_operand(expr* e);
vmarg* make_booloperand(vmarg* arg, unsigned int tf);
vmarg* make_retvaloperand(vmarg* arg);
vmarg* make_numberoperand(vmarg *, unsigned int);

void run_generate(void);
void generate(vmopcode , quad *);
void generate_relational(vmopcode op, quad* q);

void generate_ADD(quad* q);
void generate_SUB(quad* q);
void generate_MUL(quad* q);
void generate_DIV(quad* q);
void generate_MOD(quad* q);
void generate_NEWTABLE(quad* q);
void generate_TABLEGETELEM(quad* q);
void generate_TABLESETELEM(quad* q);
void generate_ASSIGN(quad* q);
void generate_NOP(quad* q);
void generate_JUMP(quad* q);
void generate_UMINUS(quad *q);
void generate_IF_EQ(quad* q);
void generate_IF__EQ(quad *q);
void generate_IF_NOTEQ(quad* q);
void generate_IF_GREATER(quad* q);
void generate_IF_GREATEREQ(quad* q);
void generate_IF_LESS(quad* q);
void generate_IF_LESSEQ(quad* q);
void generate_AND(quad* q);
void generate_NOT(quad* q);
void generate_OR(quad* q);
void generate_PARAM(quad* q);
void generate_CALL(quad* q);
void generate_GETRETVAL(quad* q);
void generate_FUNCSTART(quad* q);
void generate_RETURN(quad* q);
void generate_FUNCEND(quad* q);

generator_func_t generators_t[] = {

	generate_ASSIGN,
	generate_ADD,
	generate_SUB,
	generate_MUL,
	generate_DIV,
	generate_MOD,
	generate_UMINUS,
	generate_AND,
	generate_OR,
	generate_NOT,
	generate_IF_EQ,
	generate_IF_NOTEQ,
	generate_IF_LESSEQ,
	generate_IF_GREATEREQ,
	generate_IF_LESS,
	generate_IF__EQ,
	generate_IF_GREATER,
	generate_JUMP,
	generate_CALL,
	generate_PARAM,
	generate_RETURN,
	generate_GETRETVAL,
	generate_FUNCSTART,
	generate_FUNCEND,
	generate_NEWTABLE, /* TABLECREATE */
	generate_TABLEGETELEM,
	generate_TABLESETELEM,
	generate_NOP
};


void insert_Instr(instruction* t);
void expand_instruction();
void add_incomplete_jump (unsigned instrNo, unsigned iaddress);
void patch_incomplete_jumps();

/**tables to inserts Consts & Funcs*/
unsigned consts_newstring(char *);
unsigned consts_intnumber(int );
unsigned consts_dblnumber(double );
unsigned libfuncs_newused(const char *);
unsigned userfuncs_newfunc(SymbolNode *);



/**other funcs*/

void print_Args(vmarg *arg);
void print_instruction();

/// END OF PROTOTYPES


/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
 userFunc * funcHead = NULL;
 func *fhead = NULL;
 incomplete_jump *ij_head =(incomplete_jump*) 0;
 unsigned ij_total = 0;
 instruction *instructions = NULL;

 ///TABLES declaration
   char ** stringConsts;
  unsigned totalStringConts=0;

  int* intConsts;
  unsigned totalIntConsts=0;

  double* doubleConsts;
  unsigned totalDbConts=0;

  char **nameLibfuncs;
  unsigned totalNamedLibfuncs=0;

  userFunc * userFuncs;
  unsigned totalUserFuncs=0;

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
unsigned consts_newstring(char *val){
    if (!totalDbConts){
	stringConsts = (char**) malloc (sizeof(char*) );
    }
    else{
	stringConsts = (char **) realloc(stringConsts, ((totalDbConts)+1)*sizeof(char*) );
    }
    
    stringConsts[totalStringConts++] = strdup(val);
}

unsigned consts_intnumber(int val){
    if (!totalIntConsts){
	intConsts = (int*) malloc (sizeof(int));
    }
    else{
	intConsts = (int *) realloc(intConsts, ((totalIntConsts)+1)*sizeof(int) );
    }

    intConsts[totalIntConsts++] = val;
    return totalIntConsts-1;
}

unsigned consts_dblnumber(double val){
    if (!totalDbConts){
	doubleConsts = (double*) malloc (sizeof(double));
    }
    else{
	doubleConsts = (double *) realloc(doubleConsts, ((totalDbConts)+1)*sizeof(double) );
    }

    doubleConsts[totalDbConts++] = val;
}

unsigned libfuncs_newused(const char *id){
    if (!totalNamedLibfuncs){
	nameLibfuncs = (char**) malloc (sizeof(char *) );
    }
    else{
	nameLibfuncs = (char **) realloc(nameLibfuncs, ((totalNamedLibfuncs)+1)*sizeof(char*) );
    }

    nameLibfuncs[totalNamedLibfuncs++] = strdup(id);
}

unsigned userfuncs_newfunc(SymbolNode *UserF){
    
    if (!totalUserFuncs){
	userFuncs = (userFunc*) malloc (sizeof(userFunc));
    }
    else{
	userFuncs = (userFunc *)realloc(userFuncs, (totalUserFuncs+1)*sizeof(userFuncs)) ;
	 
    }

    userFuncs[totalUserFuncs].address = UserF->taddress;
    userFuncs[totalUserFuncs].localSize = UserF->condominass->FuncVarNum;
    userFuncs[totalUserFuncs++].id = strdup(UserF->name);
}

void insert_Instr(instruction* t)
{
  assert(t);

  instruction *instr;//=(instruction *)malloc(sizeof(instruction));
  assert(instr);
  if(currInstr == totalInstr)
    expand_instruction();
   

  instr = instructions + currInstr++;
  instr->opcode = t->opcode;

  instr->arg1 = t->arg1;
  instr->arg2 = t->arg2;
  instr->result = t->result;

  instr->srcLine = t->srcLine;

   return;
}

void generate(vmopcode op, quad *q)
{
  instruction *t;

  t = (instruction *)malloc(sizeof(instruction));
  assert(t != NULL);
  t->opcode= op;
  t->arg1 = make_operand(q->arg1);
  t->arg2 = make_operand(q->arg2);
  t->result = make_operand(q->result);
  q->taddress = currInstr;
  
  insert_Instr(t);
    
  return;
}

void generate_relational(vmopcode op, quad *q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  t->opcode = op;

   t->arg1 = make_operand(q->arg1);
   t->arg2 = make_operand(q->arg2);
   t->result=(vmarg*)malloc(sizeof(vmarg));
   t->result->type = label_a;
   
   if(q->label < zoumpas)
    t->result->val = quads[q->label].taddress;
   else
    add_incomplete_jump(currInstr, q->label);

  q->taddress = currInstr;
  insert_Instr(t);
}

vmarg * make_operand(expr* e)
{
	vmarg* arg;
	if(e == NULL)
		return NULL;
		
	arg = (vmarg *)malloc(sizeof(vmarg));
	
	if(e->sym)///keep symbol node of expr in vmarg
	  {arg->sym=e->sym;printf("%s ", enum_to_str(e->sym->type));
	  }
	switch(e->type)
	{
		/* All those below use a variable for storage */
		case var_e:
		case tableitem_e:
		case arithexpr_e:
		case boolexpr_e:
		case assgnexpr_e:
		{
		  
				arg->val = e->sym->condominass->offset;
			
				if(!strcmp(e->sym->condominass->ScopeSpace,"Formal Argument")) 
						arg->type = formal_a;
				else if(!strcmp(e->sym->condominass->ScopeSpace,"Programm Variables")){
					    if (arg->sym->type == 1)
					      arg->type = local_a;	
					    else 
					      arg->type = global_a;				
				}
				else if(!strcmp(e->sym->condominass->ScopeSpace,"Function Local"))
						arg->type = local_a;
				else
						printf("\t\t\tFalse Scope Space!\n");
				break;
		}
		
		case constbool_e:
		{
			arg->val = e->uni.boolConst;
			arg->type = bool_a;
			break;
		}
		case conststring_e:
		{
			arg->val = consts_newstring(e->uni.strConst);
			arg->type = string_a;
			break;
		}
		case constdouble_e:
			arg->val = consts_dblnumber(e->uni.doubleConst);
			arg->type =double_a;
			break;
		case constint_e:
		{	printf("Mphka k edw e\n : ");
			arg->val = consts_intnumber(e->uni.intConst);
			arg->type = integer_a;
			break;
		}
		case nil_e :
		{
			arg->type = nil_a;
			break;
		}
		case programmfunc_e:
		{
			arg->type = userfunc_a;
			//arg->val = e->sym->taddress;
			break;
		}
		case libraryfunc_e:
		{
			arg->type = libfunc_a;
			arg->val = libfuncs_newused(e->sym->name);
			break;
		}
		default:
			assert(0);
			break;
	}
	
	return arg;
}

void run_generate(void)
{
  
	for(zoumpas=0;zoumpas<currQuad;zoumpas++)
		(*generators_t[quads[zoumpas].op]) (quads+zoumpas);

	patch_incomplete_jumps();
}

void patch_incomplete_jumps()
{
	incomplete_jump *current_ij= ij_head;
	while( current_ij )
	{
		if(current_ij->iaddress == currQuad)
		{
			instructions[current_ij->instrNo].result->val = currInstr;
		}
		else
		{
			instructions[current_ij->instrNo].result->val = quads[current_ij->iaddress].taddress;
		}
		current_ij = current_ij->next;
	}
	return;
}

vmarg* make_booloperand(vmarg* arg, unsigned int val)
{
  arg->val = consts_intnumber(val);
  arg->type = bool_a;
  return arg;
}
vmarg* make_retvaloperand(vmarg* arg)
{
  arg->type = retval_a;
  return arg;
}

vmarg* make_numberoperand(vmarg* arg, unsigned int val)
{
  arg->val = consts_intnumber(val);
  arg->type = integer_a;
}

void generate_ADD(quad* q)
{
  
  generate(add_v, q);
  return;
}
void generate_SUB(quad* q)
{
  generate(sub_v, q);
  return;
}
void generate_MUL(quad* q)
{
  generate(mul_v, q);
  return;
}
void generate_DIV(quad* q)
{
  generate(div_v, q);
  return;
}
void generate_MOD(quad* q)
{
  
  generate(mod_v, q);
  return;
}

void generate_NEWTABLE(quad* q)
{
  generate(newtable_v, q);
  return;
}
void generate_TABLEGETELEM(quad* q)
{
  generate(tablegetelem_v, q);
  return;
}
void generate_TABLESETELEM(quad* q)
{
  generate(tablesetelem_v, q);
  return;
}
void generate_ASSIGN(quad* q)
{
  generate(assign_v, q);
  return;
}
void generate_NOP(quad* q)
{
  assert(q);
  instruction *t = (instruction *)malloc(sizeof(instruction));
  t->opcode = nop_v;
  insert_Instr(t);
  return;
}
void generate_JUMP(quad* q)
{
  generate_relational(jump_v, q);
  return;
}
void generate_IF_EQ(quad* q)
{
  generate_relational(jeq_v, q);
  return;
}
void generate_UMINUS(quad *q)
{
  
}
void generate_IF__EQ(quad *q)
{
   generate_relational(jeq_v, q);
  return;

}
void generate_IF_NOTEQ(quad* q)
{
  generate_relational(jne_v, q);
  return;
}
void generate_IF_GREATER(quad* q)
{
  generate_relational(jgt_v, q);
  return;
}
void generate_IF_GREATEREQ(quad* q)
{
  generate_relational(jge_v, q);
  return;
}
void generate_IF_LESS(quad* q)
{
  generate_relational(jlt_v, q);
  return;
}
void generate_IF_LESSEQ(quad* q)
{
  
  generate_relational(jle_v, q);
  return;
}

/**
 * leipei h ylopoihsh
 */
void generate_AND(quad* q)
{
  return;
}
void generate_NOT(quad* q)
{
  return;
}
void generate_OR(quad* q)
{
  return;
}

/**
 * mexri edw
 */
void generate_PARAM(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  assert(t);
  q->taddress = currInstr;
  t->opcode = pusharg_v;
   t->arg1 = make_operand(q->arg1);
  insert_Instr(t);
  return;
}
void generate_CALL(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  t->opcode = call_v;
  q->taddress = currInstr;
  t->arg1 = make_operand(q->arg1);
  insert_Instr(t);
  return;
}
void generate_GETRETVAL(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  t->opcode = assign_v;
  q->taddress = currInstr;
  t->result = make_operand(q->result);
 t->arg1 = make_retvaloperand(t->arg1);
  insert_Instr(t);
  return;
}
void generate_FUNCSTART(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  SymbolNode *func = q->result->sym;
  if(func==NULL) exit(1);

  func->taddress = currInstr;
  q->taddress = currInstr;

  /**
   * userfunctions.add(id,taddress,localsize);
   * push(funstack, f);
   */
  t->opcode = funcenter_v;
  t->result = make_operand(q->result);
  insert_Instr(t);
  return;
}
void generate_RETURN(quad* q)
{
  q->taddress = currInstr;
  instruction *t = (instruction *)malloc(sizeof(instruction));
  t->opcode = assign_v;
  t->result = make_retvaloperand(t->result);
   t->arg1 = make_operand(q->arg1);
  insert_Instr(t);

  /**
   * f = top(funcstack);
   * append(f.returnlist,currInstr);
   */
  t->opcode = jump_v;

  ///reset_operand(t->arg1);
  ///reset_operand(t->arg2);
  t->result->type = label_a;
  insert_Instr(t);
  return;
}
void generate_FUNCEND(quad* q)
{
  
  /**eimai o vogiatzhs kai exw protimhsh sto piswkolhto
   * f = pop(funcstack);
   * backpatch(f->returnlist,currInstr);
   */

  q->taddress = currInstr;
  instruction *t = (instruction *)malloc(sizeof(instruction));
  t->opcode = funcexit_v;
   t->result = make_operand(q->result);
  insert_Instr(t);
  return;
}



void add_incomplete_jump (unsigned instrNo, unsigned iaddress)
{
    incomplete_jump *newnode = (incomplete_jump*)malloc(sizeof(incomplete_jump));

    newnode->instrNo = instrNo;
    newnode->iaddress = iaddress;

    newnode->next = ij_head;
    ij_total++;

    return;
}
void expand_instruction(){
	instruction *newInstr=malloc(NEW_SIZE_I);

	memcpy(newInstr,instructions,CURR_SIZE_I);
	free(instructions);

	instructions= newInstr;
	totalInstr=totalInstr+EXPAND_SIZE_I;
}


void print_instruction()
{

	int j=0;
	instruction *tmp = instructions;

	printf("    OP  \tResult \t\tArg1 \t\tArg2\n\n");
	while(j < currInstr){

		printf("%d:  ",j);
		tmp= instructions+j++;
		switch(tmp->opcode)
		{
			case assign_v:
				printf("%s\t", "ASSIGN");
				break;
			case add_v:
				printf("%s\t\t", "ADD");
				break;
			case sub_v:
				printf("%s\t\t", "SUB");
				break;
			case mul_v:
				printf("%s\t\t", "MUL");
				break;
			case div_v:
				printf("%s\t\t", "DIV");
				break;
			case mod_v:
				printf("%s\t\t", "MOD");
				break;
			case and_v:
				printf("%s\t\t", "AND");
				break;
			case or_v:
				printf("%s\t\t", "OR");
				break;
			case not_v:
				printf("%s\t\t", "NOT");
				break;
			case jeq_v:
				printf("%s\t\t", "JEQ");
				break;
			case jne_v:
				printf("%s\t\t", "JNE");
				break;
			case jgt_v:
				printf("%s\t\t", "JQT");
				break;
			case jlt_v:
				printf("%s\t\t", "JLT");
				break;
			case jge_v:
				printf("%s\t\t", "JGE");
				break;
			case jle_v:
				printf("%s\t\t", "JLE");
				break;
			case call_v:
				printf("%s\t\t", "CALL");
				break;
			case funcenter_v:
				printf("%s\t", "FUNCENTER");
				break;
			case funcexit_v:
				printf("%s\t", "FUNCEXIT");
				break;
			case newtable_v:
				printf("%s\t", "NEWTABLE");
				break;
			case tablegetelem_v:
				printf("%s\t", "TABLEGETLEM");
				break;
			case tablesetelem_v:
				printf("%s\t", "TABLESETLEM");
				break;
			case pusharg_v:
				printf("%s\t", "PUSHARG");
				break;
			case nop_v:
				printf("%s\t\t", "NOP");
				break;
			case jump_v:
				printf("%s\t\t", "JUMP");
				break;
			default:
				printf("\t*******\n");
				break;
		}
		
		if(tmp->result)
		      print_Args(tmp->result);
		if(tmp->arg1)
		      print_Args(tmp->arg1);
		if(tmp->arg2)
		      print_Args(tmp->arg2);
		printf("\n");
}
	}


void print_Args(vmarg *arg)
{
  switch(arg->type)
			{
				case label_a:
					printf("%d(label)%d\t",arg->type ,arg->val);
					break;
				case global_a:
					printf("%d(global)%d:%s\t",arg->type, arg->val,arg->sym->name);
					break;
				case formal_a:
					printf("%d(formal)%d:%s\t",arg->type, arg->val,arg->sym->name);
					break;
				case local_a:
					printf("%d(local)%d:%s\t",arg->type, arg->val,arg->sym->name);
					break;
				case integer_a:
					printf("%d(int)%d:%d\t", arg->type, arg->val, intConsts[arg->val]);
					break;
				case double_a:
					printf("%d(double)%d:%lf\t",arg->type, arg->val,doubleConsts[arg->val]);
					break;
				case string_a:
					printf("%d(string)%d:%s\t",arg->type, arg->val,stringConsts[arg->val] );
					break;
				case bool_a:
					printf("%d(boolean)%d:",arg->type, arg->val);
					if(arg->val)
					  printf("true\t");
					else
					  printf("false;\t");
					break;
				case nil_a:
					printf("%d(nil)%d:nil\t",arg->type,arg->val);
					break;
				case userfunc_a:
					printf("userfunc%d-\t", arg->val);
					break;
				case libfunc_a:
					printf("libfunc-%d-\t",arg->val);
					break;
				case retval_a:
					printf("retval-%d-\t",arg->val);
					break;
				default:
					printf ("\t*****\t");
					break;
			}
		
}
