#include "av_machine.h"


#define execute_add execute_arithmetic
#define execute_sub execute_arithmetic
#define execute_mul execute_arithmetic
#define execute_div execute_arithmetic
#define execute_mod execute_arithmetic


void print_ti_skata_einai (vmopcode op);

typedef void (*execute_func_t)(instruction *);
execute_func_t executeFuncs[]={
	execute_assign , /// 0
	execute_add,
	execute_sub,
	execute_mul,
	execute_div,
	execute_mod,
	0, //uminus
	0, //and
	0, //or
	0, //not
	execute_jeq,
	execute_jne,
	execute_jgt,
	execute_jlt,
	execute_jge,
	execute_jle,
	execute_jump,
	execute_call,
	execute_funcenter,
	execute_funcexit,
	execute_newtable,
	execute_tablegetelem,
	execute_tablesetelem,
	execute_pusharg
	//execute_nop
};
void execute_cycle(void)
{
	unsigned int oldPc;
	if(executionFinished)
		return;

	else{

		if(pc==AVM_ENDING_PC){
			printf("Execution of Virtual Machine completed.\n");
			executionFinished =1;
			return;
		}
		else{
			assert(pc < AVM_ENDING_PC);
			//instruction * instr = code+pc;     old
			instruction * instr = instructions+pc;
			//printf("AVM_ENDING_PC %d",AVM_ENDING_PC);

			assert(instr->opcode>=0 && instr->opcode <= AVM_MAX_INSTRUCTIONS);

			if(instr->srcLine)
				currLine =  instr->srcLine;

				oldPc=pc;
				  print_ti_skata_einai(instr->opcode);
				(*executeFuncs[instr->opcode])(instr);


			if(pc==oldPc)
				pc++;

		}
	}


}


/**code for run phase 5*/
void avm_read_binary()
{
//Read the binary and start running
  FILE *fp=NULL;
  int magNum;
  int i, tmp;
  instruction *newCodeItem;


  fp = fopen("BinaryCode.abc", "rb");
  if(!fp)
    printf("Error could not open source file \n");


  fread(&magNum,sizeof(int),1,fp);
  if( magNum != GenerateMagic("Poutsa kai ksulo...Stis karioles Freno!!!"))
    {
       printf("Wrong magic Number. Program will exit! Poulos leme!\n");
       exit(-1);
    }

	fread(&RtotalStringConsts,sizeof(int),1,fp);///read total
	RstringConsts = (char**) malloc (RtotalStringConsts * sizeof(char*) );///memory allocate total size table
  	for(i=0; i<RtotalStringConsts; i++){
		if (fread(&RstringConsts[i],sizeof(char *),1,fp)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	}

	fread(&RtotalNumConsts,sizeof(int),1,fp);///read total
	RNumConsts = (double*) malloc (RtotalNumConsts * sizeof(double));///memory allocate total size table
	for(i=0; i<RtotalNumConsts; i++){
		if (fread(&RNumConsts[i],sizeof(double),1,fp)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	}

	fread(&RtotalNamedLibfuncs,sizeof(int),1,fp);///read total
	RnameLibfuncs = (char**) malloc (RtotalNamedLibfuncs * sizeof(char *) );///memory allocate total size table
	for(i=0; i<RtotalNamedLibfuncs; i++){
		if (fread(&RnameLibfuncs[i],sizeof(char *),1,fp)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	}

	fread(&RtotalUserFuncs,sizeof(int),1,fp);///read total
	RuserFuncs = (userFunc*) malloc (RtotalUserFuncs * sizeof(userFunc));///memory allocate total size table
	for(i=0; i<RtotalUserFuncs; i++){

		if (fread(&RuserFuncs[i].id,sizeof(char *),1,fp)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
		if (fread(&RuserFuncs[i].address,sizeof(int),1,fp)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
		if (fread(&RuserFuncs[i].localSize,sizeof(int),1,fp)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	}


  fread(&codeSize,sizeof(int),1,fp);
  if(!codeSize){
    printf("No instructions, Programm terminated!\n");
    exit(1);
  }

  code = (instruction*) malloc (codeSize * sizeof (instruction) );
  for(i = 0 ; i< codeSize; i++)
  {
		newCodeItem = code+i;
		fread(&(newCodeItem->opcode), sizeof(int), 1,fp);

		switch (newCodeItem->opcode){

		case assign_v:
		{
			  newCodeItem->result = (vmarg*) malloc(sizeof(vmarg));
			  fread(&(newCodeItem->result->type), sizeof(int), 1,fp);
			  fread(&(newCodeItem->result->val), sizeof(int), 1,fp);

			  newCodeItem->arg1 = (vmarg*) malloc(sizeof(vmarg));
			  fread(&(newCodeItem->arg1->type), sizeof(int), 1,fp);
			  fread(&(newCodeItem->arg1->val), sizeof(int), 1,fp);
			  break;
		}
		case add_v:
		case sub_v:
		case mul_v:
		case div_v:
		case mod_v:
		case jeq_v:
		case jne_v:
		case jgt_v:
		case jge_v:
		case jlt_v:
		case jle_v:
		case tablegetelem_v:
		case tablesetelem_v:{
			  newCodeItem->result = (vmarg*) malloc(sizeof(vmarg));
			  fread(&(newCodeItem->result->type), sizeof(int), 1,fp);
			  fread(&(newCodeItem->result->val), sizeof(int), 1,fp);

			  newCodeItem->arg1 = (vmarg*) malloc(sizeof(vmarg));
			  fread(&(newCodeItem->arg1->type), sizeof(int), 1,fp);
			  fread(&(newCodeItem->arg1->val), sizeof(int), 1,fp);

			  newCodeItem->arg2 = (vmarg*) malloc(sizeof(vmarg));
			  fread(&(newCodeItem->arg2->type), sizeof(int), 1,fp);
			  fread(&(newCodeItem->arg2->val), sizeof(int), 1,fp);
			  break;
		}
		case funcenter_v:
		case funcexit_v:
		case jump_v:
		case newtable_v:{
			  newCodeItem->result = (vmarg*) malloc(sizeof(vmarg));
			  fread(&(newCodeItem->result->type), sizeof(int), 1,fp);
			  fread(&(newCodeItem->result->val), sizeof(int), 1,fp);
			  break;
		}
		case call_v:
		case pusharg_v:{
			  newCodeItem->arg1 = (vmarg*) malloc(sizeof(vmarg));
			  fread(&(newCodeItem->arg1->type), sizeof(int), 1,fp);
			  fread(&(newCodeItem->arg1->val), sizeof(int), 1,fp);
			  break;
		}
	}
 }


  codeSize=currInstr;

  Print_txt_Final_Code("FinalCodeAfterRead.txt", RstringConsts, RtotalStringConsts, RNumConsts, RtotalNumConsts,
				RnameLibfuncs, RtotalNamedLibfuncs, RuserFuncs, RtotalUserFuncs, code, codeSize);




 avm_initialize ();
 glbnum=AVM_STACKSIZE - ScopeArray[0]->condominass->offset -1;
 printf("Glbnum is :%d  \n", glbnum);
 top = glbnum -2;
 topsp=top;
 printf("Top    is : %d \nTopsp  is :  %d\n", top,topsp);
 while(executionFinished ==0)
	 execute_cycle();

 close(fp);

}


void print_ti_skata_einai (vmopcode op)
{
  printf("\n");
  switch(op)
  {
    case assign_v:
	puts("Opcode : assign ");
	break;
    case add_v:
      puts("Opcode : add ");
	break;
    case sub_v:
      puts("Opcode : sub ");
	break;
    case mul_v:
      puts("Opcode : mul ");
	break;
    case div_v:
      puts("Opcode : div \n");
	break;
    case mod_v:
      puts("Opcode : mod \n");
	break;
    case jeq_v:
      puts("Opcode : jump equal ");
	break;
    case jne_v:
      puts("Opcode : jump not equal ");
	break;
    case jgt_v:
      puts("Opcode : jump greater ");
	break;
    case jlt_v:
      puts("Opcode : jump less ");
	break;
    case jge_v:
      puts("Opcode : jump greater equal ");
	break;
    case jle_v:
      puts("Opcode : jump less equal ");
	break;
    case jump_v:
      puts("Opcode : jump ");
	break;
    case call_v:
      puts("Opcode : call ");
	break;
    case funcenter_v:
      puts("Opcode : funcenter ");
	break;
    case funcexit_v:
      puts("Opcode : funcexit ");
	break;
    case newtable_v:
      puts("Opcode : newtable  ");
	break;
    case tablegetelem_v:
      puts("Opcode : tablegetelem  ");
	break;
    case tablesetelem_v:
      puts("Opcode : tablesetelem ");
	break;
    case pusharg_v:
      puts("Opcode : pusharg ");
	break;

  }
    printf("top %d pc %d topsp %d \n", top,pc, topsp);
    //getchar();
}