#include "quad.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>


#define EXPAND_SIZE_I 1024
#define CURR_SIZE_I (totalInstr*sizeof(instruction))
#define NEW_SIZE_I (EXPAND_SIZE*sizeof(instruction)+CURR_SIZE_I)

 unsigned int currInstr,zoumpas = 0/** temp integer for currQuad*/;
 unsigned totalInstr=0;

typedef enum vmargs_t ///final code types
{
	label_a,    global_a,  formal_a,
	local_a,    number_a,
	string_a,   bool_a,    nil_a,
	userfunc_a, libfunc_a, retval_a,
} vmarg_t;

typedef enum vmopcodes ///final code opcodes
{	assign_v,	add_v,  	sub_v,
	mul_v, 		div_v, 		mod_v,
	uminus_v, 	and_v, 		or_v,
	not_v, 		jeq_v, 		jne_v,
	jgt_v, 		jlt_v, 		jge_v,
	jle_v, 		jump_v, 	call_v,
	funcenter_v, 	funcexit_v, 	newtable_v,
	tablegetelem_v,	tablesetelem_v,	pusharg_v,
	nop_v

} vmopcode;

typedef struct vmarg ///arg of instruction
{
  vmarg_t type;
  unsigned val;
  SymbolNode *sym;
}vmarg;

typedef struct instruction ///instruction of final code
{
  vmopcode opcode;
  vmarg    *result;
  vmarg    *arg1;
  vmarg    *arg2;
  unsigned srcLine;

}instruction;

/** List for incomplete jump patching*/
typedef struct incomplete_jumps
{
	unsigned int instrNo; /* The jump instruction number. */
	unsigned int iaddress; /* The i-code jump-target address */
	struct incomplete_jumps *next; /* A trivial linked list */
} incomplete_jump;
/**end */

/** return list and function stack for return jumps patching*/
typedef struct returnList
{
	unsigned int label; /* incomplete jumps of return ops */
	struct returnList *next;
} returnList;

typedef struct funcStack
{
	SymbolNode *sym;
	returnList* returnHead;
	int StartJump;
	struct funcStack* next;

} funcStack;
funcStack *FunctionStack = NULL;
/** end */


/** Table for User functions */
typedef struct userFunc
{
  unsigned address;
  unsigned endaddress;
  unsigned localSize;
  char*	   id;
}userFunc;
/** end */


typedef void (*generator_func_t) (quad*);


vmarg* make_operand(expr* );
vmarg* make_booloperand(vmarg* , unsigned int);
vmarg* make_retvaloperand(vmarg* );
vmarg* make_numberoperand(vmarg *, double);

void run_generate(void);
void generate(vmopcode , quad *);
void generate_relational(vmopcode op, quad* q);

void generate_ADD(quad* q);
void generate_SUB(quad* q);
void generate_MUL(quad* q);
void generate_DIV(quad* q);
void generate_MOD(quad* q);
void generate_NEWTABLE(quad* q);
void generate_TABLEGETELEM(quad* q);
void generate_TABLESETELEM(quad* q);
void generate_ASSIGN(quad* q);
void generate_NOP(quad* q);
void generate_JUMP(quad* q);
void generate_UMINUS(quad *q);
void generate_IF_EQ(quad* q);
void generate_IF_NOTEQ(quad* q);
void generate_IF_GREATER(quad* q);
void generate_IF_GREATEREQ(quad* q);
void generate_IF_LESS(quad* q);
void generate_IF_LESSEQ(quad* q);
void generate_AND(quad* q);
void generate_NOT(quad* q);
void generate_OR(quad* q);
void generate_PARAM(quad* q);
void generate_CALL(quad* q);
void generate_GETRETVAL(quad* q);
void generate_FUNCSTART(quad* q);
void generate_RETURN(quad* q);
void generate_FUNCEND(quad* q);

generator_func_t generators_t[] = {

	generate_ASSIGN,
	generate_ADD,
	generate_SUB,
	generate_MUL,
	generate_DIV,
	generate_MOD,
	generate_UMINUS,
	generate_AND,
	generate_OR,
	generate_NOT,
	generate_IF_EQ,
	generate_IF_NOTEQ,
	generate_IF_LESSEQ,
	generate_IF_GREATEREQ,
	generate_IF_LESS,
	generate_IF_GREATER,
	generate_JUMP,
	generate_CALL,
	generate_PARAM,
	generate_RETURN,
	generate_GETRETVAL,
	generate_FUNCSTART,
	generate_FUNCEND,
	generate_NEWTABLE,
	generate_TABLEGETELEM,
	generate_TABLESETELEM,
	generate_NOP
};


void insert_Instr(instruction* t);
void expand_instruction();
void add_incomplete_jump (unsigned instrNo, unsigned iaddress);
void patch_incomplete_jumps();

/**tables to inserts Consts & Funcs*/
unsigned consts_newstring(char *);
unsigned consts_number(double );
unsigned libfuncs_newfunc(const char *);
unsigned userfuncs_newfunc(SymbolNode *);

/** pop and push for function stack
	and add of return list jumps */
funcStack* FunctionPop();
void FunctionPush(SymbolNode *);
void AddtoTopReturnList(int );
void PatchReturnJumps();

/**other funcs*/
void print_Args(vmarg *arg);
void print_instruction();
void Print_txt_Final_Code(char *txt, char ** strCon, unsigned totalS, double* NumCon, unsigned totalN,
				char **Libf, unsigned totalLibf, userFunc * userF, unsigned totalUserF, instruction *root, int total);
void TestPrintAllTables(FILE *bin, char ** strCon, unsigned totalS, double* NumCon, unsigned totalN,
				char **Libf, unsigned totalLibf, userFunc * userF, unsigned totalUserF);
void Write_Binary_Code();
/// END OF PROTOTYPES


/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

 incomplete_jump *ij_head =(incomplete_jump*) 0;
 unsigned ij_total = 0;
 instruction *instructions = NULL;

 ///TABLES declaration
   char ** stringConsts;
  unsigned totalStringConsts=0;

  double* NumConsts;
  unsigned totalNumConsts=0;

  char **nameLibfuncs;
  unsigned totalNamedLibfuncs=0;

  userFunc * userFuncs;
  unsigned totalUserFuncs=0;


/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
unsigned consts_newstring(char *val){
    if (!totalStringConsts){
	stringConsts = (char**) malloc (sizeof(char*) );
    }
    else{
	stringConsts = (char **) realloc(stringConsts, ((totalStringConsts)+1)*sizeof(char*) );
    }

    stringConsts[totalStringConsts++] = strdup(val);

    return totalStringConsts -1;
}


unsigned consts_number(double val){
    if (!totalNumConsts){
	NumConsts = (double*) malloc (sizeof(double));
   }
    else{
	NumConsts = (double *) realloc(NumConsts, ((totalNumConsts)+1)*sizeof(double) );
    }

    NumConsts[totalNumConsts++] = val;
    return totalNumConsts - 1;
}

unsigned libfuncs_newfunc(const char *id){
	//if already exists return its index,
	for (i=0;i<totalNamedLibfuncs; i++){
		if (!strcmp(nameLibfuncs[i],id) )
			return i;
	}

	if ( totalNamedLibfuncs == 12){///if all libs entered
		return -1; ///not found
	}

    if (!totalNamedLibfuncs){
	nameLibfuncs = (char**) malloc (sizeof(char *) );
    }
    else{
	nameLibfuncs = (char **) realloc(nameLibfuncs, ((totalNamedLibfuncs)+1)*sizeof(char*) );
    }

    nameLibfuncs[totalNamedLibfuncs++] = strdup(id);
    return totalNamedLibfuncs-1;
}

unsigned userfuncs_newfunc(SymbolNode *UserF){
	int i;

	assert(UserF->type == USERFUNC);///check if it is a USERFUNC

	//if already exists return its index,
	for (i=0;i<totalUserFuncs; i++){
		if (!strcmp(userFuncs[i].id,UserF->name) )
			return i;
	}
	//else insert to the table.
    if (!totalUserFuncs){
		userFuncs = (userFunc*) malloc (sizeof(userFunc));
    }
    else{
		userFuncs = (userFunc *)realloc(userFuncs, ((totalUserFuncs)+1)*sizeof(userFunc)) ;
    }

    userFuncs[totalUserFuncs].address = UserF->taddress;
	userFuncs[totalUserFuncs].endaddress = -1;
    userFuncs[totalUserFuncs].localSize = UserF->condominass->FuncVarNum;
    userFuncs[totalUserFuncs++].id = strdup(UserF->name);

    return totalUserFuncs-1;
}

void insert_Instr(instruction* t)
{
  assert(t);

  instruction *instr;
  //assert(instr);//to evala se sxolio giati gamietai
  if(currInstr == totalInstr)
    expand_instruction();


  instr = instructions + currInstr++;
  instr->opcode = t->opcode;

  instr->arg1 = t->arg1;
  instr->arg2 = t->arg2;
  instr->result = t->result;

  instr->srcLine = currInstr-1;

   return;
}

void generate(vmopcode op, quad *q)
{
  instruction *t;

  t = (instruction *)malloc(sizeof(instruction));
  assert(t != NULL);
  t->opcode= op;
  t->arg1 = make_operand(q->arg1);
  t->arg2 = make_operand(q->arg2);
  //printf("q->result : %d %s\n", q->result->sym->condominass->offset, q->result->uni.strConst);
  t->result = make_operand(q->result);
  q->taddress = currInstr;

  insert_Instr(t);

  return;
}

void generate_relational(vmopcode op, quad *q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  t->opcode = op;

   t->arg1 = make_operand(q->arg1);
   t->arg2 = make_operand(q->arg2);
   t->result=(vmarg*)malloc(sizeof(vmarg));
   t->result->type = label_a;

   if(q->label < zoumpas)
    t->result->val = quads[q->label].taddress;
   else
    add_incomplete_jump(currInstr, q->label);

  q->taddress = currInstr;
  insert_Instr(t);
}

vmarg * make_operand(expr* e)
{
	vmarg* arg;
	if(e == NULL){
		//printf("h fash einai null to expr sth make operand\n");
		return NULL;
	}
	arg = (vmarg *)malloc(sizeof(vmarg));

	if(e->sym)///keep symbol node of expr in vmarg
	  arg->sym=e->sym;

	switch(e->type)
	{
		/* All those below use a variable for storage */
		case var_e:
		case arithexpr_e:
		case boolexpr_e: ///NEVER COMES HERE
		case newtable_e:
		case tableitem_e:
		case assgnexpr_e:
		{

				arg->val = e->sym->condominass->offset;

				if(!strcmp(e->sym->condominass->ScopeSpace,"Formal Argument"))
						arg->type = formal_a;

				else if(!strcmp(e->sym->condominass->ScopeSpace,"Programm Variables")){
					    if (arg->sym->type == 1)
					      arg->type = local_a;
					    else
					      arg->type = global_a;
				}
				else if(!strcmp(e->sym->condominass->ScopeSpace,"Function Local"))
						arg->type = local_a;


				break;
		}

		case constbool_e:
		{
			arg->val = e->uni.boolConst;
			arg->type = bool_a;
			break;
		}
		case conststring_e:
		{
			arg->val = consts_newstring(e->uni.strConst);
			arg->type = string_a;
			break;
		}
		case constdouble_e:
		{
			arg->val = consts_number(e->uni.doubleConst);
			arg->type = number_a;
			break;
		}
		case constint_e:
		{
			arg->val = consts_number(e->uni.intConst);
			arg->type = number_a;
			break;
		}
		case nil_e :
		{
			arg->type = nil_a;
			break;
		}

		case programmfunc_e:
		{
			arg->type = userfunc_a;
			arg->val = 	userfuncs_newfunc(e->sym);	
			break;
		}
		case libraryfunc_e:
		{
			arg->type = libfunc_a;
			arg->val = libfuncs_newfunc(e->sym->name);
			break;
		}
		default:
			assert(0);
			break;
	}

	return arg;
}

void run_generate(void)
{
	for(zoumpas=0;zoumpas<currQuad;zoumpas++){
	  printf("to Op tou quad twra: %d\n",quads[zoumpas].op);
		(*generators_t[quads[zoumpas].op]) (quads+zoumpas);
	}

	patch_incomplete_jumps();
}

void patch_incomplete_jumps()
{
	incomplete_jump *current_ij= ij_head;
	while( current_ij )
	{
		if(current_ij->iaddress == currQuad)
		{
			instructions[current_ij->instrNo].result->val = currInstr;
		}
		else
		{
			instructions[current_ij->instrNo].result->val = quads[current_ij->iaddress].taddress;
		}
		current_ij = current_ij->next;
	}
	return;
}

vmarg* make_booloperand(vmarg* arg, unsigned int val)
{
  arg->val = consts_number(val);
  arg->type = bool_a;
  return arg;
}

vmarg* make_retvaloperand(vmarg* arg)
{
  arg->type = retval_a;
  ///no need for val.
  return arg;
}

vmarg* make_numberoperand(vmarg* arg, double val)
{
  arg->val = consts_number(val);
  arg->type = number_a;
  return arg;
}

void generate_ADD(quad* q)
{
  generate(add_v, q);
  return;
}
void generate_SUB(quad* q)
{
  generate(sub_v, q);
  return;
}
void generate_MUL(quad* q)
{
  generate(mul_v, q);
  return;
}
void generate_DIV(quad* q)
{
  generate(div_v, q);
  return;
}
void generate_MOD(quad* q)
{
  generate(mod_v, q);
  return;
}

void generate_NEWTABLE(quad* q)
{
  generate(newtable_v, q);
  return;
}
void generate_TABLEGETELEM(quad* q)
{
  generate(tablegetelem_v, q);
  return;
}
void generate_TABLESETELEM(quad* q)
{
  generate(tablesetelem_v, q);
  return;
}
void generate_ASSIGN(quad* q)
{
  generate(assign_v, q);
  return;
}
void generate_NOP(quad* q)
{
  assert(q);
  instruction *t = (instruction *)malloc(sizeof(instruction));
  t->opcode = nop_v;
  insert_Instr(t);
  return;
}
void generate_JUMP(quad* q)
{
  generate_relational(jump_v, q);
  return;
}
void generate_IF_EQ(quad* q)
{
  generate_relational(jeq_v, q);
  return;
}
void generate_UMINUS(quad *q)
{

}

void generate_IF_NOTEQ(quad* q)
{
  generate_relational(jne_v, q);
  return;
}
void generate_IF_GREATER(quad* q)
{
  generate_relational(jgt_v, q);
  return;
}
void generate_IF_GREATEREQ(quad* q)
{
  generate_relational(jge_v, q);
  return;
}
void generate_IF_LESS(quad* q)
{
  generate_relational(jlt_v, q);
  return;
}
void generate_IF_LESSEQ(quad* q)
{

  generate_relational(jle_v, q);
  return;
}

/**
 * Den xreiazontai logw veltistopoihshs triths fashs
 */
void generate_AND(quad* q)
{
  return;
}
void generate_NOT(quad* q)
{
  return;
}
void generate_OR(quad* q)
{
  return;
}
/**
 * mexri edw
 */

void generate_PARAM(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  assert(t);
  q->taddress = currInstr;
  t->opcode = pusharg_v;
  t->arg1 = make_operand(q->arg1);
  t->arg2 = NULL;
  t->result = NULL;
  insert_Instr(t);
  return;
}

void generate_CALL(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  instruction *fj = (instruction *)malloc(sizeof(instruction));
  assert(q->arg1->sym);

  int JumpLabelFuncExit;
																		///the function exit
  //call
  t->opcode = call_v;
  q->taddress = currInstr;
  t->arg1 = make_operand(q->arg1);
  t->arg2 = NULL;
  t->result = NULL;
  insert_Instr(t);
  //apo edw (gia allo tropo func def)
  if(q->arg1->type!=libraryfunc_e){
	//jump
	JumpLabelFuncExit = userFuncs[userfuncs_newfunc(q->arg1->sym)].endaddress + 1;///take the address of the jump after
	fj->opcode = jump_v;
	fj->result = (vmarg *) malloc (sizeof(vmarg) );
	fj->result->type = label_a;
	fj->result->val = userFuncs[userfuncs_newfunc(q->arg1->sym)].address;
	fj->arg1 = NULL;
	fj->arg2 = NULL;
	insert_Instr(fj);

	//patch the jump after func exit to the call address (assign of retval)
	instructions[JumpLabelFuncExit].result->val = currInstr;
	//mexri edw
  }



  return;
}
void generate_GETRETVAL(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));

  t->opcode = assign_v;
  q->taddress = currInstr;
  t->result = make_operand(q->result);
  t->arg1 = (vmarg *) malloc (sizeof(vmarg) );
  t->arg1 = make_retvaloperand(t->arg1);
  t->arg2 = NULL;

  insert_Instr(t);

  return;
}

void generate_FUNCSTART(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  instruction *fj = (instruction *)malloc(sizeof(instruction));

  //jump
  fj->opcode = jump_v;
  fj->result = (vmarg *) malloc (sizeof(vmarg) );
  fj->result->type = label_a;
  fj->arg1 = NULL;
  fj->arg2 = NULL;
  insert_Instr(fj);

  q->taddress = currInstr;
  ///update the taddress in quad and symbolnode before we insert an item to userfunc table
  q->result->sym->taddress = currInstr;
  FunctionPush(q->result->sym);///add to top of stack

  //funcstart
  t->opcode = funcenter_v;
  t->result = make_operand(q->result);
  t->result->val = userfuncs_newfunc(q->result->sym);///add function to function table
													///and assign the index to ->val.
  t->arg2 = NULL;
  t->arg1 = NULL;
  insert_Instr(t);
  return;
}


void generate_RETURN(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  instruction *tj = (instruction *)malloc(sizeof(instruction));

  q->taddress = currInstr;

  //assign value to retval
  t->opcode = assign_v;
  t->result = (vmarg *) malloc (sizeof(vmarg) );
  t->result = make_retvaloperand(t->result);

  if (q->result)
    t->arg1 = make_operand(q->result);
  else///case for return ;
    t->arg1 = make_operand(ConstBool_exp(0) );

  t->arg2 = NULL;
  insert_Instr(t);


  AddtoTopReturnList(currInstr);///add this label to the Return List

  //jump
  tj->opcode = jump_v;
  tj->result = (vmarg *) malloc (sizeof(vmarg) );
  tj->result->type = label_a;
  tj->arg1 = NULL;
  tj->arg2 = NULL;
  insert_Instr(tj);
  return;
}

void generate_FUNCEND(quad* q)
{
  instruction *t = (instruction *)malloc(sizeof(instruction));
  instruction *fj = (instruction *)malloc(sizeof(instruction));

  q->taddress = currInstr;
  userFuncs[userfuncs_newfunc(q->result->sym)].endaddress = currInstr;//insert the end address in table
  PatchReturnJumps();///patch all jumps and pop the stack

  //funcexit
  t->opcode = funcexit_v;
  t->result = make_operand(q->result);
  t->result->val = currInstr;
  t->arg2 = NULL;
  t->arg1 = NULL;
  insert_Instr(t);

  //apo edw
  //jump
  fj->opcode = jump_v;//Jump after func end to label after call.
  fj->result = (vmarg *) malloc (sizeof(vmarg) );
  fj->result->type = label_a;
  fj->arg1 = NULL;
  fj->arg2 = NULL;
  insert_Instr(fj);
  //mexri edw
  return;
}


void add_incomplete_jump (unsigned instrNo, unsigned iaddress)
{
    incomplete_jump *newnode = (incomplete_jump*)malloc(sizeof(incomplete_jump));

    newnode->instrNo = instrNo;
    newnode->iaddress = iaddress;

    newnode->next = ij_head;
    ij_head = newnode;
    ij_total++;

    return;
}

funcStack* FunctionPop(){
	funcStack *PopItem;

	if (!FunctionStack)
		return NULL;

	PopItem = FunctionStack;
	FunctionStack = FunctionStack->next;

	return PopItem;
}

void FunctionPush(SymbolNode *FuncSym){
	funcStack *tmpHead;

	///create newnode
	funcStack *newnode = (funcStack *) malloc( sizeof(funcStack) );
	newnode->sym = FuncSym;
	newnode->returnHead = NULL;
	newnode->StartJump = currInstr-1;//needed to patch the jump before the funcenter

	///add to top
	tmpHead = FunctionStack;
	newnode->next = tmpHead;
	FunctionStack = newnode;

	return;
}

void AddtoTopReturnList(int label){
	returnList *RetListHead = FunctionStack->returnHead;

	///create newnode
	returnList *newnode = (returnList*) malloc (sizeof(returnList) );
	newnode->label = label;

	///add to head
	newnode->next = RetListHead;
	FunctionStack->returnHead = newnode;

	return;
}

void PatchReturnJumps(){
	funcStack *topFunc;
	returnList *TopRetList;

	topFunc = FunctionPop();///Delete Top and Keep it for now
	TopRetList = topFunc->returnHead;///to make the backpatch

	for(;TopRetList!=NULL;TopRetList=TopRetList->next){

			instructions[TopRetList->label].result->val = currInstr;
	}
	instructions[topFunc->StartJump].result->val = currInstr+/*1*/2;///Finally patch the jump before the FuncEnter to the label
																	///after the jump of the func end.
}

void expand_instruction(){
	instruction *newInstr=malloc(NEW_SIZE_I);

	memcpy(newInstr,instructions,CURR_SIZE_I);
	free(instructions);

	instructions= newInstr;
	totalInstr=totalInstr+EXPAND_SIZE_I;
}


void print_instruction()
{
	int j=0;
	instruction *tmp = instructions;

	printf("\n\nLine \tOpCode \t\tResult \t\tArg1 \t\tArg2\n\n");
	while(j < currInstr){

		printf("%d:\t",j);
		tmp= instructions+j++;
		switch(tmp->opcode)
		{
			case assign_v:
				printf("%s\t\t", "ASSIGN");
				break;
			case add_v:
				printf("%s\t\t", "ADD");
				break;
			case sub_v:
				printf("%s\t\t", "SUB");
				break;
			case mul_v:
				printf("%s\t\t", "MUL");
				break;
			case div_v:
				printf("%s\t\t", "DIV");
				break;
			case mod_v:
				printf("%s\t\t", "MOD");
				break;
			case and_v:
				printf("%s\t\t", "AND");
				break;
			case or_v:
				printf("%s\t\t", "OR");
				break;
			case not_v:
				printf("%s\t\t", "NOT");
				break;
			case jeq_v:
				printf("%s\t\t", "JEQ");
				break;
			case jne_v:
				printf("%s\t\t", "JNE");
				break;
			case jgt_v:
				printf("%s\t\t", "JQT");
				break;
			case jlt_v:
				printf("%s\t\t", "JLT");
				break;
			case jge_v:
				printf("%s\t\t", "JGE");
				break;
			case jle_v:
				printf("%s\t\t", "JLE");
				break;
			case call_v:
				printf("%s\t\t", "CALL");
				break;
			case funcenter_v:
				printf("%s\t", "FUNCENTER");
				break;
			case funcexit_v:
				printf("%s\t", "FUNCEXIT");
				break;
			case newtable_v:
				printf("%s\t", "NEWTABLE");
				break;
			case tablegetelem_v:
				printf("%s\t", "TABLEGETLEM");
				break;
			case tablesetelem_v:
				printf("%s\t", "TABLESETLEM");
				break;
			case pusharg_v:
				printf("%s\t\t", "PUSHARG");
				break;
			case nop_v:
				printf("%s\t\t", "NOP");
				break;
			case jump_v:
				printf("%s\t\t", "JUMP");
				break;
			default:
				printf("\t*******\n");
				break;
		}

		if(tmp->result)
		      print_Args(tmp->result);
		else	printf("\t\t");
		if(tmp->arg1)
		      print_Args(tmp->arg1);
		else	printf("\t\t");
		if(tmp->arg2)
		      print_Args(tmp->arg2);
		else	printf("\t\t");
		printf("\n");
	}
	printf("\n");
	TestPrintAllTables(stderr, stringConsts, totalStringConsts, NumConsts, totalNumConsts, nameLibfuncs, totalNamedLibfuncs, userFuncs, totalUserFuncs);
	Write_Binary_Code();
	Print_txt_Final_Code("FinalCodeBeforeRead.txt", stringConsts, totalStringConsts, NumConsts, totalNumConsts,
				nameLibfuncs, totalNamedLibfuncs, userFuncs, totalUserFuncs, instructions, currInstr);

}


void print_Args(vmarg *arg)
{
  switch(arg->type)
			{
				case label_a:
					printf("0%d(label)%d\t",arg->type ,arg->val);
					break;
				case global_a:
					printf("0%d(global)%d:%s\t",arg->type, arg->val,arg->sym->name);
					break;
				case formal_a:
					printf("0%d(formal)%d:%s\t",arg->type, arg->val,arg->sym->name);
					break;
				case local_a:
					printf("0%d(local)%d:%s\t",arg->type, arg->val,arg->sym->name);
					break;
				case number_a:
					printf("0%d(number)%d:%.1lf\t",arg->type, arg->val,NumConsts[arg->val]);
					break;
				case string_a:
					printf("0%d(string)%d:%s\t",arg->type, arg->val,stringConsts[arg->val] );
					break;
				case bool_a:
					printf("0%d(boolean)%d:",arg->type, arg->val);
					if(arg->val)
					  printf("true\t");
					else
					  printf("false;\t");
					break;
				case nil_a:
					printf("0%d(nil)%d:nil\t",arg->type,arg->val);
					break;
				case userfunc_a:
					printf("0%d(userfunc)%d:%s\t", arg->type, arg->val, arg->sym->name);
					break;
				case libfunc_a:
					printf("0%d(libfunc)%d:%s\t",arg->type, arg->val, nameLibfuncs[arg->val]);
					break;
				case retval_a:
					printf("%d(retval)%d:\t",arg->type, arg->val);
					break;
				default:
					printf ("\t*****\t");
					break;
			}

}


int GenerateMagic(char *s){
    int i=0, Magic=0;
    for (i=0; i<strlen(s); i++){
	Magic += s[i];
    }

    return Magic;
}

void Write_Binary_Code(){

	FILE* bin = NULL;
	int MagicNum = GenerateMagic("Poutsa kai ksulo...Stis karioles Freno!!!");
	int tmpCurrInst=0, i;
	instruction * p;

	bin = fopen("BinaryCode.abc", "wb");
	if(!bin)
		fprintf(stderr, "Error, in opening the Binary File.\n");

	fwrite(&MagicNum, sizeof(int), 1, bin);


	if (fwrite(&totalStringConsts,sizeof(int),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	for(i=0; i<totalStringConsts; i++){
		if (fwrite(&stringConsts[i],sizeof(char *),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	}

	if (fwrite(&totalNumConsts,sizeof(int),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	for(i=0; i<totalNumConsts; i++){
		if (fwrite(&NumConsts[i],sizeof(double),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	}

	if (fwrite(&totalNamedLibfuncs,sizeof(int),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	for(i=0; i<totalNamedLibfuncs; i++){
		if (fwrite(&nameLibfuncs[i],sizeof(char *),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	}

	if (fwrite(&totalUserFuncs,sizeof(int),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	for(i=0; i<totalUserFuncs; i++){

		if (fwrite(&userFuncs[i].id,sizeof(char *),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
		if (fwrite(&userFuncs[i].address,sizeof(int),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
		if (fwrite(&userFuncs[i].localSize,sizeof(int),1,bin)!=1)
		  fprintf(stderr, "Error, while writing in the Binary file.\n");
	}

	 //print the number of instructions
	fwrite(&currInstr, sizeof(unsigned int), 1, bin);
	p = instructions;
	while(tmpCurrInst < currInstr){

		///Opcode
		if (fwrite(&(p->opcode),sizeof(int),1,bin)!=1)
				fprintf(stderr, "Error, while writing in the Binary file.\n");

		///result

		if (p->result)
		{
			if (fwrite(&(p->result->type),sizeof(int),1,bin)!=1)
				fprintf(stderr, "Error, while writing in the Binary file.\n");

			if (fwrite(&(p->result->val),sizeof(int),1,bin)!=1)
				fprintf(stderr, "Error, while writing in the Binary file.\n");
		}

		///arg1

		if(p->arg1)
		{
			if (fwrite(&(p->arg1->type),sizeof(int),1,bin)!=1)
				fprintf(stderr, "Error, while writing in the Binary file.\n");

			if (fwrite(&(p->arg1->val),sizeof(int),1,bin)!=1)
				fprintf(stderr, "Error, while writing in the Binary file.\n");
		}

		///arg2

		if(p->arg2)
		{
			if (fwrite(&(p->arg2->type),sizeof(int),1,bin)!=1)
				fprintf(stderr, "Error, while writing in the Binary file.\n");

			if (fwrite(&(p->arg2->val),sizeof(int),1,bin)!=1)
				fprintf(stderr, "Error, while writing in the Binary file.\n");
		}

		p+=1;
		tmpCurrInst++;
	}

	if(bin)	fclose(bin);
}

void TestPrintAllTables(FILE *bin, char ** strCon, unsigned totalS, double* NumCon, unsigned totalN,
				char **Libf, unsigned totalLibf, userFunc * userF, unsigned totalUserF){
		int i=0;

		fprintf(bin,"The Data in all the Tables:\n\n");

		fprintf(bin,"\tStringConsts:\n");
		for(i=0; i<totalS; i++){
			fprintf(bin,"\t\tIndex: %d,  Data: %s\n", i, strCon[i]);
		}
		fprintf(bin,"\tNumConsts:\n");
		for(i=0; i<totalN; i++){
			fprintf(bin,"\t\tIndex: %d, Data: %lf\n", i, NumCon[i]);
		}
		fprintf(bin,"\tLibFuncs:\n");
		for(i=0; i<totalLibf; i++){
			fprintf(bin,"\t\tIndex: %d,  Data: %s\n", i, Libf[i]);
		}
		fprintf(bin,"\tUserFuncs:\n");
		for(i=0; i<totalUserF; i++){
			fprintf(bin,"\t\tIndex: %d,  Data:\n\t\t\t\ttaddress: %d, localsize: %d, id: %s\n", i,
					userF[i].address, userF[i].localSize, userF[i].id );
		}
}

void Print_txt_Final_Code(char *txt, char ** strCon, unsigned totalS, double* NumCon, unsigned totalN,
				char **Libf, unsigned totalLibf, userFunc * userF, unsigned totalUserF, instruction *root, int total){

	FILE* bin = NULL;
	int MagicNum = GenerateMagic("Poutsa kai ksulo...Stis karioles Freno!!!");
	int tmpCurrInst=0, i;
	instruction * p;
	int a=666, b =667, c=668;
	bin = fopen(txt, "w");
	if(!bin){
		fprintf(stderr, "Error, in opening the Txt File.\n");
	}

	fprintf(bin,"magicnumber -> %d\n",MagicNum);
	fprintf(bin, "arrays -> strings number userfunctions libfunctions \n");


	TestPrintAllTables(bin, strCon, totalS, NumCon, totalN, Libf, totalLibf, userF, totalUserF);


	fprintf(bin, "Total instructions %d \n", total); //print the number of instructions
	p = root;
	while(tmpCurrInst < total){

		///Opcode
		fprintf(bin,"Opcode %d \n",(p->opcode));
		///result


		if (p->result)
		{
			fprintf(bin,"\tResult \n");
			fprintf(bin,"\t\ttype: %d \n",p->result->type);

			fprintf(bin,"\t\tval: %d \n",p->result->val);
		}
		///arg1

		if(p->arg1)
		{
			fprintf(bin,"\targ1 \n");
			fprintf(bin,"\t\ttype: %d \n",p->arg1->type);


			fprintf(bin,"\t\tval: %d \n",p->arg1->val);

		}
		///arg2

		if(p->arg2)
		{
			fprintf(bin,"\targ2 \n");
			fprintf(bin,"\t\ttype: %d \n",p->arg2->type);

			fprintf(bin,"\t\tval: %d \n",p->arg2->val);
		}

		p+=1;
		tmpCurrInst++;
	}
	if(bin)	fclose(bin);
}