#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include "final_code.h"

#define AVM_STACKSIZE 4096
#define AVM_WIPEOUT(m)  memset( &(m), 0, sizeof(m))
#define AVM_STACKENV_SIZE 4
#define AVM_ENDING_PC codeSize
#define AVM_MAX_INSTRUCTIONS nop_v

#define AVM_TABLE_HASHSIZE 211

#define AVM_NUMACTUALS_OFFSET 	+4
#define AVM_SAVEDPC_OFFSET	+3
#define AVM_SAVEDTOP_OFFSET	+2
#define AVM_SAVEDTOPSP_OFFSET	+1

unsigned totalActuals=0;

typedef void (*library_func_t) (void);
library_func_t avm_getlibraryfunc ( char *id);


library_func_t libraryFuncs[12]={0};

typedef enum avm_memcell_t {
  number_m, 	///0
  string_m,		///1
  bool_m,		///2
  table_m,		///3
  userfunc_m,	///4
  libfunc_m,	///5
  nil_m,		///6
  undef_m,		///7
}avm_memcell_t;
typedef struct avm_table
{
  unsigned 		refCounter;
  struct avm_table_bucket* 	strIndexed[AVM_TABLE_HASHSIZE];
  struct avm_table_bucket*	numIndexed[AVM_TABLE_HASHSIZE];
  unsigned		total;
}avm_table;

typedef struct avm_memcells{

  avm_memcell_t type;
  union
  {
    double numVal;
    char * strVal;
    unsigned int boolVal;
    avm_table * tableVal;
    unsigned int funcVal;
    char * libfuncVal;
  }data;
} avm_memcell;

typedef struct avm_table_bucket{
  avm_memcell	*key;
  avm_memcell 	*value;
  struct avm_table_bucket * next;
}avm_table_bucket;




typedef char * (*tostring_func_t) (avm_memcell*);
typedef unsigned char (*tobool_func_t) (avm_memcell *);
void printmemcell(avm_memcell* tmp);
void printbuckets(avm_table_bucket* tmp);
char * return_char_memcell(avm_memcell* tmp);
unsigned char number_tobool(avm_memcell *);
unsigned char string_tobool(avm_memcell*);
unsigned char bool_tobool(avm_memcell *);
unsigned char table_tobool(avm_memcell*);
unsigned char userfunc_tobool(avm_memcell*);
unsigned char libfunc_tobool(avm_memcell*);
unsigned char nil_tobool(avm_memcell*);
unsigned char undef_tobool(avm_memcell*);

char *number_tostring 	(avm_memcell *);
char *string_tostring 	(avm_memcell *);
char *bool_tostring	(avm_memcell *);
char *table_tostring   	(avm_memcell *);
char *userfunc_tostring	(avm_memcell *);
char *libfunc_tostring	(avm_memcell *);
char *nil_tostring	(avm_memcell *);
char *undef_tostring	(avm_memcell *);


tostring_func_t tostringFuncs[]=
{
  number_tostring,
  string_tostring,
  bool_tostring,
  table_tostring,
  userfunc_tostring,
  libfunc_tostring,
  nil_tostring,
  undef_tostring
};

tobool_func_t toboolFuncs[]=
{
  number_tobool,
  string_tobool,
  bool_tobool,
  table_tobool,
  userfunc_tobool,
  libfunc_tobool,
  nil_tobool,
  undef_tobool
};


void execute_assign(instruction*);
void execute_add(instruction*);
void execute_sub(instruction*);
void execute_mul(instruction*);
void execute_div(instruction*);
void execute_mod(instruction*);
void execute_jeq(instruction*);
void execute_jne(instruction*);
void execute_jle(instruction*);
void execute_jge(instruction*);
void execute_jlt(instruction*);
void execute_jgt(instruction*);
void execute_jump(instruction*);
void execute_call(instruction*);
void execute_pusharg(instruction*);
void execute_funcenter(instruction*);
void execute_funcexit(instruction*);
void execute_newtable(instruction*);
void execute_tablegetelem(instruction*);
void execute_tablesetelem(instruction*);
//void execute_nop(instruction*);
void execute_cycle(void);

void 			avm_calllibfunc (char *id);		//implemented
void 			avm_dec_top(void);			//implemented
void 			avm_push_envvalue(unsigned val);	//implemented
void 			avm_callsaveenviroment (void);		//implemented

///lib functions
void 			libfunc_print (void);			//implemented
void			libfunc_input (void);
void			libfunc_objectmemberkeys (void);
void			libfunc_objecttotalmembers (void);
void			libfunc_objectcopy (void);
void 			libfunc_totalarguments (void);		//implemented
void			libfunc_argument (void);
void 			libfunc_typeof (void);			//implemented
void			libfunc_strtonum (void);
void			libfunc_sqrt (void);
void			libfunc_cos (void);
void			libfunc_sin (void);


unsigned 		avm_get_envvalue(unsigned i);		//implemented

unsigned 		avm_totalactuals (void);		//implemented
avm_memcell* 		avm_getactual (unsigned i);		//implemented
void 			avm_registerlibfunc(char *id, library_func_t addr);///xreiazetai ylopoihsh

//struct avm_table;



char *typeStrings[]=
{
  "number", 	/// 0
  "string", 	/// 1
  "bool",   	/// 2
  "table",  	/// 3
  "userfunc",	/// 4
  "libfunc",  	/// 5
  "nil",		/// 6
  "undef"		/// 7
};

/**
 * functions prototypes
 */

typedef double (*arithmetic_func_t) (double x, double y);
typedef void (*memclear_func_t) (avm_memcell*);


double add_impl(double , double);
double sub_impl(double , double);
double mul_impl(double , double);
double div_impl(double , double);
double mod_impl(double , double);

arithmetic_func_t arithmeticFuncs[] = {
  0,
  add_impl,
  sub_impl,
  mul_impl,
  div_impl,
  mod_impl
 };
/**
        arithmetics
*/
void avm_assign(avm_memcell *lv, avm_memcell *rv);	//implemented


char *avm_tostring (avm_memcell *m);


/**
    tables
*/
avm_table* 	avm_tablenew (void);						//implemented
void 		avm_tabledestroy(avm_table *t);					//implemented
void 		avm_tablebucketsdestroy (avm_table_bucket ** p);		//implemented
void 		avm_tablebucketsinit(avm_table_bucket **p);			//implemented
void 		avm_tabledecrefcounter(avm_table *t);				//implemented
void 		avm_tableincrefcounter(avm_table *t);				//implemented

/** CHECK IT FIRST*/
avm_memcell* 	avm_tablegetelem( avm_table * table , avm_memcell * index);	///ylopoihsh
void avm_tablesetelem( avm_table *table, avm_memcell *index, avm_memcell *content);///ylopoihsh




void avm_initialize (void);
void avm_warning(char *format, ...);
void avm_error(char *format, ...);
void avm_memcellclear (avm_memcell *m);
void memclear_string(avm_memcell *m);
void memclear_table(avm_memcell *m);
char * libfuncs_used(unsigned  int index);

/* function table*/

memclear_func_t memclearFuncs[] =//
{
  0, /*number*/
  memclear_string,
  0, /*bool*/
  memclear_table,
  0, /*userfunc*/
  0, /*livfunc*/
  0, /*nil */
  0  /*undef*/
};
/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


/*stack variables*/
avm_memcell stack[AVM_STACKSIZE]; /// stack[4096]//
avm_memcell retval;avm_memcell ax, bx, cx;

unsigned int  top,topsp;
/*....*/
unsigned char 	executionFinished = 0;
unsigned int	pc = 0;
unsigned int	currLine = 0;
unsigned int	codeSize = 0;
instruction*	code = (instruction*) 0;
 ///TABLES declaration
char ** RstringConsts;//str
unsigned RtotalStringConsts=0;
double* RNumConsts;//num
unsigned RtotalNumConsts=0;
char **RnameLibfuncs;//lib func
unsigned RtotalNamedLibfuncs=0;
userFunc * RuserFuncs;//user func
unsigned RtotalUserFuncs=0;

static int aek =0;
unsigned int 	glbnum = 0;

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
static void avm_initstack(void)
{
  unsigned int i;
  for(i=0; i<AVM_STACKSIZE;i++)
  {
      AVM_WIPEOUT(stack[i]);
      stack[i].type = undef_m;
  }
}

void avm_registerlibfunc(char *id, library_func_t addr){
  int i=libfuncs_newfunc(id);
  if(i<=11 && i>=0)
      libraryFuncs[i]=addr;
  else
    printf("\nERROR LIBFUNC NOT FOUND\n");
}


library_func_t avm_getlibraryfunc (char *id){
  int i=libfuncs_newfunc(id);
  if(i<=11 && i>=0)
    return libraryFuncs[libfuncs_newfunc(id)];

  printf("\n11111ERROR LIBFUNC NOT FOUND\n");
  return NULL;
}

void avm_initialize (void)
{
  avm_initstack();
  avm_registerlibfunc("print", libfunc_print);
  avm_registerlibfunc("input", libfunc_input);
  avm_registerlibfunc("objectmemberkeys", libfunc_objectmemberkeys);
  avm_registerlibfunc("objecttotalmembers", libfunc_objecttotalmembers);
  avm_registerlibfunc("objectcopy", libfunc_objectcopy);
  avm_registerlibfunc("totalarguments", libfunc_totalarguments);
  avm_registerlibfunc("argument", libfunc_argument);
  avm_registerlibfunc("typeof", libfunc_typeof);
  avm_registerlibfunc("strtonum", libfunc_strtonum);
  avm_registerlibfunc("sqrt", libfunc_sqrt);
  avm_registerlibfunc("cos", libfunc_cos);
  avm_registerlibfunc("sin", libfunc_sin);
}
void avm_warning(char *format, ...)
{
  printf("\n Warning : %s \n", format);
  return;
}
void avm_error(char *format, ...)
{
  printf("\nError: %s ",format);
  executionFinished=1;
  return;
}


void avm_memcellclear (avm_memcell *m)
{
  memclear_func_t  f;
  if(m->type !=undef_m)
  {
     f = memclearFuncs[m->type];
    if(f)
      (*f)(m);
    m->type = undef_m;
  }
}

void memclear_string(avm_memcell *m)
{
  assert(m->data.strVal);
  free(m->data.strVal);
}

void memclear_table(avm_memcell *m)
{
  assert(m->data.tableVal);
  avm_tabledecrefcounter(m->data.tableVal);
}

avm_memcell * avm_translate_operand (vmarg *arg, avm_memcell *reg)
{

      switch (arg->type)
      {

        case global_a : return &stack[AVM_STACKSIZE -1- arg->val];
        case local_a  : return &stack[topsp - arg->val];
        case formal_a : /*printf("asdf %d  \n",topsp+AVM_STACKENV_SIZE +arg->val); getchar(); */return &stack[topsp + AVM_STACKENV_SIZE  + arg->val];
        case retval_a : return &retval;
        case number_a :
        {

          reg->type = number_m;
	  reg->data.numVal = RNumConsts[arg->val]; ///const num value
	//  printf("Number %lf \n",reg->data.numVal);
          return reg;
        }
        case string_a :
        {
          reg->type = string_m;
          reg->data.strVal = RstringConsts[arg->val]; ///const string value
          return reg;
        }
        case bool_a :
        {
          reg->type = bool_m;
          reg->data.boolVal = arg->val;
          return reg;
        }
        case nil_a :
        {
          reg->type = nil_m;
          return reg;
        }
        case userfunc_a:
        {
          reg->type = userfunc_m;



          reg->data.funcVal = arg->val ; ///index of userFunc array

          return reg;
        }
        case libfunc_a:
        {
           reg->type = libfunc_m;
           reg->data.libfuncVal = strdup(RnameLibfuncs[arg->val]);/// name of lib func
           return reg;
        }

    }
}


void execute_newtable (instruction *instr)
{
    avm_memcell * lv = avm_translate_operand(instr->result, (avm_memcell *) 0);
    assert ( lv && (&stack[top]   <= lv && &stack[AVM_STACKSIZE] > lv || lv == &retval));

    avm_memcellclear(lv);

    lv->type = table_m;
    lv->data.tableVal = avm_tablenew();
    avm_tableincrefcounter ( lv->data.tableVal);
}


void avm_tableincrefcounter(avm_table *t)
{
  ++t->refCounter;
}

void avm_tabledecrefcounter(avm_table *t)
{
  assert(t->refCounter > 0);
  if(! --t->refCounter)
    avm_tabledestroy(t);
}


void avm_tablebucketsinit(avm_table_bucket **p)
{
  unsigned i;
  for(i=0; i<AVM_TABLE_HASHSIZE; ++i)
    p[i] = (avm_table_bucket *) 0;
}


avm_table * avm_tablenew(void)
{
  avm_table *t = (avm_table *) malloc(sizeof(avm_table));
  AVM_WIPEOUT(*t);

  t->refCounter = t->total = 0;
  avm_tablebucketsinit(t->numIndexed);
  avm_tablebucketsinit(t->strIndexed);

  return t;
}


void avm_tablebucketsdestroy (avm_table_bucket ** p)
{
  unsigned i;
  avm_table_bucket *b;
  avm_table_bucket * del;
  for(i =0; i<AVM_TABLE_HASHSIZE;i++)
  {
    for(b = *p; b;)
    {
      del = b;
      b = b->next;
      avm_memcellclear(del->key);
      avm_memcellclear(del->value);
      free(del);
    }
    p[i] = (avm_table_bucket*) 0;
  }
}

void avm_tabledestroy(avm_table *t)
{
  avm_tablebucketsdestroy(t->strIndexed);
  avm_tablebucketsdestroy(t->numIndexed);
  free(t);
}



void execute_tablegetelem ( instruction *instr)
{

  avm_memcell* lv = avm_translate_operand(instr->result,(avm_memcell*)0);
	avm_memcell* t  = avm_translate_operand(instr->arg1,(avm_memcell*)0);
	avm_memcell* i  = avm_translate_operand(instr->arg2, &ax);

	avm_memcell* content;

	assert(lv && (&stack[top] <= lv && &stack[AVM_STACKSIZE] > lv || lv==&retval));
	assert(t && &stack[top] <= t && &stack[AVM_STACKSIZE] >t);

	avm_memcellclear(lv);
	lv->type=nil_m;
  if(t->type != table_m)
  {
    printf("\nError: illegal use of type %s as table!", typeStrings[t->type]);
    executionFinished=1;
  }
  else
  {
   content = avm_tablegetelem (t->data.tableVal, i);
    if(content)
      avm_assign(lv, content);
    else
    {
      char *ts = avm_tostring(t);
      char *is = avm_tostring(i);
      avm_warning("%s [%s] not found! ", ts, is);
      free(ts);
      free(is);
    }
  }
}

void execute_tablesetelem( instruction *instr)
{
  avm_memcell * t = avm_translate_operand(instr->result, (avm_memcell *) 0);
  avm_memcell * i = avm_translate_operand( instr->arg1, &ax);
  avm_memcell * c = avm_translate_operand( instr->arg2, &bx);

  assert(t && &stack[top] <= t && &stack[AVM_STACKSIZE] > t);
  assert ( i && c);

  if(t->type != table_m)
  {
    printf("\nError: illegal use of type %s as table ! ", typeStrings[t->type]);
    executionFinished=1;
  }
  else
  {
    avm_tablesetelem(t->data.tableVal, i, c);
   // printf("%lf %lf \n", t->data.tableVal->numIndexed[0]->key->data.numVal,t->data.tableVal->numIndexed[0]->value->data.numVal);
  }
}


unsigned int avm_hashfunc(avm_memcell* index){

   unsigned int hash = 0;
   unsigned char *p;

	if (index->type == number_m)
		return((int)index->data.numVal % AVM_TABLE_HASHSIZE);
	else if(index->type == string_m){
	 	for( p=(unsigned char *)index->data.strVal ; *p!='\0' ; p++ ){
	    	hash = 31*hash + *p;
	 	}
	 	return(hash%AVM_TABLE_HASHSIZE);
	}

	else return -1;
}


avm_memcell* avm_tablegetelem(avm_table* table, avm_memcell* index){

	unsigned int i;
   	avm_table_bucket* curr;
	avm_memcell* tmp;
	i = avm_hashfunc(index);
	int a,b;

   	if(i<0){
	  avm_error("invalid array index!\n");
   	}
   	else{
	  if(index->type == number_m){
		curr = table->numIndexed[i];
		while(curr){
		  tmp = (avm_memcell*)curr->key;
		  if(tmp->data.numVal==index->data.numVal)
			return (avm_memcell*)curr->value;

		   curr = curr->next;
		}
	  }
	  else{
		  curr = table->strIndexed[i];
		  tmp =(avm_memcell*) curr->key;
		  while(curr!=NULL){
		    if(!strcmp(tmp->data.strVal,index->data.strVal))
		      return (avm_memcell*)curr->value;
		    curr = curr->next;
		  }
	  }
	}
   return 0;
}
avm_table_bucket * insertNewTableBucket_int(avm_table_bucket *head, avm_memcell *index, avm_memcell *content)
{
    avm_table_bucket *temp = head, *newNode;

    newNode=(avm_table_bucket*)malloc(sizeof(struct avm_table_bucket));
    newNode->key=(avm_memcell *)malloc(sizeof(avm_memcell));
    newNode->value=(avm_memcell *)malloc(sizeof(avm_memcell));
     newNode->key->type = number_m;
     newNode->next = NULL;
     newNode->value->type=content->type;
     newNode->key->data.numVal = index->data.numVal;

    if(content->type == number_m)
      newNode->value->data.numVal = content->data.numVal;
    else if(content->type == string_m){
      newNode->value->data.strVal = content->data.strVal;}
    else if(content->type == libfunc_m)
       newNode->value->data.libfuncVal = strdup(content->data.libfuncVal);
    else if (content->type == bool_m)
	 newNode->value->data.boolVal = content->data.boolVal;
    else if (content->type == userfunc_m)
	 newNode->value->data.funcVal= content->data.funcVal;
    else{
	 newNode->value->data.tableVal = content->data.tableVal;//self
	 avm_tableincrefcounter( content->data.tableVal);
	 avm_tableincrefcounter( newNode->value->data.tableVal);
	}


       // avm_assign((avm_memcell*)newNode->key, index);
   //   avm_assign((avm_memcell*)newNode->value, content);
    if(head==NULL){

      return newNode;
    }
    while(temp->next)
      temp = temp->next;

      temp->next = newNode;

    return head;

}
avm_table_bucket * insertNewTableBucket_str(avm_table_bucket *head, avm_memcell *index, avm_memcell *content)
{
  avm_table_bucket *temp = head, *newNode;

    newNode=(avm_table_bucket*)malloc(sizeof(struct avm_table_bucket));
    newNode->key=(avm_memcell *)malloc(sizeof(avm_memcell));
    newNode->value=(avm_memcell *)malloc(sizeof(avm_memcell));
     newNode->next = NULL;
     newNode->key->type= string_m;
     newNode->value->type = content->type;
     newNode->key->data.strVal = strdup(index->data.strVal);

    if(content->type == number_m){printf("Newnode %s \n",newNode->key->data.strVal);
      newNode->value->data.numVal = content->data.numVal;}
    else if(content->type == string_m){
      newNode->value->data.strVal = strdup(content->data.strVal);}
    else if(content->type == libfunc_m)
       newNode->value->data.libfuncVal = strdup(content->data.libfuncVal);
    else if (content->type == bool_m)
	 newNode->value->data.boolVal = content->data.boolVal;
    else if (content->type == userfunc_m)
	 newNode->value->data.funcVal= content->data.funcVal;
    else{
	 newNode->value->data.tableVal = content->data.tableVal;//self
	 avm_tableincrefcounter( content->data.tableVal);
	 avm_tableincrefcounter( newNode->value->data.tableVal);
	}


   // avm_assign((avm_memcell*)newNode->key, index);
   //   avm_assign((avm_memcell*)newNode->value, content);
  if(head==NULL)
    return newNode;

  while(temp->next)
  {

    temp = temp->next;

  }
    temp->next = newNode;

  return head;

}
void avm_tablesetelem(avm_table* table, avm_memcell* index, avm_memcell* content){

	unsigned int i;
   	avm_table_bucket* p;

	avm_memcell* curr=NULL;
   	i = avm_hashfunc(index);


   	if(i == -1){
	  avm_error("invalid array index!\n");
   	}
   	else
	{



	  if(index->type == number_m)
	  {
	    //printf("Newnode %s \n",newNode->key->data.strVal);
	      if(content->type!= nil_m)
	      {

		  table->numIndexed[i] = insertNewTableBucket_int(table->numIndexed[i],index,content);
	      }



	  }
	  else
	  {
	          if(content->type!= nil_m)
		{

		  table->strIndexed[i] = insertNewTableBucket_str(table->strIndexed[i],index,content);
		}
	  }


	}



}




double add_impl (double x, double y)
{
  return x+y;
}
double sub_impl (double x, double y)
{
  return x-y;
}
double mul_impl (double x, double y)
{
  return x*y;
}
double div_impl (double x, double y)
{
  return x/y;
}
double mod_impl (double x, double y)
{
  return (((unsigned)x)%((unsigned)y));
}

void execute_assign(instruction *instr)
{
  avm_memcell * lv = avm_translate_operand(instr->result, (avm_memcell*) 0);
  avm_memcell * rv = avm_translate_operand(instr->arg1, &ax);

  assert(lv && (&stack[top] <= lv && &stack[AVM_STACKSIZE] > lv || lv == &retval));
  assert(rv);


  avm_assign(lv,rv);

}

void avm_assign(avm_memcell *lv, avm_memcell *rv)
{
  if( lv == rv )
    return;

  if( lv->type == table_m &&
      rv->type == table_m
    &&  lv->data.tableVal == rv->data.tableVal

  )
      return;



  if ( rv->type == undef_m )
    avm_warning("assigning \"from undef\" content! \n");

  avm_memcellclear(lv);

  memcpy(lv,rv,sizeof(avm_memcell));

  if(lv->type ==  string_m)
    lv->data.strVal = strdup(rv->data.strVal);
  else if (lv->type ==table_m){
    avm_tableincrefcounter( lv->data.tableVal);

  }
// else if(lv->type == number_m)
// lv->data.numVal = rv->data.numVal;

   lv->type= rv->type;
//printf("% \n", lv->data.strVal);
}

void execute_arithmetic (instruction *instr)
{
  avm_memcell *lv = avm_translate_operand(instr->result, (avm_memcell *) 0);
  avm_memcell *rv1 = avm_translate_operand(instr->arg1, &ax);
  avm_memcell *rv2 = avm_translate_operand(instr->arg2, &bx);

  assert(lv && (&stack[top] <= lv && &stack[AVM_STACKSIZE] > lv || lv == &retval));
  assert(rv1 && rv2);

  if(rv1->type != number_m || rv2->type != number_m)
  {
    avm_error("not a number in arithmetic !\n");
    executionFinished = 1;
  }
  else
  {

    arithmetic_func_t op = arithmeticFuncs[instr->opcode];
    avm_memcellclear(lv);
    lv->type = number_m;

    lv->data.numVal = (*op) (rv1->data.numVal, rv2->data.numVal);
  }
}


unsigned char number_tobool(avm_memcell *m)
{
  return m->data.numVal !=0;
}
unsigned char string_tobool(avm_memcell*m)
{
  return m->data.strVal[0] !=0;
}
unsigned char bool_tobool(avm_memcell *m)
{
  return m->data.boolVal;
}
unsigned char table_tobool(avm_memcell*m)
{
  return 1;
}
unsigned char userfunc_tobool(avm_memcell*m)
{
  return 1;
}
unsigned char libfunc_tobool(avm_memcell*m)
{
  return 1;
}
unsigned char nil_tobool(avm_memcell*m)
{
  return 0;
}
unsigned char undef_tobool(avm_memcell*m)
{
  assert(0);
  return 0;
}
unsigned char avm_tobool (avm_memcell *m)
{
  assert(m->type >=0 && m->type <undef_m);
  return (*toboolFuncs[m->type])(m);
}

char *avm_tostring (avm_memcell *m)
{
  assert (m->type >=0 && m->type <=undef_m);
  return (*tostringFuncs[m->type]) (m);
}


char *number_tostring 	(avm_memcell *m)
{
  assert(m &&  m->type == number_m);
  char *buf = malloc (15*sizeof(char));
  sprintf(buf, "%lf", m->data.numVal);
  return buf;
}
char *string_tostring 	(avm_memcell *m)
{
  assert(m&& m->type == string_m);
  char *buf = malloc(15*sizeof(char));
  buf = strdup (m->data.strVal);
  return buf;
}
char *bool_tostring	(avm_memcell *m)
{
  assert(m && m->type == bool_m);
  if(m->data.boolVal ==0)
    return "false";
  else
    return "true";
}
void clear_str(char **ar)
{
  int i;
  for(i=0;i<strlen(*ar);i++)
    *ar[i]='\0';
}

char * print_table(avm_memcell *m)
{
    char s[499]={'\0'}, *temp_str;

  int i =0;
  avm_memcell *tmp = m;
  strcat(s,"[");
   for(i=0;i<AVM_TABLE_HASHSIZE;i++)
   {

	
	if(tmp->data.tableVal->strIndexed[i]!=NULL)
	{
	
	   strcat(s,"{ ");

	   if(tmp->data.tableVal->strIndexed[i]->key->type == table_m)
	   {
			if (tmp->data.tableVal->strIndexed[i]->key)
				strcat (s, print_table(tmp->data.tableVal->strIndexed[i]->key) );
	   }
	   else{
			temp_str = return_char_memcell(tmp->data.tableVal->strIndexed[i]->key);
			strcat(s,temp_str);
	   }

	   strcat(s, ": ");

	   if(tmp->data.tableVal->strIndexed[i]->value->type == table_m)
	   {
			if (tmp->data.tableVal->strIndexed[i]->value)
				strcat (s, print_table(tmp->data.tableVal->strIndexed[i]->value) );
	   }
	   else{
			temp_str =  return_char_memcell(tmp->data.tableVal->strIndexed[i]->value);
			strcat(s,temp_str);
	   }

	    strcat(s,"} ");

	   tmp->data.tableVal->strIndexed[i] = tmp->data.tableVal->strIndexed[i]->next;
	}
	

	if(tmp->data.tableVal->numIndexed[i]!=NULL)
	{
	   strcat(s,"{ ");

	   if(tmp->data.tableVal->numIndexed[i]->key->type == table_m)
	   {
			if (tmp->data.tableVal->numIndexed[i]->key)
				strcat (s, print_table(tmp->data.tableVal->numIndexed[i]->key) );
	   }
	   else{
			temp_str = return_char_memcell(tmp->data.tableVal->numIndexed[i]->key);
			strcat(s,temp_str);
	   }	  

	   strcat(s, ": ");

	   if(tmp->data.tableVal->numIndexed[i]->value->type == table_m)
	   {
			if (tmp->data.tableVal->numIndexed[i]->value)
				strcat (s, print_table(tmp->data.tableVal->numIndexed[i]->value) );
	   }
	   else{
			temp_str = return_char_memcell(tmp->data.tableVal->numIndexed[i]->value);
			strcat(s,temp_str);
	   }	  
	    strcat(s,"} ");

	   tmp->data.tableVal->numIndexed[i] = tmp->data.tableVal->numIndexed[i]->next;
	}
    }
    strcat(s,"]");
   temp_str = strdup(s);
   return temp_str;
}

char *table_tostring   	(avm_memcell *m)/*******************************************************************/
{
  assert(m && m->type == table_m);

   return print_table(m);
}

char * return_char_memcell(avm_memcell* tmp)
{
    int i;
    char its[50];
    char *string;
	
  if(tmp->type==string_m){
   return tmp->data.strVal;
  }
  else if(tmp->type==bool_m)
  {
    sprintf(its,"%d ",tmp->data.boolVal);

  }
  else if(tmp->type==userfunc_m)
  {
   sprintf(its,"%d ",tmp->data.funcVal);

  }
  else if(tmp->type==libfunc_m)
  {
   return tmp->data.libfuncVal;
  }
  else if(tmp->type==number_m)
  {
     sprintf(its,"%d ",(int)tmp->data.numVal);

  }
  string = strdup(its);

  return string;
}
char *userfunc_tostring	(avm_memcell *m)
{
  assert(m && m->type == userfunc_m);
  char *buf = malloc(50*sizeof(char));
  sprintf(buf, "%d", m->data.funcVal);
  return buf;
}
char *libfunc_tostring	(avm_memcell *m)
{
  	assert(m && m->type == libfunc_m);
	char *buf;
	buf = strdup(m->data.libfuncVal);
	return buf;
}
char *nil_tostring	(avm_memcell *m)
{
  assert(m && m->type == nil_m);
  return "nil";
}
char *undef_tostring	(avm_memcell *m)
{
  assert(m && m->type == undef_m);
  return "undef";
}



void execute_jeq (instruction *instr)
{
  assert(instr->result->type == label_a);

  avm_memcell *rv1 =avm_translate_operand (instr->arg1, &ax);
  avm_memcell *rv2 = avm_translate_operand(instr->arg2, &bx);

  unsigned char result =0;

  if(rv1->type == undef_m || rv2->type == undef_m){
	avm_error("\"undef\" involved in equality!\n");
	executionFinished=1;
  }
  else if (rv1->type == nil_m || rv2->type == nil_m)
    result = rv1->type == nil_m && rv2->type == nil_m;
  else if ( rv1->type == bool_m || rv2->type == bool_m)
    result = (avm_tobool(rv1) == avm_tobool(rv2));
  else if(rv1->type != rv2->type){
    printf("\nError: %s == %s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
    executionFinished=1;
  }
  else
  {
    if(rv1->type == number_m && rv2->type == number_m)
 		result = rv1->data.numVal == rv2->data.numVal;
    if(rv1->type == string_m )
		result = !strcmp(rv1->data.strVal,rv2->data.strVal);
    if(rv1->type == table_m)
		result = (rv1->data.tableVal == rv2->data.tableVal);
    if(rv1->type == userfunc_m)
		result = rv1->data.funcVal == rv2->data.funcVal ;
    if(rv1->type == libfunc_m)
		result = !strcmp(rv1->data.libfuncVal, rv2->data.libfuncVal);
      }


  if(!executionFinished && result )
    pc = instr->result->val;
}


void execute_jne (instruction *instr)
{
  assert(instr->result->type == label_a);

  avm_memcell *rv1 =avm_translate_operand (instr->arg1, &ax);
  avm_memcell *rv2 = avm_translate_operand(instr->arg2, &bx);

  unsigned char result =0;

  if(rv1->type == undef_m || rv2->type == undef_m){
	avm_error("\"undef\" involved in equality!\n");
	executionFinished=1;
  }
  else if (rv1->type == nil_m || rv2->type == nil_m)
    result = rv1->type != nil_m && rv2->type != nil_m;
  else if ( rv1->type == bool_m || rv2->type == bool_m)
    result = (avm_tobool(rv1) != avm_tobool(rv2));
  else if(rv1->type != rv2->type){
    printf("\nError: %s == %s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
    executionFinished=1;
  }
  else
  {
    if(rv1->type == number_m && rv2->type == number_m)
 		result = rv1->data.numVal != rv2->data.numVal;
    if(rv1->type == string_m )
		result = strcmp(rv1->data.strVal,rv2->data.strVal);
    if(rv1->type == table_m)
		result = (rv1->data.tableVal != rv2->data.tableVal);
    if(rv1->type == userfunc_m)
		result = rv1->data.funcVal != rv2->data.funcVal ;
    if(rv1->type == libfunc_m)
		result = strcmp(rv1->data.libfuncVal, rv2->data.libfuncVal);
    }
  if(!executionFinished && result )
    pc = instr->result->val;
}

void execute_jle (instruction *instr)
{
  assert(instr->result->type == label_a);

  avm_memcell *rv1 =avm_translate_operand (instr->arg1, &ax);
  avm_memcell *rv2 = avm_translate_operand(instr->arg2, &bx);

  unsigned char result =0;

  if(rv1->type == undef_m || rv2->type == undef_m){
	avm_error("\"undef\" involved in equality!\n");
	executionFinished=1;
  }
  else if (rv1->type == nil_m || rv2->type == nil_m)
    result = rv1->type <= nil_m && rv2->type <= nil_m;
  else if ( rv1->type == bool_m || rv2->type == bool_m)
    result = (avm_tobool(rv1) <= avm_tobool(rv2));
  else if(rv1->type != rv2->type){
    printf("\nError: %s == %s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
    executionFinished=1;
  }
  else
  {
if(rv1->type == number_m && rv2->type == number_m)
 		result = rv1->data.numVal <= rv2->data.numVal;
    if(rv1->type == string_m )
		result = strcmp(rv1->data.strVal,rv2->data.strVal);///
    if(rv1->type == table_m)
		result = (rv1->data.tableVal <= rv2->data.tableVal);
    if(rv1->type == userfunc_m)
		result = rv1->data.funcVal <= rv2->data.funcVal ;
    if(rv1->type == libfunc_m)
		result = strcmp(rv1->data.libfuncVal, rv2->data.libfuncVal);///
  }
    if(!executionFinished && result )
    pc = instr->result->val;
}


void execute_jge (instruction *instr)
{
  assert(instr->result->type == label_a);

  avm_memcell *rv1 =avm_translate_operand (instr->arg1, &ax);
  avm_memcell *rv2 = avm_translate_operand(instr->arg2, &bx);

  unsigned char result =0;

  if(rv1->type == undef_m || rv2->type == undef_m){
	avm_error("\"undef\" involved in equality!\n");
	executionFinished=1;
  }
  else if (rv1->type == nil_m || rv2->type == nil_m)
    result = rv1->type >= nil_m && rv2->type >= nil_m;
  else if ( rv1->type == bool_m || rv2->type == bool_m)
    result = (avm_tobool(rv1) >= avm_tobool(rv2));
  else if(rv1->type != rv2->type){
    printf("\nError: %s == %s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
    executionFinished=1;
  }
  else
  {
  if(rv1->type == number_m && rv2->type == number_m)
 		result = rv1->data.numVal >= rv2->data.numVal;
    if(rv1->type == string_m )
		result = strcmp(rv1->data.strVal,rv2->data.strVal);
    if(rv1->type == table_m)
		result = (rv1->data.tableVal >= rv2->data.tableVal);
    if(rv1->type == userfunc_m)
		result = rv1->data.funcVal >= rv2->data.funcVal ;
    if(rv1->type == libfunc_m)
		result = strcmp(rv1->data.libfuncVal, rv2->data.libfuncVal);

  }
    if(!executionFinished && result )
    pc = instr->result->val;
}


void execute_jlt (instruction *instr)
{
  assert(instr->result->type == label_a);

  avm_memcell *rv1 =avm_translate_operand (instr->arg1, &ax);
  avm_memcell *rv2 = avm_translate_operand(instr->arg2, &bx);

  unsigned char result =0;

  if(rv1->type == undef_m || rv2->type == undef_m){
	avm_error("\"undef\" involved in equality!\n");
	executionFinished=1;
  }
  else if (rv1->type == nil_m || rv2->type == nil_m)
    result = rv1->type < nil_m && rv2->type < nil_m;
  else if ( rv1->type == bool_m || rv2->type == bool_m)
    result = (avm_tobool(rv1) < avm_tobool(rv2));
  else if(rv1->type != rv2->type){
    printf("\nError: %s == %s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
    executionFinished=1;
  }
  else
  {
    if(rv1->type == number_m && rv2->type == number_m)
 		result = rv1->data.numVal < rv2->data.numVal;
    if(rv1->type == string_m )
		result = strcmp(rv1->data.strVal,rv2->data.strVal);
    if(rv1->type == table_m)
		result = (rv1->data.tableVal < rv2->data.tableVal);
    if(rv1->type == userfunc_m)
		result = rv1->data.funcVal < rv2->data.funcVal ;
    if(rv1->type == libfunc_m)
		result = strcmp(rv1->data.libfuncVal, rv2->data.libfuncVal);
  }
    if(!executionFinished && result )
    pc = instr->result->val;
}


void execute_jgt (instruction *instr)
{
  assert(instr->result->type == label_a);

  avm_memcell *rv1 =avm_translate_operand (instr->arg1, &ax);
  avm_memcell *rv2 = avm_translate_operand(instr->arg2, &bx);

  unsigned char result =0;

  if(rv1->type == undef_m || rv2->type == undef_m){
	avm_error("\"undef\" involved in equality!\n");
	executionFinished=1;
  }
  else if (rv1->type == nil_m || rv2->type == nil_m)
    result = rv1->type > nil_m && rv2->type > nil_m;
  else if ( rv1->type == bool_m || rv2->type == bool_m)
    result = (avm_tobool(rv1) > avm_tobool(rv2));
  else if(rv1->type != rv2->type){
    printf("\nError: %s == %s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
    executionFinished=1;
  }
  else
  {
    if(rv1->type == number_m && rv2->type == number_m)
 		result = rv1->data.numVal > rv2->data.numVal;
    if(rv1->type == string_m )
		result = strcmp(rv1->data.strVal,rv2->data.strVal);
    if(rv1->type == table_m)
		result = (rv1->data.tableVal > rv2->data.tableVal);
    if(rv1->type == userfunc_m)
		result = rv1->data.funcVal > rv2->data.funcVal ;
    if(rv1->type == libfunc_m)
		result = strcmp(rv1->data.libfuncVal, rv2->data.libfuncVal);

  }
    if(!executionFinished && result )
    pc = instr->result->val;
}


void execute_jump(instruction *instr)
{
    if(!executionFinished && instr->result )
	pc = instr->result->val;


}

void 	execute_call(instruction *instr)
{
  avm_memcell * func =avm_translate_operand(instr->arg1, &ax);
  assert(func);
  avm_callsaveenviroment();


  switch (func->type)
  {
    case userfunc_m :
    {
      printf("ton pinw\n");
      printf("func->data.funcVal %d \n", func->data.funcVal);
      pc = RuserFuncs[func->data.funcVal].address;
      assert (pc< AVM_ENDING_PC);
      assert (code[pc].opcode == funcenter_v);
      break;
    }
    case string_m :
      avm_calllibfunc(func->data.strVal);
      break;
    case libfunc_m:
      avm_calllibfunc(func->data.libfuncVal);
       break;
    default:
    {
      char *s = avm_tostring(func);
      printf("\nError: call : cannot bind \"%s\" to function! \n", s);
      free(s);
      executionFinished=1;
      break;
    }
  }
}


void execute_pusharg (instruction *instr)
{
  avm_memcell *arg = avm_translate_operand(instr->arg1, &ax);
  assert(arg);

  avm_assign(&stack[top], arg);
  ++totalActuals;

  avm_dec_top();

}

void execute_funcenter (instruction *instr)
{
  avm_memcell *func = avm_translate_operand(instr->result, &ax);
  assert(func);
  assert(pc == RuserFuncs[func->data.funcVal].address);
  totalActuals = 0;

  topsp = top;
  top = top - RuserFuncs[func->data.funcVal].localSize;

}

void avm_calllibfunc (char *id)
{
  library_func_t f = avm_getlibraryfunc(id);
  if(!f)
  {
    printf("unsupported lib func \"%s\" called!\n", id);
    executionFinished = 1;
  }
  else
  {
    topsp = top;
    totalActuals = 0;

    (*f)();
    if ( !executionFinished )
      execute_funcexit ((instruction *) 0);
  }
}

unsigned avm_totalactuals (void)
{
  return avm_get_envvalue(topsp + AVM_NUMACTUALS_OFFSET);
}

avm_memcell* avm_getactual (unsigned i)
{
  assert (i < avm_totalactuals());
  return &stack[topsp + AVM_STACKENV_SIZE + 1 + i];
}


///implement lib functions.
void libfunc_print (void)
{
  unsigned n = avm_totalactuals(), i;
  char *s='\0';
  for (i=0; i<n; i++){
    s = strdup(avm_tostring(avm_getactual(i)));
    if(strcmp(s,"undef")){
      puts(s);
      free(s);
    }
    else{
      printf("nil\n");
    }
  }
}

void	libfunc_input (void){

  char c[100];
  int i, flag=0;
  printf("Input Here :");
  fgets(c,100, stdin);
  c[strlen(c)-1]='\0';

  if(c[0] == '"' && c[strlen(c)-1]=='"'){
    //string
    retval.type= string_m;
    retval.data.strVal = strdup(c);
  }
  else if (strcmp(c,"true")==0 || strcmp(c, "false")==0){
    //boolean
    retval.type = bool_m;
    if(!strcmp(c,"true"))
      retval.data.boolVal = 1;
    else
      retval.data.boolVal = 0;
  }
  else if(isdigit(c[0])){

    for(i=0;i<strlen(c); i++){
      if(isalpha(c[i])){
			flag=1;
			break;
      }
    }

    if(flag==0){
      //int
      retval.data.numVal = atof(c);
      retval.type=number_m;
    }
    else{
      avm_warning("Wrong input %s \n", c);
      retval.type=nil_m;
    }
  }
  else{
    avm_warning("Wrong input %s \n", c);
    retval.type=nil_m;
  }

}


void	libfunc_objectmemberkeys (void)
{
    avm_memcell *cell=avm_getactual(0);
    int i=avm_totalactuals(), j=0, element=0;
    char *temp_str;
	avm_memcell *tmpCell, tmpRet, *index = (avm_memcell *) malloc (sizeof (avm_memcell) );
    if(cell->type!=table_m){
	avm_error("invalid type of argument in objecttotalmembers");
	executionFinished=1;
	return ;
    }
    if(i!=1){
	executionFinished=1;
	printf("\nError: one argument (not %d) expected in \"objecttotalmembers\" !\n", i);
	return ;
    }

	retval.type=table_m;
    retval.data.tableVal=avm_tablenew();
    //retval.data.tableVal=cell->data.tableVal;
	tmpRet = retval;
	tmpCell = cell;
	
	
  index->type = number_m;
   
  for(i=0;i<AVM_TABLE_HASHSIZE;i++)
   {

	
		if(tmpCell->data.tableVal->strIndexed[i]!=NULL)
		{
			
		   index->data.numVal = j++;
		   
		   tmpRet.data.tableVal->numIndexed[i] = insertNewTableBucket_int(tmpRet.data.tableVal->numIndexed[i], index, 
					tmpCell->data.tableVal->strIndexed[i]->key);
		   
		   tmpCell->data.tableVal->strIndexed[i] = tmpCell->data.tableVal->strIndexed[i]->next;
		   
		}
		
	    
		if(tmpCell->data.tableVal->numIndexed[i]!=NULL)
		{
		   index->data.numVal = j++;
		   
		   tmpRet.data.tableVal->numIndexed[i] = insertNewTableBucket_int(tmpRet.data.tableVal->numIndexed[i], index, 
					tmpCell->data.tableVal->numIndexed[i]->key);
		   
		   tmpCell->data.tableVal->numIndexed[i] = tmpCell->data.tableVal->numIndexed[i]->next;
		}
		
   }
}

void libfunc_objecttotalmembers (void){
    int result;
    avm_memcell *cell=avm_getactual(0);
    int i=avm_totalactuals();
    if(cell->type!=table_m){
	avm_error("invalid type of argument in objecttotalmembers");
    }
    if(i!=1){
	executionFinished=1;
	printf("\nError: one argument (not %d) expected in \"objecttotalmembers\" !\n", i);
    }
    else
    {
	result=cell->data.tableVal->total;
	retval.type=number_m;
	retval.data.numVal=result;
    }
}

void libfunc_objectcopy (void){
  avm_memcell *cell=avm_getactual(0);
  int i=avm_totalactuals();
  if(cell->type!=table_m){
	avm_error("invalid type of argument in objectcopy");
    }
  if(i!=1){
	executionFinished=1;
	printf("\nError: one argument (not %d) expected in \"objectcopy\" !\n", i);
  }
  else{
    retval.type=table_m;
    retval.data.tableVal=avm_tablenew();
    retval.data.tableVal=cell->data.tableVal;
  }
}

void libfunc_totalarguments (void)
{
  unsigned p_topsp =avm_get_envvalue (topsp + AVM_SAVEDTOP_OFFSET);
  avm_memcellclear (&retval);
  
  if(!p_topsp)
  {
    avm_error("\"totalarguments\" called outside a function!\n");
    retval.type = nil_m;
  }
  else
  {
    retval.type = number_m;
    retval.data.numVal = avm_get_envvalue (p_topsp + 2*AVM_NUMACTUALS_OFFSET)  ;
  }
}

void  libfunc_argument (void){
  unsigned n = avm_totalactuals(), i;
  char *s;
  double result;
  avm_memcell *cell;
  unsigned p_topsp =avm_get_envvalue (topsp + AVM_SAVEDTOP_OFFSET);
  avm_memcellclear (&retval);
  
  if(n!=1){
    printf("\nError: one argument (not %d) expected in \"argument\" !\n", n);
	executionFinished =1;
	return ;
  }

  cell=avm_getactual(0);
  if (cell->type != number_m){
	printf("\nError: argument of type number_m expected in \"argument\" !\n");
	executionFinished =1;
	return ;
  }

  s = strdup (avm_tostring(avm_getactual(0)) );
  result = atoi(s);
  if (result<0 || result > avm_get_envvalue (p_topsp + 2*AVM_NUMACTUALS_OFFSET) ){
	retval.type=nil_m;
	retval.data.strVal = "nil_m";
  }
  else{
	  if(!p_topsp)
	  {
		avm_error("\"totalarguments\" called outside a function!\n");
		retval.type = nil_m;
	  }
	  else
	  {
		retval.type = number_m;
		retval.data.numVal = avm_get_envvalue (p_topsp + 2*AVM_NUMACTUALS_OFFSET + result+1)  ;
	  }
  }
}

void libfunc_typeof (void)
{
  unsigned n = avm_totalactuals();
  if(n!=1){
    printf("\nError: one argument (not %d) expected in \"typeof\" !\n", n);
    executionFinished=1;
  }
  else
  {
    avm_memcellclear(&retval);
    retval.type=string_m;
    retval.data.strVal = strdup (typeStrings [avm_getactual(0)->type]);
  }
}


void libfunc_strtonum(void){
  int i=avm_totalactuals();
  int result=0;
  avm_memcell *cell=avm_getactual(0);
  char *s=strdup(cell->data.strVal);
  if(cell->type!=string_m){
	avm_error("invalid type of argument in strtonum");
  }
  if(i!=1){
	executionFinished=1;
	printf("\nError: one argument (not %d) expected in \"strtonum\" !\n", i);
  }
  else{
	result=atof(s);
	retval.type=number_m;
	retval.data.numVal=result;
  }
}

void			libfunc_sqrt (void){
  unsigned n = avm_totalactuals(), i;
  char *s;
  double result;
  avm_memcell *cell;
  if(n!=1){
    printf("\nError: one argument (not %d) expected in \"sqrt\" !\n", n);
	executionFinished =1;
	return ;
  }

  cell=avm_getactual(0);
  if (cell->type != number_m){
	printf("\nError: argument of type number_m expected in \"sqrt\" !\n");
	executionFinished =1;
	return ;
  }

  s = strdup (avm_tostring(avm_getactual(0)) );
  result = atof(s);
  if (result<0){
	retval.type=nil_m;
	retval.data.strVal = "nil_m";
  }
  else{
	retval.type=number_m;
	retval.data.numVal = sqrt(result);
  }
  //free(s);
}

void			libfunc_cos (void){
  unsigned n = avm_totalactuals(), i;
  char *s;
  double result;

  avm_memcell *cell;
  if(n!=1){
    printf("\nError: one argument (not %d) expected in \"cos\" !\n", n);
	executionFinished =1;
	return ;
  }

  cell=avm_getactual(0);
  if (cell->type != number_m){
	printf("\nError: argument of type number_m expected in \"cos\" !\n");
	executionFinished =1;
	return ;
  }

  s = strdup (avm_tostring(avm_getactual(0)) );
  result = atof(s);

  if (result<0 || result>360){
	retval.type=nil_m;
	retval.data.strVal = "nil_m";
  }
  else{
	retval.type=number_m;
	retval.data.numVal = cos(result);
	//printf("COS(%lf)= %lf ", result, sin(result) );
  }
  //free(s);
}

void			libfunc_sin (void){
  unsigned n = avm_totalactuals(), i;
  char *s;
  double result;

  avm_memcell *cell;
  if(n!=1){
    printf("\nError: one argument (not %d) expected in \"sin\" !\n", n);
	executionFinished =1;
	return ;
  }

  cell=avm_getactual(0);
  if (cell->type != number_m){
	printf("\nError: argument of type number_m expected in \"sin\" !\n");
	executionFinished =1;
	return ;
  }

  s = strdup (avm_tostring(avm_getactual(0)) );
  result = atof(s);

  if (result<0 || result>360){
	retval.type=nil_m;
	retval.data.strVal = "nil_m";
  }
  else{
	retval.type=number_m;
	retval.data.numVal = sin(result);
	//printf("SIN(%lf)= %lf ", result, sin(result) );
  }
  //free(s);
}


void avm_callsaveenviroment (void)
{
  avm_push_envvalue (totalActuals);
  avm_push_envvalue (pc + 1);
  avm_push_envvalue (top + totalActuals + 2);
  avm_push_envvalue (topsp);

}

void execute_funcexit(instruction *unused)
{
  unsigned oldTop = top;
  top 	= avm_get_envvalue ( topsp + AVM_SAVEDTOP_OFFSET);
  /*Kratame to pc twn dialeksewn se periptwsh kapoiou la8ous
	giati sthn fash 4 exoume valei parapanw jumps gia ta call twn funcs
	*/
 // pc  	= avm_get_envvalue ( topsp + AVM_SAVEDPC_OFFSET );
  topsp	= avm_get_envvalue (topsp + AVM_SAVEDTOPSP_OFFSET);

  while(oldTop++ <= top)
    avm_memcellclear(&stack[oldTop]);
}

unsigned avm_get_envvalue(unsigned i)
{

  assert(stack[i].type == number_m);
  unsigned val = (unsigned) stack[i].data.numVal;
  assert(stack[i].data.numVal == ((double) val));
  return val;
}
void avm_dec_top(void)
{
  if(!top)
  {
    avm_error("stack overflow");
    executionFinished = 1;
  }
  else
    --top;
}

void avm_push_envvalue(unsigned val)
{
  printf("val: %d, top: %d\n",val,top);
  stack[top].type 		= number_m;
  stack[top].data.numVal	= val;
  avm_dec_top();
}

void printbuckets(avm_table_bucket*);
void printmemcell(avm_memcell*);

void printbuckets(avm_table_bucket* tmp)
{
  avm_table_bucket* head=tmp;
  printf("\n");
  while(head!=NULL){

    printmemcell(head->key);

    printmemcell(head->value);

    head=head->next;
  }
}



void printmemcell(avm_memcell* tmp){
  int i;
  if(tmp->type==table_m){
    printf("Table:\t");
    //printf("Table RefCounter: %d\t",tmp->data.tableVal->refCounter);
    //printf("Table Total: %d",tmp->data.tableVal->total);
    for(i=0;i<AVM_TABLE_HASHSIZE;i++){
	if(tmp->data.tableVal->strIndexed[i]!=NULL)
	    printbuckets(tmp->data.tableVal->strIndexed[i]);
	if(tmp->data.tableVal->numIndexed[i]!=NULL)
	    printbuckets(tmp->data.tableVal->numIndexed[i]);
    }

    }
  else if(tmp->type==string_m){
    printf("String:%s \n",tmp->data.strVal);
  }
  else if(tmp->type==bool_m)
  {
    printf("Bool:%d \n",tmp->data.boolVal);
  }
  else if(tmp->type==userfunc_m)
  {
    printf("Userfunc%d",tmp->data.funcVal);
  }
  else if(tmp->type==libfunc_m)
  {
    printf("Libfunc %s \n",tmp->data.libfuncVal);
  }
  else if(tmp->type==number_m)
  {
    printf("NumVal %d \n", (int)tmp->data.numVal);
  }
}


void prinstack(void)
{
  unsigned int i;
  printf("\nSTACK\n");
  for(i=0; i<AVM_STACKSIZE;i++)
  {
      if(stack[i].type!=undef_m){
	printf("%d|",i);

	printmemcell(&stack[i]);
	printf("\n");
      }
  }
}
