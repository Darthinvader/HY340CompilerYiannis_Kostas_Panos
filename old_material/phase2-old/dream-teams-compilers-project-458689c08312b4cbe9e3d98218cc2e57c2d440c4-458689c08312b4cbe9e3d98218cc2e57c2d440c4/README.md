CS340: Compilers
Project: A Compiler For The Alpha Programming Language
Spring Semester 2016

Team members:
- Koufopoulos Panos
- Psaradakis Yiannis
- Vardoulakis Michalis

#####################################################################
Build Instructions:
Building a binary requires gcc and GNU make and flex(optionaly, to
make changes to the scanner's definition file<al.lex>)
    $make
        Builds the project
    $make clean 
        Removes all object files
    $make dist-clean
        Executes clean and additionaly removes the generated binary file
	$make src-clean
		Executes dist-clean and additionally removes all source files
		generated from flex and bison
