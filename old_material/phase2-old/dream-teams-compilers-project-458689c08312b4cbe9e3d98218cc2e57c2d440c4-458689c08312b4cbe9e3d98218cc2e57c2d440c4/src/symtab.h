/* filename: symtab.h
 * CS340: Compilers Project
 * Author: Vardoulakis Michalis, 3247
 *
 * Symbol Table API
 */
#ifndef __SYMTAB_H
#define __SYMTAB_H

#define SCOPE_SIZE	30

enum sym_type {
	GLOBAL, LOCAL, FORMAL, USERFUNC, LIBFUNC
};

typedef struct {
	char *id;
	unsigned int scope;
	unsigned int line;
} var_t;

typedef struct {
	char *id;
	//FIXME add arguments list
	unsigned int scope;
	unsigned int line;
} funct_t;

typedef struct symtab_elt {
	int active;
	union {
		var_t v;
		funct_t f;
	} value;
	int type;
	struct symtab_elt *next;
} symtab_elt_t; // Type for the elements of the symbol table

void symtab_init(void);

symtab_elt_t *symtab_insert(char *id, unsigned int scope, unsigned int line, int type);

void symtab_hide(unsigned int scope);

//Lookup in specified scope
symtab_elt_t *symtab_lookup(char *id, unsigned int scope);

//Lookup from specified scope down to 0
symtab_elt_t *symtab_lookup_all(char *id, unsigned int scope);

void symtab_print_all(void);

#endif /*__SYMTAB_H*/
