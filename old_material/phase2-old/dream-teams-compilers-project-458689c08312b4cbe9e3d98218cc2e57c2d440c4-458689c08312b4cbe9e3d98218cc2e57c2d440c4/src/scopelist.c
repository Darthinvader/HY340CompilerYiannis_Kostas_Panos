/* filename: scopelist.c 
 * CS340: Compilers Project
 * Author: Vardoulakis Michalis, 3247
 *
 * Scope List Implementation 
 */
#include "scopelist.h"
#include "symtab.h"
#include <stdlib.h>
#include <assert.h>


slist_node_t *slist_insert(slist_node_t *head, symtab_elt_t *symbol)
{
	assert(symbol);
	slist_node_t *new = malloc(sizeof(slist_node_t));
	new->next = head;
	new->symbol = symbol;

	return new;
}

void slist_destroy(slist_node_t *head)
{
	while(head) {
		slist_node_t *tmp = head;
		head = head->next;
		free(tmp);
	}
}
