/* filename: symtab.c
 * CS340: Compilers Project
 * Author: Vardoulakis Michalis, 3247
 *
 * Symbol Table Implementation
 */

//This define is only used to enable the testing code
//#define TESTING

#include "symtab.h"
#include "scopelist.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifndef TESTING
	#define TABLE_SIZE	7919
#else
	#include <stdio.h>
	#include <time.h>
	#define TABLE_SIZE 17
#endif


struct {
	symtab_elt_t **table;
	unsigned int size;
	unsigned int filled;
} symbol_table;

slist_node_t *scope_lists[SCOPE_SIZE];

static unsigned int symtab_hash(char *key)
{
	assert(key);
	unsigned long hash = 5381;
	int c;
	
	while (c = *key++)
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
	
	return hash;
}

void symtab_init(void)
{
	symbol_table.size = TABLE_SIZE;
	symbol_table.filled = 0;
	symbol_table.table = malloc(symbol_table.size * sizeof(symtab_elt_t *));
	memset(symbol_table.table, 0, symbol_table.size * sizeof(symtab_elt_t *));
	memset(scope_lists, 0, SCOPE_SIZE * sizeof(slist_node_t *));

	//Insert all library functions
	symtab_insert("print", 0, 0, LIBFUNC);
	symtab_insert("input", 0, 0, LIBFUNC);
	symtab_insert("objectmemberkeys", 0, 0, LIBFUNC);
	symtab_insert("objecttotalmembers", 0, 0, LIBFUNC);
	symtab_insert("objectcopy", 0, 0, LIBFUNC);
	symtab_insert("totaalarguments", 0, 0, LIBFUNC);
	symtab_insert("argument", 0, 0, LIBFUNC);
	symtab_insert("typeof", 0, 0, LIBFUNC);
	symtab_insert("strtonum", 0, 0, LIBFUNC);
	symtab_insert("sqrt", 0, 0, LIBFUNC);
	symtab_insert("cos", 0, 0, LIBFUNC);
	symtab_insert("sin", 0, 0, LIBFUNC);
}

symtab_elt_t *symtab_insert(char *id, unsigned int scope, unsigned int line, int type)
{
	assert(id);
	symtab_elt_t *new = malloc(sizeof(symtab_elt_t));
	unsigned long h = symtab_hash(id)%symbol_table.size;
	new->active = 1;
	new->type = type;
	
	if(type == USERFUNC || type == LIBFUNC) {
		new->value.f.id = strdup(id);
		new->value.f.scope = scope;
		new->value.f.line = line;
	} else {
		new->value.v.id = strdup(id);
		new->value.v.scope = scope;
		new->value.v.line = line;
	}
	new->next = symbol_table.table[h];
	symbol_table.table[h] = new;
	scope_lists[scope] = slist_insert(scope_lists[scope], new);

	return new;
}

void symtab_hide(unsigned int scope)
{
	slist_node_t *list = scope_lists[scope];
	while(list) {
		list->symbol->active = 0;
		list = list->next;
	}
}

symtab_elt_t *symtab_lookup(char *id, unsigned int scope)
{
	assert(id);
	symtab_elt_t *list = symbol_table.table[symtab_hash(id)%symbol_table.size];
	while(list) {
		if(list->type == LIBFUNC || list->type == USERFUNC) {
			if(list->active && list->value.f.scope == scope  && strcmp(list->value.f.id, id) == 0)
				return list;
		} else {
			if(list->active && list->value.v.scope == scope  && strcmp(list->value.v.id, id) == 0)
				return list;
		}
		list = list->next;
	}

	return NULL;
}

symtab_elt_t *symtab_lookup_all(char *id, unsigned int scope)
{
	symtab_elt_t *res;
	for(int i = scope; i >= 0; --i) {
		res = symtab_lookup(id, i);
		if(res) 
			break;
	}

	return res;
}

void symtab_print_all(void)
{
	symtab_elt_t **symtab = symbol_table.table;
	symtab_elt_t *list;
	for(int i = 0; i < symbol_table.size; ++i) {
		list = symtab[i];
		if(list) {
			printf("%d: ", i);
			while(list) {
				if(list->type == LIBFUNC || list->type == USERFUNC)
					printf("[%d %s %d %d ",list->active, list->value.f.id, list->value.f.scope, list->value.f.line);
				else
					printf("[%d %s %d %d ", list->active, list->value.v.id, list->value.v.scope, list->value.v.line);
				switch(list->type) {
					case GLOBAL:
						printf("GLOBAL");
						break;
					case LOCAL:
						printf("LOCAL");
						break;
					case FORMAL:
						printf("FORMAL");
						break;
					case USERFUNC:
						printf("USERFUNC");
						break;
					case LIBFUNC:
						printf("LIBFUNC");
						break;
					default:
						assert(0);
				}
				printf(" ] ");
				list = list->next;
			}
			printf("\n");
		}
	}
}

/* The code below was written and used to verify the above implementation
 * and is not part of it. To use it uncomment the line '#define TESTING' at
 * the top and recompile.
 */
#ifdef TESTING

static void randstr(char *buf, unsigned long len)
{
	assert(buf);

	char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	while(len--) {
		*buf = charset[rand()%(sizeof charset - 1)];
		buf++;
	}
	*buf = '\0';
}

int main(void)
{
	char id[9];
	symtab_elt_t *tmp;
	srand(time(NULL));
	symtab_init();
	for(int i = 0; i < 10; ++i) {
		randstr((char *)&id, 8);
		symtab_insert(id, 0, 0, LIBFUNC);
	}
	for(int i = 0; i < 10; ++i) {
		randstr((char *)&id, 8);
		symtab_insert(id, 3, 0, LIBFUNC);
	}
	tmp = symtab_lookup("print", 0);
	printf("tmp: %s\n", tmp->value.f.id);
	symtab_hide(3);

	symtab_print_all();

	return 0;
}

#endif /*TESTING*/
