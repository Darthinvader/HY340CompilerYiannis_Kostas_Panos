%top{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "symtab.h"
#include "parser.h"

void multi_line_comment(void);

}

integer	[0-9]+
real	{integer}\.{integer}
id	[a-zA-Z][a-zA-Z_0-9]*
space	[\r \t\v\n]

%option prefix="alpha_yy"
%option yylineno
%option noyywrap

%%

if		{ return IF; }
else		{ return ELSE; }
while 		{ return WHILE; }
for   		{ return FOR; }
function 	{ return FUNC; }
return 		{ return RT; }
break 		{ return BRK; }
continue 	{ return CNT; }
and  		{ return AND; }
or  		{ return OR; }
not 		{ return NOT; }
local 		{ return LC; }
true  		{ return T; }
false 		{ return F; }
nil  		{ return NIL; }

{integer}	{ alpha_yylval.intVal = atoi(yytext); return INT; }
{real}		{ alpha_yylval.floatVal = atof(yytext); return REAL; }
{id}		{ alpha_yylval.strVal = strdup(yytext); return ID; }
{space}		{ ; }

"\""	{	
		int firstLine = yylineno;
		int c; 
		int i=0;
		char *why;
		why = (char *)malloc(sizeof(char));
		why[i] = '\"'; 
		i++;
		while((c = input())!=EOF && c != '\"'){
			if(c == '\\'){
				c = input();
				if(c != '\"' && c != 'n'&& c != '\\' &&c!='t'){
					//printf("%c",c);fflush(stdout);
					fprintf(stderr, "l%d warning: illegal escape sequence \\%c in string\n", firstLine, c);
					why = (char *)realloc(why,(i+1)*sizeof(char));
					why[i] = '\\';
					i++;
					why = (char *)realloc(why,(i+1)*sizeof(char));
					why[i] = c;
					i++;
				}
				else{	
					if (c == '\"'){ why = (char *)realloc(why,(i+1)*sizeof(char));
						why[i] = '\"';
						i++;
					}
					if(c == '\\'){why = (char *)realloc(why,(i+1)*sizeof(char));
						why[i] = '\\';
						i++;					
					}
					if(c == 'n'){why = (char *)realloc(why,(i+1)*sizeof(char));
						why[i] = '\n';
						i++;
					}
					if(c == 't'){why = (char *)realloc(why,(i+1)*sizeof(char));
						why[i] = '\t';
						i++;
					}
				}
			}
			else{
				why = (char *)realloc(why,(i+1)*sizeof(char));
				why[i] = c;
				i++;
				}
		}
		if(c == EOF){
			fprintf(stderr, "l%d error: encountered EOF while parsing string\n", firstLine);
			exit(-1);
		}
		why = (char *) realloc(why,(i+2)*sizeof(char));
		why[i] = '\"';
		why[i+1] = '\0';
		alpha_yylval.strVal = strdup(yytext);
		return STR;
	}

"{"  { return OPBRACE; }
"}"  { return CLBRACE; }
"["  { return OPBRACKET; }
"]"  { return CLBRACKET; }
"("  { return OPPAR; }
")"  { return CLPAR; }
";"  { return SEMCOL; }
","  { return COMMA; }
"::" { return GLBOP; }
":"  { return COLON; }
".." { return DDOT; }
"."  { return DOT; }

"="  { return ASS; }
"+"  { return PLUS; }
"-"  { return MIN; }
"*"  { return MUL; }
"/"  { return DIV; }
"%"  { return MOD; }
"==" { return EQ; }
">"  { return GT; }
">=" { return GTEQ; }
"<"  { return LT; }
"<=" { return LTEQ; }
"!=" { return NEQ; }
"++" { return INC; }
"--" { return DEC; }

"//"  {
		char c;	
		while((c = input()) != EOF && (c != '\n')) { ; };
	  }

"/*"  { multi_line_comment(); }
.	  {
		fprintf(stderr, "l%d error: unrecognised character %s\n", yylineno, yytext); 
	  }
<<EOF>>	{ return -1;}

%%
void multi_line_comment(void)
{
	int c;
	int c_prev;
	int multCommDepthCount = 1;
	int start_line = yylineno;

	c_prev = 'a';	//init 
	while(((c = input()) != EOF) && multCommDepthCount > 0){
		if(c_prev == '/' && c == '*'){
			multCommDepthCount++;
			c_prev = 'a';	//avoid '/' from being used again
		}else if(c_prev == '*' && c == '/'){
			multCommDepthCount--;		
			c_prev = 'a';	//avoid '*' from being used again
		}else{	
			c_prev = c;
			continue;			
		}		
	}

	if(multCommDepthCount > 0){
		fprintf(stderr, "l%d error: reached EOF without finding equal nummber of closing comment symbol(s)\n", start_line);
		exit(-1);
	}
}
