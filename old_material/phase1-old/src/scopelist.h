/* filename: scopelist.h 
 * CS340: Compilers Project
 * Author: Vardoulakis Michalis, 3247
 *
 * Scope List API
 */
#ifndef __SCOPELIST_H
#define __SCOPELIST_H

#include "symtab.h"

typedef struct sl_node {
	symtab_elt_t *symbol;
	struct sl_node *next;
}slist_node_t;

slist_node_t *slist_insert(slist_node_t *head, symtab_elt_t *symbol);

void slist_destroy(slist_node_t *head);

#endif /*__SCOPELIST_H*/
