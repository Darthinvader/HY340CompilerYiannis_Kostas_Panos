%{
	/* Prologue(user code)*/
	#include <stdio.h>
	#include <stdlib.h>
	#include <assert.h>
	#include <string.h>

	#include "symtab.h"

	extern FILE *alpha_yyin;
	extern int alpha_yylex();
	extern int alpha_yylineno;
	unsigned int scope = 0;
	unsigned int tmp_counter = 0;

	int in_func = 0;
	int func_nesting = 0;
	unsigned int func_scope[SCOPE_SIZE] = {0};
	
	void alpha_yyerror(const char *str);
	int is_libfunc(char *id);

	#define ANONFPREFIX		"_anonf"
%}

/* Bison declarations */

%union {
	int intVal;
	float floatVal;
	char *strVal;
	symtab_elt_t *symbol;
}

/* Keywords, operators & punctuation */
%token IF
%token ELSE
%token WHILE
%token FOR
%token FUNC
%token RT
%token BRK
%token CNT
%token AND
%token OR
%token NOT
%token LC
%token T
%token F
%token NIL
%token ASS
%token PLUS
%token MIN
%token MUL
%token DIV
%token MOD
%token EQ
%token NEQ
%token INC
%token DEC
%token GT
%token LT
%token GTEQ
%token LTEQ
%token OPBRACE
%token CLBRACE
%token OPBRACKET
%token CLBRACKET
%token OPPAR
%token CLPAR
%token SEMCOL
%token COMMA
%token GLBOP
%token COLON
%token DDOT
%token DOT 

%token <intVal> 	INT
%token <floatVal> 	REAL
%token <strVal> 	STR ID

%type <symbol>		lvalue

%start program

%right ASS
%left OR
%left AND
%nonassoc EQ NEQ
%nonassoc GT GTEQ LT LTEQ
%left PLUS MIN
%left MUL DIV MOD
%right NOT INC DEC UMIN
%left DOT DDOT
%left OPBRACKET CLBRACKET
%left OPPAR CLPAR

%%

program:    stmt echo	{ printf("program->stmt echo\n"); 		}
echo:				{ printf("echo->{0}\n");  }
		| stmt echo	{ printf("echo->stmt echo\n");}
			
stmt:	    expr SEMCOL	{ printf("stmt->expr\n");		}
		  | ifstmt 	{ printf("stmt->ifstmt\n"); 	 	}
		  | whilestmt	{ printf("stmt->whilestmt\n"); 	}
		  | forstmt	{ printf("stmt->forstmt\n"); 		}
		  | returnstmt	{ printf("stmt->returnstmt\n");	}
		  | BRK	SEMCOL	{ printf("stmt->break;\n");	}
		  | CNT SEMCOL	{ printf("stmt->continue;\n");	}
		  | block	{ printf("stmt->block\n"); 		}
		  | funcdef	{ printf("stmt->funcdef\n"); 		}
		  | SEMCOL	{ printf("stmt->;\n");		}

expr:		assignexpr 	 { printf("expr->assignxpr\n");	}	
		  | expr op term { printf("expr->expr op term\n"); 	}
		  | term 		 { printf("epxr->term\n"); 		}
			
op:			PLUS { printf("op->+\n"); 	} 
		  | MIN  { printf("op->-\n");	}
		  | MUL  { printf("op->*\n");	}
		  | DIV  { printf("op->/\n");	}
		  | MOD  { printf("op->%\n");	}
		  | GT   { printf("op->>\n");	}
		  | GTEQ { printf("op->>=\n");	}
		  | LT   { printf("op-><\n");	}
		  | LTEQ { printf("op-><=\n");	}
		  | EQ   { printf("op->==\n");	}
		  | NEQ  { printf("op->!=\n");	}
		  | AND  { printf("op->AND\n");	}
		  | OR   { printf("op->OR\n");	}


term: 		OPPAR expr CLPAR 	{ printf("term->(expr)\n");  		 } 
		  | MIN expr %prec UMIN		{ printf("term->- expr\n"); 		 }
		  | NOT expr 		{ printf("term->NOT expr\n"); 	 } 
		  | INC lvalue 		{ 
		  						printf("term->++lvalue\n");
						 		if($2->type == USERFUNC || $2->type == LIBFUNC) {
									char str[32 + strlen($2->value.f.id) + 1];
									sprintf(str, "function id '%s' used in operation", $2->value.f.id);
									alpha_yyerror(str);
								}
							}
		  | lvalue INC	 	{ 
		  						printf("term->lvalue++\n");
								if($1->type == USERFUNC || $1->type == LIBFUNC) {
									char str[32 + strlen($1->value.f.id) + 1];
									sprintf(str, "function id '%s' used in operation", $1->value.f.id);
									alpha_yyerror(str);
								}
							}
		  | DEC	lvalue	 	{
		  						printf("term->--lvalue\n"); 
								if($2->type == USERFUNC || $2->type == LIBFUNC) {
									char str[32 + strlen($2->value.f.id) + 1];
									sprintf(str, "function id '%s' used in operation", $2->value.f.id);
									alpha_yyerror(str);
								}
							}
		  | lvalue DEC	 	{ 
		  						printf("term->lvalue--\n");
								if($1->type == USERFUNC || $1->type == LIBFUNC) {
									char str[32 + strlen($1->value.f.id) + 1];
									sprintf(str, "function id '%s' used in operation", $1->value.f.id);
									alpha_yyerror(str);
								}
							}
		  | primary 		{ printf("term->primary\n");		 } 

assignexpr:	lvalue ASS expr		{ 
									printf("assignexpr->lvalue = expr\n");
									if($1->type == USERFUNC || $1->type == LIBFUNC) {
										char str[32 + strlen($1->value.f.id) + 1];
										sprintf(str, "function id '%s' used in operation", $1->value.f.id);
										alpha_yyerror(str);
									}
								}

primary:	lvalue 			{ printf("primary->lvalue\n");		 }
		  | call 		{ printf("primary->call\n"); 		 }
		  | objectdef 		{ printf("primary->objectdef\n"); 	 }
		  | OPPAR funcdef CLPAR { printf("primary->(funcdef)\n");	 }
		  | const	 	{ printf("primary->const\n"); 		 }

lvalue:		ID 			{ 
							printf("lvalue->ID \n");
							symtab_elt_t *elt = symtab_lookup_all($1, scope);
							$$ = elt;
							if(!elt) {
								$$ = symtab_insert($1, scope, alpha_yylineno, (scope == 0)?GLOBAL:LOCAL);
							} else {
								unsigned int elt_scope;
								if(elt->type == LIBFUNC || elt->type == USERFUNC)
									elt_scope = elt->value.f.scope;
								else
									elt_scope = elt->value.v.scope;
								if(elt_scope != 0 && elt_scope < func_scope[func_nesting]) {
									char err_str[26 + strlen($1) + 1];
									sprintf(err_str, "identifier '%s' out of scope", $1);
									alpha_yyerror(err_str);
								}
							}
						}
		  | LC ID		{ 
		  					printf("lvalue->local ID\n");
							assert($2);
							if(scope != 0 && is_libfunc($2)) {
								char err_str[36 + strlen($2) + 1];
								sprintf(err_str, "redeclaration of library function '%s'", $2);
								alpha_yyerror(err_str);
							} else {
								if(!($$ = symtab_lookup($2, scope)))
									$$ = symtab_insert($2, scope, alpha_yylineno, (scope == 0)?GLOBAL:LOCAL);
							}
						}
		  | GLBOP ID	{ 
		  					printf("lvalue->::ID\n");
							if(!($$ = symtab_lookup($2, 0))) {
								char err_str[29 + strlen($2) + 1];
								sprintf(err_str, "undeclared global variable '%s'", $2);
								alpha_yyerror(err_str);
							}
							
						}
		  | member		{ printf("lvalue->member\n"); 	 }	

member:		lvalue DOT ID					{ printf("member->lvalue.ID\n");    }
		  | lvalue OPBRACKET expr CLBRACKET { printf("member->lvalue[expr]\n"); }
		  | call DOT ID			    		{ printf("member->call.ID\n");      }
		  | call OPBRACKET expr CLBRACKET   { printf("member->call[expr]\n");   }

call:		call OPPAR elist CLPAR					{ printf("call->call(elist)\n"); 	    }
		  | lvalue callsuffix						{ printf("call->lvalue callsuffix\n"); } 
		  | OPPAR funcdef CLPAR OPPAR elist CLPAR	{ printf("call->(funcdef) (elist)\n"); }

callsuffix:	normcall	{ printf("callsufix->normcall\n");   	}
		  | methodcall  { printf("callsufix->methodcall\n");	}

normcall:	OPPAR elist CLPAR	    	{ printf("normcall->(elist)\n");	}
methodcall:	DDOT ID OPPAR elist CLPAR   { printf("methodcall->..ID(elist)\n");	}

elist:								{ printf("elist->{0}\n"); 		  }
		| expr elistoption 			{ printf("elist->expr elistoption *\n"); }
elistoption:				 		{ printf("elistoption->{0}\n"); 			}
		| COMMA expr elistoption	{ printf("elistoption->,expr elistoption\n"); 	}	

objectdef:	OPBRACKET objectdefoption CLBRACKET	{ printf("objectdef->[objectfefoption]\n"); }
objectdefoption:  elist							{ printf("objectdefoption->elist | {0}\n"); }
		| indexed								{ printf("objectdefoption->indexed\n"); }

indexed:	 indexedelem indexedoption 		{ printf("indexed->indexelem indexedoption \n"); }
indexedoption:								{ printf("indexedoption->{0}\n"); 		           }
		| COMMA indexedelem indexedoption	{ printf("indexedoption->, indexedelem indexedoption\n"); }
	
indexedelem:	OPBRACE expr COLON expr CLBRACE	  { printf("indexedelem->{expr:expr}\n"); }

block:		OPBRACE					{ 
										++scope; 
									}
			blockoption CLBRACE		{ 
								  		printf("block->[blockoption]\n"); 
										symtab_hide(scope--);
										tmp_counter = 0;
									}
blockoption:				{ printf("blockoption->{0}\n"); 		}
		| stmt blockoption	{ printf("blockoption->stmt blockoption\n"); }

funcdef:	FUNC funcdefoption OPPAR 	{ in_func = 1; func_nesting++; func_scope[func_nesting] = scope + 1;}
			idlist CLPAR block		 	{ in_func = 0; func_nesting--; printf("funcdef->FUNCTION funcdefoption (idlist) block\n"); 	}
funcdefoption:		{ 
						//Anonymous function
						printf("funcdefoption->{0}\n");
						char suffix[sizeof(int)];
						sprintf(suffix, "%d", tmp_counter);
						char id[strlen(ANONFPREFIX) + strlen(suffix) + 1];
						sprintf(id, "%s%d", ANONFPREFIX, tmp_counter);
						++tmp_counter;
						symtab_insert(id, scope, alpha_yylineno, USERFUNC);
					}
		| ID		{
						printf("funcdefoption->ID\n");
						if(!symtab_lookup($1, scope) && !is_libfunc($1)) {
							symtab_insert($1, scope, alpha_yylineno, USERFUNC);
						} else {
							char err_str[30 + strlen($1) + 1];
							sprintf(err_str, "redeclaration of identifier '%s'", $1);
							alpha_yyerror(err_str);
						}
					} 

const:		INT 	{ printf("const->INT\n"); }
		  | REAL 	{ printf("const->REAL\n"); }
		  | STR	 	{ printf("const->STRING\n"); }
		  | NIL 	{ printf("const->NIL\n"); }
		  | T 		{ printf("const->TRUE\n"); }
		  | F	 	{ printf("const->FALSE\n"); }

idlist:		
		| ID idlistoption		{
									printf("idlist->ID idlistoption \n");
									if(!symtab_lookup($1, scope + 1)) {
										symtab_insert($1, scope + 1, alpha_yylineno, FORMAL);
									} else {
										char err_str[30 + strlen($1) + 1];
										sprintf(err_str, "redeclaration of identifier '%s'", $1);
										alpha_yyerror(err_str);
									}
								}
idlistoption:					{ printf("idlistoption->{0}\n"); 		 }
		| COMMA ID idlistoption	{ 
									printf("idlistoption->, ID idlistoption\n"); 
									if(!symtab_lookup($2, scope + 1)) {
										symtab_insert($2, scope + 1, alpha_yylineno, FORMAL);
									} else {
										char err_str[30 + strlen($2) + 1];
										sprintf(err_str, "redeclaration of identifier '%s'", $2);
										alpha_yyerror(err_str);
									}
								}

ifstmt:		IF OPPAR expr CLPAR stmt ifstmtoption	{ printf("ifstmt->IF(expr)stmt ifstmtoption\n"); }
ifstmtoption:		{ printf("ifstmtoption->{0}\n"); 	 }
	  	| ELSE stmt	{ printf("ifstmtoption->ELSE stmt\n"); }

whilestmt:	WHILE OPPAR expr CLPAR stmt				{ printf("whilestmt->WHILE(expr)stmt\n");		 	}

forstmt:	FOR OPPAR elist SEMCOL expr SEMCOL elist CLPAR stmt	{ printf("forstmt->FOR(elist;expr;elist;)\n");	}

returnstmt:	RT returnoption SEMCOL { printf("returnstmt->RETURN returnoption ;\n"); }
returnoption:		{ printf("returnoption->{0}\n"); }
		| expr	{ printf("returnoption->expr\n"); }
	


%%
int is_libfunc(char *id)
{
	symtab_elt_t *elt = symtab_lookup(id, 0);
	if(elt && (elt->type == LIBFUNC))
		return 1;
	else
		return 0;
}

void alpha_yyerror(const char *str)
{
	fprintf(stderr, "l%d: %s\n", alpha_yylineno, str);
}

int main(int argc, char *argv[])
{
	FILE *out;

	if(argc == 1 || argc > 3) {
		printf("usage: ./al input_file [output_file]\n");
		exit(-1);
	}

	alpha_yyin = fopen(argv[1], "r");
	if(!alpha_yyin) {
		fprintf(stderr, "Cannot open file %s for reading\n", argv[1]);
		exit(-1);
	}

	if(argc == 3) {
		out = fopen(argv[2], "w");
		if(!out) {
			fprintf(stderr, "Cannot open file %s for writing\n", argv[2]);
			exit(-1);
		}
	} else {
		out = stdout;
	}
	
	symtab_init();

	while(!feof(alpha_yyin)) {
		alpha_yyparse();
	}
		
	symtab_print_all();

	return 0;
}
