#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "symtable.h"
#include "quadgen.h"
#include "emitters.h"
#include "log.h"


typedef enum finalopcode {
	assignn,addd,subb,mull,divdd,modd,
	jeq,jne,jle,jge,jlt,jgt,ju,
	calll,pusharg,funcenter,funcexit,
	newtable,tablegetelemm,tablesetelemm,
	nop
} fopcode_t;

static const char *finalcodes[]={
	"assign","add","sub","mul","div","mod","jeq","jne","jle","jge","jlt","jgt","ju",
	"call","pusharg","funcenter","funcexit",
	"newtable","tablegetelem","tablesetelem",
	"nop"
};
typedef enum arraycode{
	globals,stringss,integers,realnums,booleans,functions,locals,formals,returnvalue,nill,noc
}acode_t;

static const char *arraycodes_str[]={
	"globals","stringss","integers","realnums","booleans","functions","locals","formals","returnvalue","nill","label"
};

typedef struct GlobalVars {
	int index;
	char *name;
}glob_t;

typedef struct formalocal{
	int function_index;
	char *name;
}formalocal_t;

typedef struct function{
	formalocal_t *formals;
	int formal_index;
	formalocal_t *locals;
	int local_index;
	char *fname;
}function_t;

typedef struct finale{
	fopcode_t opcode;
	acode_t aarg1;
	int indexarg1;
	acode_t aarg2;
	int indexarg2;
	acode_t aarg3;
	int indexarg3;
	int jump_patch_func;
}final_t;


typedef struct analyzerresults{
	acode_t array;
	int index;
}anres_t;

function_t *funpin;
int fun_index;
glob_t *globalpin;
int global_index;
char **strings;
int string_index;
int *integer;
int int_index;
double *reals;
int real_index;

final_t *finalcoder;
int fcoder_index;
int *nest_funcs;
int nf_index;
int nf_alloc;

int finalcode(quadtbl_t *qt,symtable_t *st, FILE *fp);

void quadto_final(iquad_t *q,symtable_t *st,int index);
void addnocs(int i);

void init_arrays(quadtbl_t *qt,symtable_t *st);

anres_t expr_analyzer(expr_t *expr,symtable_t *st);



void finalmalloc();


int add_libfunc(char *funcname,int index);

int add_function(char *funcname,symtable_t *st);

int add_local(char *name,int index);

int add_formal(char *name,int index);
int add_global(char *name);
int add_string(char *string);
int add_int(int ints);
int add_real(double real);


int allformals(symtable_t *st,int index);

int findglobal(expr_t *expr);
int find_function(expr_t *expr);

int findlocal(expr_t *expr);

int findformal(expr_t *expr);

int findstring(char *stringtofind);

int findint(int inttofind);

int findreal(double realtofind);
int findaddglobal(expr_t *expr);
int findaddlocal(expr_t *expr);

int findaddfunction(expr_t *expr,symtable_t *st);

int findaddstring(char *stringtoadd);

int findaddreal(double realtoadd);

int findaddint(int inttoadd);

void add_nest(int index);
void remove_nest();

void addjumps();
void print_final_code(FILE *fp);

void print_final_code_with_help();
void indexprinter(acode_t acode,int index);
