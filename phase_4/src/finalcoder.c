#include "finalcoder.h"

int finalcode(quadtbl_t *qt,symtable_t *st, FILE *fp){
	init_arrays(qt,st);
	for(int i=0;i<qt->qt_index;i++){
		quadto_final(qt->qtbl[i],st,i);
	}
	print_final_code(fp);
#ifdef LOGLV_DEBUG
	print_final_code_with_help();
#endif
}

void quadto_final(iquad_t *q,symtable_t *st,int index){
	//fprintf(fp, "%s ",iopcode_str[q->op]);
	//what opcode is it;
	if(!q) return;
	anres_t returner;
	if(q->op == ret){
		finalmalloc();
		finalcoder[fcoder_index-1].opcode = assignn;
		finalcoder[fcoder_index-1].aarg1 = returnvalue;
		finalcoder[fcoder_index-1].indexarg1 = 0;
		if(q->res == NULL){
			finalcoder[fcoder_index-1].aarg2 = nill;
			finalcoder[fcoder_index-1].indexarg2 = 0;
		}
		else{
			returner = expr_analyzer(q->res,st);
			finalcoder[fcoder_index-1].aarg2 = returner.array;
			finalcoder[fcoder_index-1].indexarg2 = returner.index;
		}
		finalcoder[fcoder_index-1].aarg3 = noc;
		finalcoder[fcoder_index-1].indexarg3 = 0;
		finalcoder[fcoder_index-1].jump_patch_func = -1;
		return;
	}
	if(q->op == funcstart){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = funcenter;
		returner = expr_analyzer(q->res,st);
		finalcoder[fcoder_index-1].aarg1 = returner.array;
		finalcoder[fcoder_index-1].indexarg1 = returner.index;
		add_nest(fun_index-1);
		addnocs(2);
		finalcoder[fcoder_index-1].jump_patch_func = -1;
		return;
	}
	if(q->op == funcend){
		finalmalloc();
		remove_nest();
		finalcoder[fcoder_index-1].opcode = funcexit;
	}
	if(q->op <=mod ){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = q->op;
	}
	if(q->op == if_eq){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = jeq;
	}
	if(q->op == if_neq){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = jne;
	}
	if(q->op == if_lesseq){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = jle;
	}
	if(q->op == if_greatereq){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = jge;
	}
	if(q->op == if_less){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = jlt;
	}
	if(q->op == if_greater){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = jgt;
	}
	if(q->op == call){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = calll;
	}
	if(q->op == param){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = pusharg;
	}
	if(q->op == getretval){
		finalmalloc();
		finalcoder[fcoder_index-1].opcode = assignn;
		returner = expr_analyzer(q->res,st);
		finalcoder[fcoder_index-1].aarg1 = returner.array;
		finalcoder[fcoder_index-1].indexarg1 = returner.index;
		finalcoder[fcoder_index-1].aarg2 = returnvalue;
		finalcoder[fcoder_index-1].indexarg2 = 0;
		addnocs(3);
		finalcoder[fcoder_index-1].jump_patch_func = -1;
		return;
	}
	if(q->op == tablecreate){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = newtable;
	}
	if(q->op == tablegetelem){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = tablegetelemm;
	}
	if(q->op == tablesetelem){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = tablesetelemm;
	}
	if(q->op == jump){
		finalmalloc();
		finalcoder[fcoder_index -1].opcode = ju;
	}
	returner = expr_analyzer(q->res,st);
	finalcoder[fcoder_index-1].aarg1 = returner.array;
	finalcoder[fcoder_index-1].indexarg1 = returner.index;

	returner = expr_analyzer(q->arg1,st);
	finalcoder[fcoder_index-1].aarg2 = returner.array;
	finalcoder[fcoder_index-1].indexarg2 = returner.index;

	returner = expr_analyzer(q->arg2,st);
	finalcoder[fcoder_index-1].aarg3 = returner.array;
	finalcoder[fcoder_index-1].indexarg3 = returner.index;

	if(q->label>=0){
		finalcoder[fcoder_index-1].jump_patch_func = q->label;
	}
	else{
		finalcoder[fcoder_index-1].jump_patch_func =-1;
	}
}
void addnocs(int i){
	if(i<=3){
		finalcoder[fcoder_index-1].aarg3 = noc;
		finalcoder[fcoder_index-1].indexarg3 = 0;
	}
	if(i<=2){
		finalcoder[fcoder_index-1].aarg2 = noc;
		finalcoder[fcoder_index-1].indexarg2 = 0;
	}
	if(i<=1){
		finalcoder[fcoder_index-1].aarg1 = noc;
		finalcoder[fcoder_index-1].indexarg1 = 0;
	}
	if(i<=0){
		finalcoder[fcoder_index-1].jump_patch_func = -1;
	}
}

void init_arrays(quadtbl_t *qt,symtable_t *st){
	funpin = malloc(12*sizeof(function_t));
	fun_index = 12;
	add_libfunc("print",0);
	add_libfunc("input",1);
	add_libfunc("objectmemberkeys",2);
	add_libfunc("objecttotalmembers",3);
	add_libfunc("objectcopy",4);
	add_libfunc("totalarguments",5);
	add_libfunc("argument",6);
	add_libfunc("typeof",7);
	add_libfunc("strtonum",8);
	add_libfunc("sqrt",9);
	add_libfunc("cos",10);
	add_libfunc("sin",11);
	global_index = 0;
	string_index = 0;
	int_index = 0;
	real_index = 0;
	fcoder_index = 0;
	nf_index =0;
	nf_alloc =0;
}

anres_t expr_analyzer(expr_t *expr,symtable_t *st){
	anres_t returner;
	if(expr ==NULL){
		returner.array = noc;
		returner.index = 0;
	}
	else if(expr->type == constnum_e){
		returner.array = realnums;
		returner.index = findaddreal(expr->cval.num);
	}
	else if(expr->type == conststr_e){
		returner.array = stringss;
		returner.index = findaddstring(expr->cval.str);
	}
	else if(expr->type == constbool_e){
		returner.array = booleans;
		if(expr->cval.bl == true){
			returner.index = 1;
		}
		else{
			returner.index =0;
		}
	}
	else if(expr->type == nil_e){
		returner.array = nill;
		returner.index =0;
	}
	else if(expr->sym->id_type == func_local){
		returner.array = locals;
		returner.index = findaddlocal(expr);
	}
	else if(expr->sym->id_type == prg_var){
		returner.array = globals;
		returner.index = findaddglobal(expr);
	}
	else if(expr->sym->id_type == formal_arg){
		returner.array = formals;
		returner.index = findformal(expr);
	}
	else if(expr->sym->id_type == lib_func || expr->sym->id_type == user_func){
		returner.array = functions;
		returner.index = findaddfunction(expr,st);
	}
	return returner;
}



void finalmalloc(){
	if(fcoder_index == 0){
		finalcoder = malloc(sizeof(final_t));
	}
	else{
		finalcoder = realloc(finalcoder,sizeof(final_t)*(fcoder_index+1));
	}
	fcoder_index++;
}


int add_libfunc(char *funcname,int index){
	funpin[index].fname = strdup(funcname);
	funpin[index].formals = NULL;
	funpin[index].locals = NULL;
	funpin[index].formal_index =0;
	funpin[index].local_index = 0;
	return index;
}

int add_function(char *funcname,symtable_t *st){
	if(fun_index == 0){
		funpin = malloc(sizeof(function_t));
	}
	else{
		funpin = realloc(funpin,sizeof(function_t)*(fun_index+1));
	}
	funpin[fun_index].fname = strdup(funcname);
	funpin[fun_index].local_index =0;
	funpin[fun_index].formal_index = 0;
	//add formals to function
	allformals(st,fun_index -12);
	fun_index++;
	return fun_index -1;
}

int add_local(char *name,int index){
	int myindex = funpin[index].local_index;
	if(myindex ==0){
		funpin[index].locals = malloc(sizeof(formalocal_t));
	}
	else{
		funpin[index].locals = realloc(funpin[index].locals,sizeof(formalocal_t)*(myindex+1));
	}
	funpin[index].locals[myindex].function_index = index;
	funpin[index].locals[myindex].name = strdup(name);
	funpin[index].local_index++;
	return myindex;
}

int add_formal(char *name,int index){
	int myindex = funpin[index].formal_index;
	if(myindex ==0){
		funpin[index].formals = malloc(sizeof(formalocal_t));
	}
	else{
		funpin[index].formals = realloc(funpin[index].formals,sizeof(formalocal_t)*(myindex+1));
	}
	funpin[index].formals[myindex].function_index = index;
	funpin[index].formals[myindex].name = strdup(name);
	funpin[index].formal_index++;
	printf("formal index:%i",index);
	return myindex;
}

int add_global(char *name){
	if(global_index ==0){
		globalpin = malloc(sizeof(glob_t));
	}
	else{
		globalpin = realloc(globalpin,sizeof(glob_t)*(global_index+1));
	}
	globalpin[global_index].name = strdup(name);
	globalpin[global_index].index = global_index;
	global_index++;
	return global_index-1;
}
int add_string(char *string){
	if(string_index == 0){
		strings = malloc(sizeof(char*));
	}
	else{
		strings = realloc(strings,sizeof(char*)*(string_index+1));
	}
	strings[string_index] = strdup(string);
	string_index++;
	return string_index-1;
}
int add_int(int ints){
	if(int_index == 0){
		integer = malloc(sizeof(int));
	}
	else{
		integer = realloc(integer,sizeof(int)*(int_index+1));
	}
	integer[int_index] = ints;
	int_index++;
	return int_index-1;
}
int add_real(double real){
	if(real_index == 0){
		reals = malloc(sizeof(int));
	}
	else{
		reals = realloc(reals,sizeof(double)*(real_index+1));
	}
	reals[real_index] = real;
	real_index++;
	return real_index-1;
}


int allformals(symtable_t *st,int index){
	node_t *tmp;
	for(int i=0;i<=st->max_scope;i++){
		tmp = st->scope_lst[i];
		while(tmp){
			//printf("scanned");
			if(tmp->id_type == formal_arg && tmp->scope_id == index){
				add_formal(tmp->name,index+12);
			}
			tmp = tmp->scopeNext;
		}
	}
}



int findglobal(expr_t *expr){
	for(int i=0;i<global_index;i++){
		if(strcmp(globalpin[i].name,expr->sym->name) == 0){
			return i;
		}
	}
	return -1;
}

int find_function(expr_t *expr){
	for(int i=fun_index-1;i>=0;i--){
		if(strcmp(funpin[i].fname,expr->sym->name) == 0){
			return i;
		}
	}
	return -1;
}

int findlocal(expr_t *expr){
	int index = funpin[nest_funcs[nf_index-1]].local_index;
	for(int i=0;i<index;i++){
		if(strcmp(expr->sym->name,funpin[nest_funcs[nf_index-1]].locals[i].name)==0){
			return i;
		}
	}
	return -1;
}

int findformal(expr_t *expr){
	int index = funpin[nest_funcs[nf_index-1]].formal_index;
	for(int i=0;i<index;i++){
		if(strcmp(expr->sym->name,funpin[nest_funcs[nf_index-1]].formals[i].name)==0){
			return i;
		}
	}
	return -1;
}

int findstring(char *stringtofind){
	for(int i=0;i<string_index;i++){
		if(strcmp(stringtofind,strings[i])==0){
			return i;
		}
	}
	return -1;
}

int findint(int inttofind){
	for(int i=0;i<int_index;i++){
		if(inttofind == integer[i]){
			return i;
		}
	}
	return -1;
}

int findreal(double realtofind){
	for(int i=0;i<real_index;i++){
		if(realtofind == reals[i]){
			return i;
		}
	}
	return -1;
}

int findaddglobal(expr_t *expr){
	int i = findglobal(expr);
	if(i==-1){
		i =add_global(expr->sym->name);
	}
	return i;
}

int findaddlocal(expr_t *expr){
	int i = findlocal(expr);
	if(i==-1){
		i =add_local(expr->sym->name,nest_funcs[nf_index-1]);
	}
	return i;
}

int findaddfunction(expr_t *expr,symtable_t *st){
	int i = find_function(expr);
	if(i==-1){
		i = add_function(expr->sym->name,st);
	}
	return i;
}

int findaddstring(char *stringtoadd){
	int i = findstring(stringtoadd);
	if(i==-1){
		i = add_string(stringtoadd);
	}
	return i;
}

int findaddreal(double realtoadd){
	int i = findreal(realtoadd);
	if(i==-1){
		i = add_real(realtoadd);
	}
	return i;
}

int findaddint(int inttoadd){
	int i = findint(inttoadd);
	if(i == -1){
		i = add_int(inttoadd);
	}
	return i;
}

void add_nest(int index){
	if(nf_index == nf_alloc ){
		if(nf_alloc == 0){
			nest_funcs = malloc(sizeof(int));
		}
		else{
			nest_funcs = realloc(nest_funcs,sizeof(int)*(nf_alloc+1));
		}
		nf_alloc++;
	}
	nest_funcs[nf_index] = index;
	nf_index++;
}

void remove_nest(){
	if(nf_index>0){
		nf_index--;
	}
}

void print_escaped_string(FILE *fp, size_t i) {
	char *it;
	it = strings[i];
	while(*it != '\0') {
		switch(*it){
		case '\n':
			fprintf(fp, "%s", "\\n");
			break;
		case '\t':
			fprintf(fp, "%s", "\\t");
			break;
		case ' ':
			fprintf(fp, "%s", "\\s");
			break;
		default:
			fprintf(fp, "%c", *it);
			break;
		}
		it++;
	}
}

void print_final_code(FILE *fp){
	final_t temp;
	fprintf(fp,"%d\n",global_index);
	for(size_t i=0;i<string_index;i++){
		fprintf(fp,"\"");
		print_escaped_string(fp, i);
		fprintf(fp,"\" ");
	}
	fprintf(fp,"\n");
	for(int i=0;i<int_index;i++){
		fprintf(fp,"%d" ,integer[i]);
	}
	fprintf(fp,"\n");
	for(int i=0;i<real_index;i++){
		fprintf(fp,"%f ",reals[i]);
	}
	fprintf(fp,"\n");
	fprintf(fp,"%d",fun_index);
	fprintf(fp,"\n");
	for (int i=0;i<fun_index;i++){
		fprintf(fp,"%d ",funpin[i].local_index);
	}
	fprintf(fp,"\n");
	for (int i=0;i<fun_index;i++){
		fprintf(fp,"%d ",funpin[i].formal_index);
	}
	fprintf(fp,"\n");
	for(int i=0;i<fcoder_index;i++){
		fprintf(fp,"%i:",i);
		temp = finalcoder[i];
		fprintf(fp,"%d",temp.opcode);
		if(temp.aarg1!=noc){
			fprintf(fp," %d;%d",temp.aarg1,temp.indexarg1);
		}
		if(temp.aarg2!=noc){
			fprintf(fp," %d;%d",temp.aarg2,temp.indexarg2);
		}
		if(temp.aarg3!=noc){
			fprintf(fp," %d;%d",temp.aarg3,temp.indexarg3);
		}
		if(temp.jump_patch_func !=-1){
			fprintf(fp," 10;%d",temp.jump_patch_func);
		}
		fprintf(fp,"\n");
	}
}


void print_final_code_with_help(){
	final_t temp;
	printf("%d\n",global_index);
	for(int i=0;i<string_index;i++){
		printf("%i:\"%s\" ",i,strings[i]);
	}
	printf("\n");
	for(int i=0;i<int_index;i++){
		printf("%i:%d" ,i,integer[i]);
	}
	printf("\n");
	for(int i=0;i<real_index;i++){
		printf("%i:%f ",i,reals[i]);
	}
	printf("\n");
	printf("%d",fun_index);
	printf("\n");
	for (int i=0;i<fun_index;i++){
		printf("%i:%d ",i,funpin[i].local_index);
	}
	printf("\n");
	for (int i=0;i<fun_index;i++){
		printf("%i:%d ",i,funpin[i].formal_index);
	}
	printf("\n");
	for(int i=0;i<fcoder_index;i++){
		printf("%i:",i);
		temp = finalcoder[i];
		printf("%d(%s)",temp.opcode,finalcodes[temp.opcode]);
		if(temp.opcode ==funcenter){
			add_nest(temp.indexarg1);
		}
		if(temp.opcode ==funcexit){
			remove_nest();
		}
		if(temp.aarg1!=noc){
			printf(" %d;%d(%s;",temp.aarg1,temp.indexarg1,arraycodes_str[temp.aarg1]);
			indexprinter(temp.aarg1,temp.indexarg1);
		}
		if(temp.aarg2!=noc){
			printf(" %d;%d(%s;",temp.aarg2,temp.indexarg2,arraycodes_str[temp.aarg2]);
			indexprinter(temp.aarg2,temp.indexarg2);
		}
		if(temp.aarg3!=noc){
			printf(" %d;%d(%s;",temp.aarg3,temp.indexarg3,arraycodes_str[temp.aarg3]);
			indexprinter(temp.aarg3,temp.indexarg3);
		}
		if(temp.jump_patch_func !=-1){
			printf(" 10;%d(jumpto)",temp.jump_patch_func);
		}
		printf("\n");
	}
}

void indexprinter(acode_t acode,int index){
	if(acode == globals){
		printf("%s)",globalpin[index].name);
	}
	if(acode == stringss){
		printf("%s)",strings[index]);
	}
	if(acode == integers){
		printf("%i)",integer[index]);
	}
	if(acode == realnums){
		printf("%f)",reals[index]);
	}
	if(acode == booleans){
		if(index == 0){
			printf("FALSE)");
		}
		else{
			printf("TRUE");
		}
	}
	if(acode == functions){
		printf("%s)",funpin[index].fname);
	}
	if(acode == locals){
		printf("%s)",funpin[nest_funcs[nf_index-1]].locals[index].name);
	}
	if(acode == formals){
		printf("%s)",funpin[nest_funcs[nf_index-1]].formals[index].name);
	}
	if(acode == returnvalue){
		printf("0)");
	}
	if(acode == noc){
		printf("ERROR)");
	}
}
