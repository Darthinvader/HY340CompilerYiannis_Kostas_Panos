#include <stdio.h>
#include <stdarg.h>
#include "log.h"

void alpha_log(log_lv level, const char * restrict fmt, ... ) {
	va_list argl;
	va_start(argl, fmt);

	switch(level) {
		case LOG_ERROR:
#ifdef LOGLV_INFO
		case LOG_INFO:
#endif
#ifdef LOGLV_DEBUG
		case LOG_DEBUG:
#endif
			fprintf(stderr, "%s: ", ll_str[level]);
			vfprintf(stderr, fmt, argl);
			break;
		default: break;
	}
	va_end(argl);
}
