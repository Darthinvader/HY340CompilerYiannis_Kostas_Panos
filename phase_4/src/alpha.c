#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "symtable.h"
#include "quadgen.h"
#include "lex.alpha_yy.h"
#include "alpha_yy.tab.h"
#include "finalcoder.h"

typedef struct alpha_args {
	bool inputf_set, outputf_set, ph3_print;
	FILE *input, *output;
} al_args_t;

void cleanup(al_args_t *al_args, symtable_t *st, quadtbl_t *qt) {
	if(al_args->inputf_set == true) fclose(al_args->input);
	if(al_args->outputf_set == true) fclose(al_args->output);
	if(st != NULL) free_symtable(st);
	if(qt != NULL) free_quadtbl(qt);
}

void print_usage(const char *exec_name) {
	fprintf(stderr, "\nusage: %s [option] input-file\n", exec_name);
	fprintf(stderr, "  -o output-file\tWrite parser output to file\n");
	fprintf(stderr, "  -3 \tProduce only Phase 3 output\n");
}

void parse_args(al_args_t *al_args, int argc, char *argv[]) {
	char c;
	while(optind < argc) {
		if((c = getopt(argc, argv, ":o:3")) != -1) {
			switch(c) {
				case 'o':
					if(al_args->outputf_set != true) {
						if((al_args->output = fopen(optarg, "w")) == NULL) {
							perror(argv[0]);
							print_usage(argv[0]);
							cleanup(al_args, NULL, NULL);
							exit(EXIT_FAILURE);
						}
						al_args->outputf_set = true;
					} else {
						fprintf(stderr, "%s: duplicate output file\n", argv[0]);
						print_usage(argv[0]);
						cleanup(al_args, NULL, NULL);
						exit(EXIT_FAILURE);
					}
					break;
				case '3':
					al_args->ph3_print = true;
					break;
				case ':':
					fprintf (stderr, "%s: option -%c requires an argument.\n", argv[0], optopt);
					print_usage(argv[0]);
					cleanup(al_args, NULL, NULL);
					exit(EXIT_FAILURE);
					break;
				case '?':
					fprintf(stderr, "%s: illegal option -- %c\n", argv[0], optopt);
					print_usage(argv[0]);
					cleanup(al_args, NULL, NULL);
					exit(EXIT_FAILURE);
					break;
			}
		} else {
			if(al_args->inputf_set == false) {
				if((al_args->input = fopen(argv[optind], "r")) == NULL) {
					perror(argv[0]);
					print_usage(argv[0]);
					cleanup(al_args, NULL, NULL);
					exit(EXIT_FAILURE);
				}
				al_args->inputf_set = true;
			} else {
				fprintf(stderr, "%s: duplicate input file\n", argv[0]);
				print_usage(argv[0]);
				cleanup(al_args, NULL, NULL);
				exit(EXIT_FAILURE);
			}
			optind++;
		}
	}
}

int main(int argc, char *argv[]) {
	int retval;

	al_args_t al_args = {
		.inputf_set = false,
		.outputf_set = false,
		.ph3_print = false,
		.input = NULL,
		.output = NULL
	};
	parse_args(&al_args, argc, argv);

	symtable_t *st = init_symtable();
	quadtbl_t *qt = init_quadtbl();

	if(al_args.inputf_set == true) alpha_yyin = al_args.input;
	else alpha_yyin = stdin;
	if(al_args.outputf_set == false) al_args.output = stdout;
	retval = alpha_yyparse(st, qt);
	if(!retval) {
		print_all_nodes(st);
		if(al_args.ph3_print == true) print_quads(qt, al_args.output);
		else finalcode(qt,st,al_args.output);
	}
	cleanup(&al_args, st, qt);
	return retval;
}
