typedef enum alpha_log_level {
	LOG_INFO,
	LOG_ERROR,
	LOG_DEBUG
} log_lv;

static const char *ll_str[] = {
	"INFO", "ERROR", "DEBUG"
};

void alpha_log(log_lv level, const char * restrict fmt, ... );

