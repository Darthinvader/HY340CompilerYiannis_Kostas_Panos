#include <string.h>

#include "symtable.h"
#include "quadgen.h"
#include "log.h"

#define QTBL_SZ 300
#define LOOPL_SZ 20
#define BRKL_SZ 20
#define BPL_SZ 20

quadtbl_t* init_quadtbl() {
	quadtbl_t *qt = malloc(sizeof(quadtbl_t));
	if(!qt) return NULL;
	qt->qt_index = 0;
	qt->qt_alloc = 0;
	qt->qtbl = NULL;
	qt->loopl = init_loop_list();
	if(!qt->loopl) {
		free(qt);
		return NULL;
	}
	return qt;
}

iquad_t** allocate_quadtbl(quadtbl_t *qt) {
	if(qt == NULL) return NULL;
	iquad_t **tmp = realloc(qt->qtbl, (QTBL_SZ + qt->qt_alloc)*sizeof(iquad_t*));
	if(!tmp) return NULL;
	qt->qt_alloc += QTBL_SZ;
	return qt->qtbl = tmp;
}

void free_quadtbl(quadtbl_t *qt) {
	if(!qt) return;
	for(int i=0; i<qt->qt_alloc; i++) {
		free_quad(qt->qtbl[i]);
	}
	free_loop_list(qt->loopl);
	free(qt);
}

lplst_t* init_loop_list() {
	lplst_t *lpl;

	lpl = malloc(sizeof(lplst_t));
	if(!lpl) return NULL;
	lpl->cont_hd_lst = NULL;
	lpl->brk_alloc = NULL;
	lpl->brk_bp_lst = NULL;
	lpl->brk_count = NULL;
	lpl->loop_alloc = 0;
	lpl->loop_count = 0;
	return lpl;
}

void free_loop_list(lplst_t *lpl) {
	if(!lpl) return;

	for(size_t i=0; i<lpl->loop_alloc;i++) {
		if(lpl->brk_alloc[i] != 0) free(lpl->brk_bp_lst[i]);
	}

	free(lpl->brk_alloc);
	free(lpl->brk_count);
	free(lpl->brk_bp_lst);
	free(lpl->cont_hd_lst);
	free(lpl);
}

int8_t enter_loop(quadtbl_t *qt) {
	if(!qt) return -1;
	lplst_t *lpl = qt->loopl;
	
	// Resize the break and continue lists if required
	if(++lpl->loop_count-1 >= lpl->loop_alloc)
		if(alloc_loop_lists(lpl) == -1) return -1;

	// Initialize break list counter for this loop level
	lpl->brk_count[lpl->loop_count-1] = 0;

	// Add the continue index to the list
	if(ins_loop_list(qt, cont_hd) == -1) return -1;

	return 1;
}

void leave_loop(quadtbl_t *qt) {
	if(!qt) return;
	lplst_t *lpl = qt->loopl;
	if(lpl->loop_count > 0) {
		backpatch_brk(qt);

		// Reset the current break list index
		lpl->brk_count[lpl->loop_count-1] = 0;
		lpl->loop_count--;
	}
}

size_t peek_loop_list(quadtbl_t *qt, lplstknd_t kind) {
	if(!qt) return 0;
	lplst_t *lpl = qt->loopl;
	if(lpl->loop_count == 0) return 0;
	size_t loop_index = lpl->loop_count-1;

	switch(kind) {
		case brk_bp: return lpl->brk_bp_lst[loop_index][lpl->brk_count[loop_index]-1];
		case cont_hd: return lpl->cont_hd_lst[loop_index];
		default: return 0;
	}
}

/* Inserts the current line number to one of the lists */
int8_t ins_loop_list(quadtbl_t *qt, lplstknd_t kind) {
	if(!qt) return -1;
	lplst_t *lpl = qt->loopl;
	if(lpl->loop_count == 0) return -1;
	size_t loop_index = qt->loopl->loop_count-1;

	switch(kind) {
		/* Here, this loop index's break list is checked for appropriate size (and resized if
		 * needed, and the new break quad line is added */
		case brk_bp:
			if(lpl->brk_count[loop_index] >= lpl->brk_alloc[loop_index])
				if(alloc_brk_list(lpl) == -1) return -1;
			lpl->brk_bp_lst[loop_index][(lpl->brk_count[loop_index])++] = qt->qt_index;
			return 1;
		// Calling with cont_hd just assigns the continue head for this loop index
		case cont_hd:
			lpl->cont_hd_lst[loop_index] = qt->qt_index;
			return 1;
	}
}

int8_t backpatch_brk(quadtbl_t *qt) {
	if(!qt) return -1;
	lplst_t *lpl = qt->loopl;
	if(lpl->loop_count == 0) return 0;
	size_t loop_index = lpl->loop_count-1;

	for(size_t i=0;i<lpl->brk_count[loop_index]; i++) {
		qt->qtbl[lpl->brk_bp_lst[loop_index][i]]->label = qt->qt_index;
	}

	return 1;
}

int8_t alloc_loop_lists(lplst_t *lpl) {
	if(!lpl) return -1;
	if(lpl->loop_count == 0) return -1;

	size_t *tmp, **tmp_bpl;

	// The counter here is initialized on demand (in enter_loop())
	tmp = realloc(lpl->cont_hd_lst, (LOOPL_SZ+lpl->loop_alloc)*sizeof(size_t));
	if(!tmp) return -1;
	lpl->cont_hd_lst = tmp;

	/* Here it is required we initialize the values to 0, because only here
	 * do we know whether the breaklist for these newly allocated loop levels
	 * actually are allocated */
	tmp = realloc(lpl->brk_alloc, (LOOPL_SZ+lpl->loop_alloc)*sizeof(size_t));
	if(!tmp) return -1;
	lpl->brk_alloc = tmp;
	for(size_t i=0; i<LOOPL_SZ; i++) lpl->brk_alloc[lpl->loop_count-1+i] = 0;
	// The counter here is initialized on demand (in enter_loop())
	tmp = realloc(lpl->brk_count, (LOOPL_SZ+lpl->loop_alloc)*sizeof(size_t));
	if(!tmp) return -1;
	lpl->brk_count = tmp;

	tmp_bpl = realloc(lpl->brk_bp_lst, (LOOPL_SZ+lpl->loop_alloc)*sizeof(size_t*));
	if(!tmp) return -1;
	lpl->brk_bp_lst = tmp_bpl;

	lpl->loop_alloc += LOOPL_SZ;
	return 1;
}

int8_t alloc_brk_list(lplst_t *lpl) {
	if(!lpl) return -1;
	if(lpl->loop_count == 0) return -1;
	size_t loop_index = lpl->loop_count-1;

	size_t *tmp = realloc(lpl->brk_bp_lst[loop_index], (BRKL_SZ + lpl->brk_count[loop_index])*sizeof(size_t*));
	if(!tmp) return -1;
	lpl->brk_alloc[loop_index] += BRKL_SZ;
	lpl->brk_bp_lst[loop_index] = tmp;
	return 1;
}

expr_t* create_expr(exprtype_t type, node_t *sym, expr_t *index, constval_t cval, expr_t *next){
	expr_t *e = malloc(sizeof(expr_t));
	e->type = type;
	e->sym = sym;
	e->index = index;
	e->cval = cval;
	e->flist.count = e->tlist.count = 0;
	e->flist.alloc = e->tlist.alloc = 0;
	e->flist.list = e->tlist.list = NULL;
	e->next = next;
	return e;
}

expr_t* empty_expr(exprtype_t type) {
	constval_t t = { .num = 0 };
	return create_expr(type, NULL, NULL, t, NULL);
}

int8_t ins_bp_list(expr_t *e, iquad_t *q, bplstknd_t kind) {
	if(!e) return -1;
	bplst_t *bpl;

	switch(kind) {
		case tlist: bpl = &e->tlist; break;
		case flist: bpl = &e->flist; break;
	}

	if(bpl->count >= bpl->alloc)
		if(alloc_bp_list(bpl, BPL_SZ) == -1) return -1;

	bpl->list[(bpl->count)++] = q;
	return 1;
}

int8_t merge_lists(expr_t *dst, expr_t *src) {
	if(!dst || !src) return -1;
	
	if(merge_list(&dst->tlist, &src->tlist) == -1) return -1;
	free(src->tlist.list);
	if(merge_list(&dst->flist, &src->flist) == -1) return -1;
	free(src->flist.list);

	return 1;
}

int8_t backpatch_tflst(quadtbl_t *qt, expr_t *e, bplstknd_t kind) {
	if(!qt || !e) return -1;
	bplst_t *bpl;

	switch(kind) {
		case tlist: bpl = &e->tlist; break;
		case flist: bpl = &e->flist; break;
	}

	if(bpl->count == 0) return 1;
	for(size_t i=0; i<bpl->count; i++)
		bpl->list[i]->label = qt->qt_index;

	bpl->count = 0;
	return 1;
}

int8_t transpose_tflst(expr_t *e) {
	if(!e) return -1;

	bplst_t tmp;

	tmp = e->tlist;
	e->tlist = e->flist;
	e->flist = tmp;

	return 1;
}

int8_t merge_list(bplst_t *dst, bplst_t *src) {
	if(!dst || !src) return -1;
	if(src->count == 0) return 1;
	size_t avail_sz = dst->alloc - dst->count;
	size_t needed_sz = 0;
	if(src->count > avail_sz) needed_sz = src->count - avail_sz;

	if(needed_sz)
		if(alloc_bp_list(dst, needed_sz) == -1) return -1;

	memcpy(&dst->list[dst->count], src->list, src->count*sizeof(iquad_t*));
	dst->count += src->count;
	return 1;
}

int8_t alloc_bp_list(bplst_t *bpl, size_t alloc_sz) {
	if(!bpl) return -1;
	iquad_t **tmp =  realloc(bpl->list, (alloc_sz+bpl->alloc)*sizeof(iquad_t*));
	if(!tmp) return -1;
	bpl->list = tmp;
	bpl->alloc += alloc_sz;
	return 1;
}

iquad_t* create_quad(quadtbl_t *qt, iopcode_t op, expr_t *res, expr_t *arg1, expr_t *arg2){
	iquad_t *q = malloc(sizeof(iquad_t));
	q->op = op;
	q->res = res;
	q->arg1 = arg1;
	q->arg2 = arg2;
	q->label = -1;
	q->line = qt->qt_index;
	return q;
}

void free_quad(iquad_t *q) {
	return;
}

iquad_t* ins_quad(quadtbl_t *qt, iquad_t *q) {
	if(!qt || !q) return NULL;
	
	// Allocate if needed and handle possible error
	if(qt->qt_index >= qt->qt_alloc) {
		if(allocate_quadtbl(qt) == NULL) {
			free_quad(q);
			return NULL;
		}
	}

	qt->qtbl[qt->qt_index++] = q;
	return q;
}

iquad_t* add_quad(quadtbl_t *qt, iopcode_t op, expr_t *res, expr_t *arg1, expr_t *arg2) {
	iquad_t *q;
	q = create_quad(qt, op, res, arg1, arg2);
	if(!q) return NULL;
	if(ins_quad(qt, q) == NULL) return NULL;
	return q;
}

void print_quad(iquad_t *q, FILE *fp) {
	if(!q || !fp) return;
	fprintf(fp, "%s ",iopcode_str[q->op]);
	if(q->res !=NULL){
		expr_t *ex = q->res;
		printexpr(ex, fp);
	}
	if(q->arg1!=NULL){
		expr_t *ex = q->arg1;
		printexpr(ex, fp);
	}
	if(q->arg2!=NULL){
		expr_t *ex = q->arg2;
		printexpr(ex, fp);
	}
	if(q->label>=0){
		fprintf(fp,"%i ",q->label);
	}
	fprintf(fp,"\n");
	if(!q) return;
}

void print_quads(quadtbl_t *qt, FILE *fp) {
	if(!qt || !fp) return;
	for(size_t i=0;i<qt->qt_index;i++){
		fprintf(fp, "%zu: ", i);
		print_quad(qt->qtbl[i], fp);
	}
}

expr_t *cnstexpr(exprtype_t type,constval_t cval){
	return create_expr(type,NULL,NULL,cval,NULL);
}

expr_t* constexpr_num(double num) {
	constval_t t = { .num = num };
	return cnstexpr(constnum_e, t);
}

expr_t* constexpr_bool(bool bl) {
	constval_t t = { .bl = bl };
	return cnstexpr(constbool_e, t);
}

expr_t* constexpr_str(char* str) {
	constval_t t = { .str = str };
	return cnstexpr(conststr_e, t);
}

expr_t *nodexpr(exprtype_t type,node_t *node){
	constval_t t = { .str = NULL };
	return create_expr(type,node,NULL,t,NULL);
}

void printexpr(expr_t *expr, FILE *fp){
	if(!expr || !fp) return;
	switch(expr->type) {
		case constnum_e:
			fprintf(fp, "%lf ",expr->cval.num);
			break;
		case constbool_e:
			if(expr->cval.bl == true) fprintf(fp, "%s ", "TRUE");
			else fprintf(fp, "%s ", "FALSE");
			break;
		case conststr_e:
			fprintf(fp, "\"%s\" ",expr->cval.str);
			break;
		case nil_e:
			break;
		default:
			if(expr->sym !=NULL){
				fprintf(fp, "%s ",expr->sym->name);
				break;
			}
			else{
				printf("%d \n",expr->type);
			}
	}
}
expr_t* member_expr(quadtbl_t *qt, expr_t *tbl, expr_t *index) {
	if(!qt || !tbl || !index) return NULL;
	// TODO: Check if lvalue/call is array
	expr_t* item = empty_expr(tblitem_e);
	item->sym = tbl->sym;
	item->index = index;

	return item;
}
