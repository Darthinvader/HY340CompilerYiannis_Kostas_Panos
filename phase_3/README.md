# CS340 - Phase 3

## Rules

- [x] statements
     - [x] empty
     - [x] stmt statements

- [x] stmt
     - [x] expr SEMCOL
     - [x] ifstmt
     - [x] whilestmt
     - [x] forstmt
     - [x] returnstmt
     - [x] BRK SEMCOL
     - [x] CNT SEMCOL
     - [x] block
     - [x] funcdef

- [x] expr
     - [x] assignexpr
     - [x] expr op{PLUS,MIN,...} expr
     - [x] term

- [x] term
     - [x] OPPAR expr CLPAR
     - [x] MIN expr
     - [x] {INC,DEC} lvalue
     - [x] lvalue {INC,DEC}
     - [x] primary



- [x] assignexpr
      - [x] lvalue ASS expr


- [x] primary
      - [x] lvalue
      - [x] call
      - [x] objectdef
      - [x] OPPAR funcdef CLPAR
      - [x] const
- [x] lvalue
      - [x] ID
      - [x] LC ID
      - [x] GLBOP ID
      - [x] member
- [x] member
      - [x] lvalue DOT ID
      - [x] lvalue OPBRACKET expr CLBRACKET
      - [x] call DOT ID
      - [x] call OPBRACKET expr CLBRACKET
- [x] call
      - [x] call OPPAR elist CLPAR
      - [x] lvalue callsufix
      - [x] OPPAR funcdef CLPAR OPPAR elist CLPAR
- [x] callsufix
      - [x] normcall
      - [x] methodcal
- [x] normcall
      - [x] OPPAR elist CLPAR
- [x] methodcal
      - [x] DDOT ID OPPAR elist CLPAR
- [x] elist
      - [x] empty
      - [x] expr elistoption
- [x] elistoption
      - [x] empty
      - [x] COMMA expr elistoption
- [x] objectdef
      - [x] OPBRACKET elist CLBRACKET
      - [x] OPBRACKET indexed CLBRACKET
- [x] indexed
      - [x] indexedelem indexoption
- [x] indexoption
      - [x] empty
      - [x] COMMA indexelem indexoption
- [x] indexelem
      - [x] OPBRACE expr COLON expr CLBRACE
- [x] block
      - [x] OPBRACE statements CLBRACE
- [x] funcdef
      - [x] FUNC funcdefoptions OPPAR idlist CLPAR block
- [x] funcdefoptions
      - [x] empty
      - [x] ID
- [x] const
      - [x] INT
      - [x] REAL
      - [x] STR
      - [x] NIL
      - [x] T
      - [x] F
- [x] idlist
      - [x] empty
      - [x] ID idlistoption
- [x] ifstmt
      - [x] ifstmtdef stmt
      - [x] ifstmtdef stmt ELSE stmt
- [x] ifstmtdef
      - [x] IF OPPAR expr CLPAR
- [x] whilestmt
      - [x] WHILE OPPAR expr CLPAR stmt
- [x] forstmt
      - [x] FOR OPPAR elist SEMCOL expr SEMCOL elist CLPAR stmt
- [x] returnstmt
      - [x] RT SEMCOL
      - [x] RT expr SEMCOL
