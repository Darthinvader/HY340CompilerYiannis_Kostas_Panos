#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "symtable.h"

#ifndef _QUADGEN_H_
#define _QUADGEN_H_

typedef enum iopcode {
	assign,         add,            sub,
	mul,            divd,           mod,
	uminus,         and,            or,
	not,            if_eq,          if_neq,
	if_lesseq,      if_greatereq,   if_less,
	if_greater,     call,           param,
	ret,            getretval,      funcstart,
	funcend,        tablecreate,
	tablegetelem,   tablesetelem,
	jump
} iopcode_t;

static const char *iopcode_str[] = {
	"ASSIGN",         "ADD",            "SUB",
	"MUL",            "DIV",           "MOD",
	"UMINUS",         "AND",            "OR",
	"NOT",            "IF_EQ",          "IF_NEQ",
	"IF_LESSEQ",      "IF_GREATEREQ",   "IF_LESS",
	"IF_GREATER",     "CALL",           "PARAM",
	"RET",            "GETRETVAL",      "FUNCSTART",
	"FUNCEND",        "TABLECREATE",
	"TABLEGETELEM",   "TABLESETELEM",
	"JUMP"
};

typedef enum exprtype {
	var_e,        tblitem_e,
	prgfunc_e,    libfunc_e,
	arthexpr_e,   boolexpr_e,    assgnexpr_e,   newtable_e,
	constnum_e,   constbool_e,   conststr_e,
	nil_e
} exprtype_t;

typedef enum qt_lplstknd_t {
	brk_bp, cont_hd
} lplstknd_t;

typedef union constval {
	double num;
	char *str;
	bool bl;
} constval_t;

typedef enum backpatch_list_kind {
	tlist, flist
} bplstknd_t;

// Forward declaration of iquad_t for backpatch_list
typedef struct quad iquad_t;

typedef struct backpatch_list {
	size_t alloc, count;
	iquad_t **list;
} bplst_t;
	
typedef struct loop_list {
	size_t *brk_alloc, *brk_count;
	size_t loop_alloc, loop_count;

	/* brk_bp_lst is a 2d array that is used to keep track of
	 * jump quad lines that need to be backpatched to properly
	 * emulate alpha's loop break functionality.
	 * cont_head_lst is a 1d array that is used to keep track ofvoid print_quads(quadtbl_t *qt);
	 * the line in which a loop starts. */
	size_t **brk_bp_lst, *cont_hd_lst;
} lplst_t;

typedef struct expr {
	exprtype_t type;
	node_t *sym;
	struct expr *index;
	constval_t cval;
	bplst_t tlist, flist;
	struct expr *next;
} expr_t;

struct quad {
	iopcode_t op;
	expr_t *res;
	expr_t *arg1, *arg2;
	int label, line;//negative values in case those values are not needed
};

typedef struct quadtable {
	lplst_t *loopl;
	size_t qt_alloc, qt_index;
	iquad_t **qtbl;
} quadtbl_t;

quadtbl_t * init_quadtbl();
iquad_t** allocate_quadtbl(quadtbl_t *qt);
void free_quadtbl(quadtbl_t *qt);

lplst_t* init_loop_list();
void free_loop_list(lplst_t *lpl);

int8_t enter_loop(quadtbl_t *qt);
void leave_loop(quadtbl_t *qt);
size_t peek_loop_list(quadtbl_t *qt, lplstknd_t kind);
int8_t ins_loop_list(quadtbl_t *qt, lplstknd_t kind);
int8_t backpatch_brk(quadtbl_t *qt);

int8_t alloc_loop_lists(lplst_t *lpl);
int8_t alloc_brk_list(lplst_t *lpl);

expr_t* create_expr(exprtype_t type, node_t *sym, expr_t *index, constval_t cval, expr_t *next_node);
expr_t* empty_expr(exprtype_t type);
expr_t* cnstexpr(exprtype_t type,constval_t cval);
expr_t* constexpr_num(double num);
expr_t* constexpr_bool(bool bl);
expr_t* constexpr_str(char *str);
expr_t* nodexpr(exprtype_t type,node_t *node);
expr_t* member_expr(quadtbl_t *qt, expr_t *tbl, expr_t *index);
expr_t* create_constexpr(constval_t cval);

int8_t ins_bp_list(expr_t *e, iquad_t *q, bplstknd_t kind);
int8_t transpose_tflst(expr_t *e);
int8_t merge_lists(expr_t *dst, expr_t *src);
int8_t backpatch_tflst(quadtbl_t *qt, expr_t *e, bplstknd_t kind);

int8_t merge_list(bplst_t *dst, bplst_t *src);
int8_t alloc_bp_list(bplst_t *bpl, size_t alloc_sz);

iquad_t* create_quad(quadtbl_t *qt, iopcode_t op, expr_t *res, expr_t *arg1, expr_t *arg2);
void free_quad(iquad_t *q);
iquad_t* ins_quad(quadtbl_t *qt, iquad_t *q);
iquad_t* add_quad(quadtbl_t *qt, iopcode_t op, expr_t *res, expr_t *arg1, expr_t *arg2);
void print_quads(quadtbl_t *qt, FILE *fp);
void printexpr(expr_t *expr, FILE *fp);
#endif
