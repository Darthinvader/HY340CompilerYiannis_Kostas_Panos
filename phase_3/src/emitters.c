#include <stdbool.h>
#include "emitters.h"
#include "symtable.h"
#include "quadgen.h"
#include "log.h"

extern int alpha_yylineno;

/* Helper functions */
expr_t *expr_booltrue(){
	constval_t t;
	t.bl = true;
	expr_t *truer = cnstexpr(constbool_e,t);
	return truer;
}

expr_t *expr_boolfalse(){
	constval_t t;
	t.bl = false;
	expr_t *falser = cnstexpr(constbool_e,t);
	return falser;
}

expr_t *bool_totmp(expr_t *expr1,quadtbl_t *qt,symtable_t *st){
	if(expr1->type == boolexpr_e){
		node_t *tmp = create_tmpnode(st);
		expr1->type = assgnexpr_e;
		expr1->sym = tmp;
		backpatch_tflst(qt,expr1,tlist);
		add_quad(qt,assign,expr1,expr_booltrue(),NULL);
		add_quad(qt,jump,NULL,NULL,NULL)->label = qt->qt_index+2;
		backpatch_tflst(qt,expr1,flist);
		add_quad(qt,assign,expr1,expr_boolfalse(),NULL);
	}
	return expr1;
}

expr_t *reduce_tobool(expr_t *expr1,quadtbl_t *qt){
	if(expr1->type !=boolexpr_e){
		ins_bp_list(expr1,add_quad(qt,if_eq,expr1,expr_booltrue(),NULL),tlist);
		ins_bp_list(expr1,add_quad(qt,jump,NULL,NULL,NULL),flist);
		//expr1->type = boolexpr_e;
	}
	return expr1;
}


expr_t *transform(quadtbl_t *qt,expr_t *expr1,iopcode_t opcode,symtable_t *st){
	if(opcode!=and && opcode!=or && expr1->type == boolexpr_e){
		return bool_totmp(expr1,qt,st);
	}
	else if((opcode == and || opcode == or) && expr1->type !=boolexpr_e){
		return reduce_tobool(expr1,qt);
	}
	return expr1;
}

void andorpatch(quadtbl_t *qt,expr_t *expr1,iopcode_t opcode){
	if(opcode == and){
		backpatch_tflst(qt,expr1,tlist);
	}
	else if(opcode == or){
		backpatch_tflst(qt,expr1,flist);
	}
}

/* Functions for reduction rules */

expr_t *expropexpr(quadtbl_t *qt, expr_t *expr1,iopcode_t opcode,expr_t *expr2,symtable_t *st){
	if(opcode<=mod){
		expr2 = bool_totmp(expr2,qt,st);
		node_t *tmpnode= create_tmpnode(st);
		expr_t *returner =nodexpr(arthexpr_e, tmpnode);
		add_quad(qt, opcode, returner, expr1, expr2);
		return returner;
	}
	else if(opcode != and && opcode!=or ){//TODO special cases for boolean operations (yes we need checks and shit)!!!
		expr2 = bool_totmp(expr2,qt,st);

		node_t *tmpnode= create_tmpnode(st);
		expr_t* returner = nodexpr(boolexpr_e,tmpnode);
		ins_bp_list(returner,add_quad(qt,opcode,expr1,expr2,NULL),tlist);
		ins_bp_list(returner,add_quad(qt,jump,NULL,NULL,NULL),flist);

		merge_lists(returner, expr1);
		merge_lists(returner,expr2);
		return returner;
	}
	else{
		expr2 = reduce_tobool(expr2,qt);
		node_t *tmpnode= create_tmpnode(st);
		expr_t* returner = nodexpr(boolexpr_e,tmpnode);
		merge_lists(returner, expr1);
		merge_lists(returner,expr2);
		return returner;
	}
}

expr_t *inc_lvalue(quadtbl_t *qt, expr_t *lvalue,symtable_t *st){
	if (lvalue->sym->id_type == user_func || lvalue->sym->id_type == lib_func){
		alpha_log(LOG_ERROR, "cannot increment user or library function with name %s\n", lvalue->sym->name);
		return NULL;
	}
	else {
		if(lvalue->type == boolexpr_e){
			lvalue = bool_totmp(lvalue,qt,st);
		}
		node_t *newnode = create_tmpnode(st);
		expr_t *returner  = nodexpr(assgnexpr_e,newnode);
		constval_t t;
		t.num =1;
		expr_t *newexpr = cnstexpr(constnum_e,t);//making a new expr for const 1 to add for create_quad
		add_quad(qt, add, lvalue, lvalue, newexpr);
		add_quad(qt,assign,returner,lvalue,NULL);
		return returner;
	}
}

expr_t *dec_lvalue(quadtbl_t *qt, expr_t *lvalue,symtable_t *st){
	if (lvalue->sym->id_type == user_func || lvalue->sym->id_type == lib_func){
		alpha_log(LOG_ERROR, "cannot decrement user or library function with name %s\n", lvalue->sym->name);
		return NULL;
	}
	else {
		if(lvalue->type == boolexpr_e){
			lvalue = bool_totmp(lvalue,qt,st);
		}
		node_t *newnode = create_tmpnode(st);
		expr_t *returner  = nodexpr(assgnexpr_e,newnode);
		constval_t t;
		t.num =1;
		expr_t *newexpr = cnstexpr(constnum_e, t);//making a new expr for const 1 to add for create_quad
		add_quad(qt, sub, lvalue, lvalue, newexpr);
		add_quad(qt,assign,returner,lvalue,NULL);
		return returner;
	}
}

expr_t *lvalue_inc(quadtbl_t *qt, expr_t *lvalue,symtable_t *st){
	if (lvalue->sym->id_type == user_func || lvalue->sym->id_type == lib_func){
		alpha_log(LOG_ERROR, "cannot increment user or library function with name %s\n", lvalue->sym->name);
		return NULL;
	}
	else {
		if(lvalue->type == boolexpr_e){
			lvalue = bool_totmp(lvalue,qt,st);
		}
		node_t *newnode = create_tmpnode(st);
		expr_t *returner  = nodexpr(assgnexpr_e,newnode);
		add_quad(qt,assign,returner,lvalue,NULL);
		constval_t t;
		t.num =1;
		expr_t *newexpr = cnstexpr(constnum_e, t);//making a new expr for const 1 to add for create_quad
		add_quad(qt, add, lvalue, lvalue, newexpr);
		return returner;
	}
}

expr_t *lvalue_dec(quadtbl_t *qt, expr_t *lvalue,symtable_t *st){
	if (lvalue->sym->id_type == user_func || lvalue->sym->id_type == lib_func){
		alpha_log(LOG_ERROR, "cannot decrement user or library function with name %s\n", lvalue->sym->name);
		return NULL;
	}
	else {
		if(lvalue->type == boolexpr_e){
			lvalue = bool_totmp(lvalue,qt,st);
		}
		node_t *newnode = create_tmpnode(st);
		expr_t *returner  = nodexpr(assgnexpr_e,newnode);
		add_quad(qt,assign,returner,lvalue, NULL);
		constval_t t;
		t.num =1;
		expr_t *newexpr = cnstexpr(constnum_e, t);//making a new expr for const 1 to add for create_quad
		add_quad(qt, sub, lvalue, lvalue, newexpr);
		return returner;
	}
}

expr_t *lvalueassexpr(quadtbl_t *qt, expr_t *lvalue, expr_t *expr,symtable_t *st){
	if (lvalue->sym->id_type == user_func || lvalue->sym->id_type == lib_func) {
		alpha_log(LOG_ERROR, "cannot assign user or library function with name %s\n", lvalue->sym->name);
	}

	expr_t *rete;
	if(expr->type == boolexpr_e)
		expr = bool_totmp(expr,qt,st);

	if(lvalue->type == tblitem_e) {
		add_quad(qt, tablesetelem, lvalue, lvalue->index, expr);
		rete = emit_iftblitem(qt, lvalue,st);
	} else {
		// Add assignment quad
		add_quad(qt, assign, lvalue, expr, NULL);
		// Assign to and return temporary
		rete = empty_expr(assgnexpr_e);
		rete->sym = create_tmpnode(st);
		add_quad(qt, assign, rete, lvalue, NULL);
	}

	return rete;
}

expr_t *minusexpr(quadtbl_t *qt, expr_t *expr1,symtable_t *st){
	node_t *newnode = create_tmpnode(st);
	expr_t *returner = nodexpr(arthexpr_e, newnode);
	constval_t cval;
	cval.num = -1;
	expr_t *newexpr = cnstexpr(constnum_e,cval);//making a new expr for const 1 to add for create_quad
	add_quad(qt, mul, returner,newexpr , expr1);
	return returner;
}

/* Table related functions */
expr_t* emit_iftblitem(quadtbl_t *qt, expr_t *e,symtable_t *st) {
	if(!e) return NULL;
	if(e->type != tblitem_e) return e;

	expr_t *tmp = empty_expr(var_e);
	tmp->sym = create_tmpnode(st);
	add_quad(qt, tablegetelem, e, e->index, tmp);
	return tmp;
}

expr_t* emit_tblsetelist(quadtbl_t *qt, expr_t *tbl, expr_t *elist) {
	if(!qt || !tbl) return NULL;
	size_t i=0;

	for(expr_t* e = elist; e != NULL; e = e->next) {
		add_quad(qt, tablesetelem, tbl, constexpr_num(i++), e);
	}

	return tbl;
}

expr_t* emit_tblsetindxel(quadtbl_t *qt, expr_t *tbl, expr_t *indices, expr_t *values) {
	if(!qt || !tbl || !indices || !values) return NULL;

	expr_t *i = indices, *v = values;

	while(i) {
		add_quad(qt, tablesetelem, tbl, i, v);
		i = i->next;
		v = v->next;
	}
	return tbl;
}

/* id to expression related functions */
expr_t *IDmaker(char *name,symtable_t *st){
	node_t *node = find_node_fromscope(st,name,st->curr_scope);
	if(node==NULL){
		idflag_t type = func_local;//i dont think we keep this anymore... i dont know
		if(st->curr_nest == 0) type = prg_var;
		node = add_node(st,name,type,NULL,alpha_yylineno,st->curr_scope,1);
	}
	else if(node->scope!=0 && node->scope<peek_nestscope(st)){
		alpha_log(LOG_ERROR, "id %s is out of scope", name);
		return NULL;
	}
	expr_t *newexpr = nodexpr(var_e, node);//we need something to translate the id to a expr so we can pass it
	return newexpr;
}

expr_t *localIDmaker(char *name,symtable_t *st){
	node_t *node;
	if(is_lib_func(st,name)){
		alpha_log(LOG_ERROR, "id %s is in use by library function\n", name);
		return NULL;
	}
	else if(!(node = find_node(st,name,st->curr_scope))){
		idflag_t flag;
		if(st->formalflag != 0){
			flag = formal_arg;
		}
		else if(st->spaceflag>0){
			flag = func_local;
		}
		else{
			flag = prg_var;
		}
		node = add_node(st,name,flag,NULL,alpha_yylineno,st->curr_scope,1);
		expr_t *newexpr = nodexpr(var_e, node);//we need something to translate the id to a expr so we can pass it
		return newexpr;
	}

	return nodexpr(var_e, node);
}

expr_t *GlobalIDmaker(char *name,symtable_t *st){
	node_t *node;
	if(!(node = find_node(st,name,0))){
		alpha_log(LOG_ERROR, "id %s is out of scope", name);
		return NULL;
	}
	else{
		expr_t *newexpr = nodexpr(var_e, node);//we need something to translate the id to a expr so we can pass it
		return newexpr;
	}
}

int Return(quadtbl_t *qt, expr_t *returnexpr,symtable_t *st){
	if(peek_nestscope(st) == 0){
		alpha_log(LOG_ERROR, "return when not inside function");//error here
		return -1;
	}

	add_quad(qt, ret, returnexpr, NULL, NULL);
	return 0;
}

expr_t *intmaker(int integer,quadtbl_t *qt,symtable_t *st){//normally a enum would be good but we only have 5 values so its ok..
	constval_t t;
	t.num = integer;
	expr_t *expr = cnstexpr(constnum_e,t);
	return expr;
}

expr_t *realmaker(double real,quadtbl_t *qt,symtable_t *st){//normally a enum would be good but we only have 5 values so its ok..
	constval_t t;
	t.num = real;
	expr_t *expr = cnstexpr(constnum_e,t);
	return expr;
}

expr_t *stringmaker(char *string,quadtbl_t *qt,symtable_t *st){//normally a enum would be good but we only have 5 values so its ok..
	constval_t t;
	t.str = string;
	expr_t *expr = cnstexpr(conststr_e,t);
	return expr;
}
expr_t *NILmaker(quadtbl_t *qt,symtable_t *st){//normally a enum would be good but we only have 5 values so its ok..
	constval_t t;
	t.str = "IM NULL";
	expr_t *expr = cnstexpr(nil_e,t);
	return expr;
}
expr_t *FTmaker(int flag,quadtbl_t *qt,symtable_t *st){//normally a enum would be good but we only have 5 values so its ok..
	expr_t *expr;
	if(flag == 0)expr = expr_boolfalse();
	else expr = expr_booltrue();
	return expr;
}



void printparams(expr_t *expr1,quadtbl_t *qt){
	if(expr1 == NULL){return;}
	printparams(expr1->next,qt);
	add_quad(qt,param,expr1,NULL,NULL);
}