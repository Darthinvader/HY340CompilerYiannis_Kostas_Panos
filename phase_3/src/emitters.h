#include "quadgen.h"
int lcfmgb;


expr_t *expropexpr(quadtbl_t *qt, expr_t *expr1,iopcode_t opcode,expr_t *expr2,symtable_t *st);

expr_t *inc_lvalue(quadtbl_t *qt, expr_t *lvalue,symtable_t *st);
expr_t *dec_lvalue(quadtbl_t *qt, expr_t *lvalue,symtable_t *st);
expr_t *lvalue_inc(quadtbl_t *qt, expr_t *lvalue,symtable_t *st);
expr_t *lvalue_dec(quadtbl_t *qt, expr_t *lvalue,symtable_t *st);
expr_t *lvalueassexpr(quadtbl_t *qt, expr_t *lvalue,expr_t *expr,symtable_t *st);
expr_t *minusexpr(quadtbl_t *qt, expr_t *expr1,symtable_t *st);

expr_t *IDmaker(char *name,symtable_t *st);
expr_t *GlobalIDmaker(char *name,symtable_t *st);
expr_t *localIDmaker(char *name,symtable_t *st);

int Return(quadtbl_t *qt, expr_t *returnexpr,symtable_t *st);

expr_t *booltoarithm(expr_t *expr1,quadtbl_t *qt);
expr_t *constbooltrue();
expr_t *constboolfalse();


void andorpatch(quadtbl_t *qt,expr_t *expr1,iopcode_t opcode);
expr_t *reduce_tobool(expr_t *expr1,quadtbl_t *qt);

expr_t *reverselists(quadtbl_t *qt,expr_t *expr1);

expr_t *transform(quadtbl_t *qt,expr_t *expr1,iopcode_t opcode,symtable_t *st);
expr_t* emit_iftblitem(quadtbl_t *qt, expr_t *e,symtable_t *st);
expr_t* emit_tblsetelist(quadtbl_t *qt, expr_t *tbl, expr_t *elist);
expr_t* emit_tblsetindxel(quadtbl_t *qt, expr_t *tbl, expr_t *indices, expr_t *values);
//const here

expr_t *intmaker(int integer,quadtbl_t *qt,symtable_t *st);
expr_t *realmaker(double real,quadtbl_t *qt,symtable_t *st);
expr_t *stringmaker(char *string,quadtbl_t *qt,symtable_t *st);
expr_t *NILmaker(quadtbl_t *qt,symtable_t *st);
expr_t *FTmaker(int flag,quadtbl_t *qt,symtable_t *st);
void printparams(expr_t *expr1,quadtbl_t *qt);
expr_t *bool_totmp(expr_t *expr1,quadtbl_t *qt,symtable_t *st);
void printparams(expr_t *expr1,quadtbl_t *qt);
