#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef _SYMTABLE_H_
#define _SYMTABLE_H_

typedef enum idflag {
	user_func,lib_func,func_local,prg_var,formal_arg
} idflag_t;

static const char *idflag_str[] = {
	"user function", "lib function", "function local", "program variable", "formal argument"
};

typedef struct node {
	char *name;
	idflag_t id_type;
	struct node *formals;
	int line;
	int scope;
	int active;
	struct node *next;
	struct node *scopeNext;
	size_t scope_id;
} node_t;

typedef struct symtable {
	size_t curr_scope, max_scope, alloc_scope;
	size_t curr_nest, alloc_nest;
	size_t prgvar_offset;
	node_t **hash_tbl;
	node_t **scope_lst;
	size_t *func_scope;
	size_t currls,allocls;
	size_t currfs,allocfs;
	size_t *fnclcsp;
	size_t *fncfmsp;
	int spaceflag;
	int formalflag;

} symtable_t;


int hash_fun(char *str);

symtable_t* init_symtable();
void free_symtable(symtable_t *st);

node_t* create_node(char *name,idflag_t id_type,node_t *formals,int line,int scope,int active);
void free_node(node_t *node);
void ins_node(symtable_t *st,node_t *node);
node_t* add_node(symtable_t *st,char *name,idflag_t id_type,node_t *formals,int line,int scope,int active);

node_t* find_node(symtable_t *st,char* name,int scope);
node_t* find_node_fromscope(symtable_t *st,char* name,int currScope);
node_t* deact_node(symtable_t *st,char *name,int scope);
void print_node(node_t *node);
void print_all_nodes(symtable_t *st);

void deact_scope(symtable_t *st,int scope);
void advance_scope(symtable_t *st);
void retract_scope(symtable_t *st);
void allocate_scope(symtable_t *st);

size_t peek_nestscope(symtable_t *st);
void push_nestscope(symtable_t *st);
void pop_nestscope(symtable_t *st);
void allocate_nestscope(symtable_t *st);

void init_libfunc(symtable_t *st);
int is_lib_func(symtable_t *st,char *name);

node_t* add_formal_lst(node_t *formals,node_t *formal);

node_t *create_tmpnode(symtable_t *st);
void reset_tmp();



void push_fls(symtable_t *st);

void push_ffs(symtable_t *st);

void pop_fls(symtable_t *st);
void pop_ffs(symtable_t *st);

void inc_fls(symtable_t *st);
void inc_ffs(symtable_t *st);

size_t get_cfls(symtable_t *st);

size_t getinc_cfls(symtable_t *st);

size_t get_cffs(symtable_t *st);
size_t getinc_cffs(symtable_t *st);

void allocate_fls(symtable_t *st);

void allocate_ffs(symtable_t *st);


void inc_prgv(symtable_t *st);

size_t getincprgv_offset(symtable_t *st);
size_t getprgv_offset(symtable_t *st);

#endif



