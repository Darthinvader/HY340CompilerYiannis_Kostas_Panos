#include "symtable.h"
#include "log.h"

#define HASH_TBLSZ 6869
#define SCOPE_LSTSZ 50
#define NESTSCOPE_TBLSZ 10
#define SCOPESPC_TBLSZ 10
//TODO extern alphayylineno
//TODO move symtable_t and quadtable_qt close by
char tmp_str[27] = { "_tmp_" };
size_t tmp_ctr = 0l;
extern int alpha_yylineno;

/* Hash function for the symbol hashtable */
int hash_fun(char *str){
    unsigned long hash = 5381;
    int c;
    while ((c = *(str++))){
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
	}
	hash = hash%HASH_TBLSZ;
    return hash;
}

/* Initializes and returns a new symbol table */
symtable_t* init_symtable() {
	symtable_t *st =  malloc(sizeof(symtable_t));
	st->hash_tbl = malloc(HASH_TBLSZ*sizeof(node_t*));
	for(size_t i=0; i<HASH_TBLSZ; i++) st->hash_tbl[i] = NULL;
	st->scope_lst = malloc(SCOPE_LSTSZ*sizeof(node_t*));
	for(size_t i=0; i<SCOPE_LSTSZ; i++) st->scope_lst[i] = NULL;
	st->func_scope = malloc(NESTSCOPE_TBLSZ*sizeof(size_t));

	st->curr_scope = 0;
	st->max_scope = 0;
	st->alloc_scope = SCOPE_LSTSZ;

	st->curr_nest = 0;
	st->alloc_nest = NESTSCOPE_TBLSZ;

	st->currls =0;
	st->currfs = 0;
	st->allocls=0;
	st->allocfs = 0;
	st->spaceflag = 0;
	st->formalflag = 0;
	init_libfunc(st);
	return st;
}

/* De-Allocates a symbol table */
void free_symtable(symtable_t *st) {
	node_t *node, *tmp;
	for(size_t i=0; i<st->alloc_scope; i++) {
		node = st->scope_lst[i];
		while(node != NULL) {
			tmp = node;
			node = node->scopeNext;
			free_node(tmp);
		}
	}

	free(st->hash_tbl);
	free(st->scope_lst);
	free(st->func_scope);
	free(st);
}

/* Allocates and returns a new node */
node_t* create_node(char *name,idflag_t id_type,node_t *formals,int line,int scope,int active){
	node_t *node = malloc(sizeof(node_t));
	node->name = name;
	node->id_type = id_type;
	node->formals = formals;
	node->line = line;
	node->scope = scope;
	node->active = active;
	node->next = NULL;
	node->scopeNext = NULL;
	return node;
}

/* De-Allocates a node */
void free_node(node_t *node) {
	free(node->name);
	free(node);
}

/* Inserts a node into the symbol table */
void ins_node(symtable_t *st,node_t *node){
	int hash = hash_fun(node->name);
	node->next = st->hash_tbl[hash];
	node->scopeNext = st->scope_lst[node->scope];
	st->hash_tbl[hash] = node;
	st->scope_lst[node->scope] = node;
	return;
}

/* create_node and insert_node combined */
node_t* add_node(symtable_t *st,char *name,idflag_t id_type,node_t *formals,int line,int scope,int active){
	node_t *node = create_node(name, id_type, formals, line, scope, active);
	if(id_type == user_func || id_type == lib_func){
		node->scope_id = -1;
	}
	if(id_type == func_local){
		node->scope_id = getinc_cfls(st);
	}
	if(id_type == prg_var){
		node->scope_id = getincprgv_offset(st);
	}
	if(id_type == formal_arg){
		node->scope_id = getinc_cffs(st);
	}
	ins_node(st, node);
	return node;
}

/* Tries to find a node of given name and in given scope */
node_t* find_node(symtable_t *st,char* name,int scope){
	int hash = hash_fun(name);
	node_t *node = st->hash_tbl[hash];
	while(node!=NULL){
		if(strcmp(node->name,name) ==0 && (node->scope == scope) && node->active == 1){
			return node;
		}
		node = node->next;
	}
	return NULL;
}

/* Tries to find a node in descending order from given scope */
node_t *find_node_fromscope(symtable_t *st,char* name,int scope){
	node_t *curr =NULL;
	for(int i=scope;i>=0;i--){
		curr = find_node(st,name,i);
		if(curr!=NULL) break;
	}
	return curr;
}

/* Tries to deactivate a node of given name in given scope */
node_t *deact_node(symtable_t *st,char *name,int scope){
	int hash = hash_fun(name);
	node_t *node = st->hash_tbl[hash];
	while(node!=NULL){
		if(strcmp(node->name,name) == 0 && node->scope == scope && node->active == 1){
			printf("%i",(*node).scope);fflush(stdout);
			node->active = 0;
			return node;
		}
		node = (*node).next;
	}
	return node;
}

/* Prints a node */
void print_node(node_t *node) {
	alpha_log(LOG_INFO, "\t[name: %s, id_type: %s, ln: %i]\n", node->name, idflag_str[node->id_type], node->line);
	return;
}

/* Print nodes up to the last accessed scope */
void print_all_nodes(symtable_t *st){
	node_t *node;
	alpha_log(LOG_INFO, "Max Scope was %li\n", st->max_scope);
	for(size_t i=0;i<=st->max_scope;i++){
		alpha_log(LOG_INFO, "Scope %zu\n", i);
		node = st->scope_lst[i];
		while(node!=NULL){
			print_node(node);
			node = node->scopeNext;
		}
		printf("\n");
	}
}

/* Deactivates a given scope */
void deact_scope(symtable_t *st,int scope){
	node_t *node = st->scope_lst[scope];
	while(node){
		node->active = 0;
		node = node->scopeNext;
	}
}

/* Advances the scope by one */
void advance_scope(symtable_t *st) {
	if(st->curr_scope+1 > st->max_scope) st->max_scope++;
	if(++(st->curr_scope) >= st->alloc_scope) allocate_scope(st);
}

/* Retracts the scope by one */
void retract_scope(symtable_t *st) {
	if(st->curr_scope > 0) st->curr_scope--;
}

/* Allocates new space to the scope tables */
void allocate_scope(symtable_t *st) {
	node_t **tmp = realloc(st->scope_lst, (HASH_TBLSZ+st->alloc_scope)*sizeof(node_t*));
	if(!tmp) ; // TODO:handle error
	else {
		for(size_t i=0; i<HASH_TBLSZ; i++) tmp[st->alloc_scope+i] = NULL;
		st->scope_lst = tmp;
		st->alloc_scope += HASH_TBLSZ;
	}
}

/* Checks the scope of currently nested function */
size_t peek_nestscope(symtable_t *st) {
	return st->curr_nest>0 ? st->func_scope[st->curr_nest-1] : 0;
}

/* Stores current scope as scope of a nested function */
void push_nestscope(symtable_t *st) {
	if(st->curr_nest+1 >= st->alloc_nest) allocate_nestscope(st);
	st->func_scope[++st->curr_nest-1] = st->curr_scope;
}

/* "Pop" nested function counter */
void pop_nestscope(symtable_t *st) {
	if(st->curr_nest > 0) st->curr_nest--;
}

void push_fls(symtable_t *st){//TODO
	if(st->currls+1 > st->allocls){
		allocate_fls(st);
		st->fnclcsp[st->currls] = 0;
	}
	printf("here??");fflush(stdout);
	st->currls++;
}

void push_ffs(symtable_t *st){//TODO
	if(st->currfs+1 > st->allocfs){
		allocate_ffs(st);
		st->fncfmsp[st->currfs] = 0;
	}
	st->currfs++;
}

void pop_fls(symtable_t *st){
	if(st->currls>0){
		st->currls--;
	}
}
void pop_ffs(symtable_t *st){
	if(st->currfs>0){
		st->currfs--;
	}
}

void inc_fls(symtable_t *st){
	st->currls++;
}
void inc_ffs(symtable_t *st){
	st->currfs++;
}

size_t get_cfls(symtable_t *st){
	return st->fnclcsp[st->currls-1];
}

size_t getinc_cfls(symtable_t *st){
	printf("dead");fflush(stdout);
	printf("%i, %i",st->currls,st->allocls);fflush(stdout);
	st->fnclcsp[st->currls-1]++;
	printf("not dead");fflush(stdout);
	return (st->fnclcsp[st->currls-1] -1);
}


size_t get_cffs(symtable_t *st){
	return st->fncfmsp[st->currfs-1];
}

size_t getinc_cffs(symtable_t *st){
	st->fncfmsp[st->currfs-1]++;
	return (st->fncfmsp[st->currfs-1] -1);
}

void allocate_fls(symtable_t *st) {//TODO for ffs and fls
	size_t *tmp = realloc(st->fnclcsp, (1+st->allocls)*sizeof(size_t));
	if(!tmp) ; // TODO:handle error
	else {
		st->fnclcsp = tmp;
		st->allocls++;
	}
}

void allocate_ffs(symtable_t *st) {//TODO for ffs and fls
	size_t *tmp = realloc(st->fncfmsp, (1+st->allocfs)*sizeof(size_t));
	if(!tmp) ; // TODO:handle error
	else {
		st->fncfmsp = tmp;
		st->allocfs++;
	}
}

/* Allocates new space to the "nested function scope"-table */
void allocate_nestscope(symtable_t *st) {
	size_t *tmp = realloc(st->func_scope, (NESTSCOPE_TBLSZ+st->alloc_nest)*sizeof(size_t));
	if(!tmp) ; // TODO:handle error
	else {
		st->func_scope = tmp;
		st->alloc_nest += NESTSCOPE_TBLSZ;
	}
}

/* Checks if a lib function exists with given name */
int is_lib_func(symtable_t *st,char *name){
	node_t *curr = find_node(st,name,0);
	if(curr!=NULL &&(curr->id_type == lib_func)){
		return 1;
	}
	else{
		return 0;
	}
}

/* Adds lib functions to given symbol table */
void init_libfunc(symtable_t *st){
	add_node(st,strdup("print"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("input"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("objectmemberkeys"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("objecttotalmembers"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("objectcopy"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("totalarguments"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("argument"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("typeof"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("strtonum"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("sqrt"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("cos"),lib_func,NULL,-1,0,1);
	add_node(st,strdup("sin"),lib_func,NULL,-1,0,1);
}

node_t *add_formal_lst(node_t *formals,node_t *formal){
	formal->formals = formals;
	return formal;
};


char* tmp_name(){
	char *name;
	snprintf(tmp_str+5,22,"%zu", tmp_ctr++);
	name = malloc(sizeof(char)*27);
	strcpy(name, tmp_str);
	return name;
}

void reset_tmp(){
	tmp_ctr = 0;
}

node_t *create_tmpnode(symtable_t *st){
	//we may not need to insert the temporary in the symboltable
	node_t *tmpnode;
	tmpnode = add_node(st,tmp_name(),prg_var,NULL,alpha_yylineno,st->curr_scope,1);
	return tmpnode;
}
/*
int main(){
	Node *node = CreateAndInsert("silver",localVariable,NULL,1,1,1);
	int hash = hashFunction("silver");
	printNode(node);
	printNode(hashTable[hash]);
	Node *node2 = CreateAndInsert("silver",localVariable,NULL,1,2,1);
	hashTable[hash] = node2;
	Node *Nodeformal = CreateAndInsert("sk",formalVariable,NULL,1,1,1);
	Node *Nodeformal2 = CreateAndInsert("ska",formalVariable,NULL,1,1,1);
	addToFormalList(Nodeformal,Nodeformal2);
	Node *noder = Nodeformal2->formals;
	printf("%s",(*noder).name);
	deactivate("silver",1);
	FindNode("silver",1);
	printf("%s", node->name);
	printf("%i",node->idType);
	printf("%i",node->line);
	printf("%i",node->scope);
	printf("%i",node->active);
	return 0;


}*/

void inc_prgv(symtable_t *st){
	st->prgvar_offset++;
}

size_t getincprgv_offset(symtable_t *st){
	st->prgvar_offset++;
	return st->prgvar_offset -1;
}

size_t getprgv_offset(symtable_t *st){
	return st->prgvar_offset;
}
