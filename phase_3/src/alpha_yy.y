%{ /*User Code	*/
	#include <stdio.h>
	#include <stdlib.h>
	#include <assert.h>
	#include <string.h>

	#include "lex.alpha_yy.h"
	#include "symtable.h"
	#include "quadgen.h"
	#include "emitters.h"
	#include "log.h"

	char dfn[3] = "_fa";
	int yyerror(symtable_t *st, quadtbl_t *qt, char *str);
	iquad_t *str;
	int callelist = 0;
%}

%define api.prefix {alpha_yy}

%parse-param {symtable_t *st} {struct quadtable *qt}

%union {
	int intVal;
	float floatVal;
	char *strVal;
	node_t *symbol;
	struct expr *expression;
	struct looping{
		int val1;
		int val2;
	}looper;
	struct csfx {
		expr_t *expr;
		char *id;
		enum { norm, memb } sfknd;
	} sfxe;
	struct indexedelem {
		expr_t *index;
		expr_t *value;
	} indxel;
}
%token IF
%token ELSE
%token WHILE
%token FOR
%token FUNC
%token RT
%token BRK
%token CNT
%token AND
%token OR
%token NOT
%token LC
%token T
%token F
%token NIL
%token ASS
%token PLUS
%token MIN
%token MUL
%token DIV
%token MOD
%token EQ
%token NEQ
%token INC
%token DEC
%token GT
%token LT
%token GTEQ
%token LTEQ
%token OPBRACE
%token CLBRACE
%token OPBRACKET
%token CLBRACKET
%token OPPAR
%token CLPAR
%token SEMCOL
%token COMMA
%token GLBOP
%token COLON
%token DDOT
%token DOT

%token <intVal>   INT
%token <floatVal> REAL
%token <strVal>   STR ID

%type <sfxe> callsufix
%type <sfxe> methodcal
%type <sfxe> normcall

%type <indxel> indexed indexedelem indexoption

%type <expression> term
%type <expression> expr
%type <expression> assignexpr//
%type <expression> primary
%type <expression> lvalue
%type <expression> const
%type <expression> funcdef
%type <expression> elist
%type <expression> elistoption
%type <expression> call
%type <expression> objectdef
%type <expression> member
%type <intVal> whilestmt
%type <looper> forstmt
%type <symbol> funcdefoptions
%type <expression> ifstmtdef
%start program

%right ASS
%left OR
%left AND
%nonassoc EQ NEQ
%nonassoc GT GTEQ LT LTEQ
%left PLUS MIN
%left MUL DIV MOD
%right NOT INC DEC UMIN
%left DOT DDOT
%left OPBRACKET CLBRACKET
%left OPPAR CLPAR


%%
program:
	statements {
		alpha_log(LOG_DEBUG, "program->stmt statements\n");
	}
	;

statements:
	{
		alpha_log(LOG_DEBUG, "statements -> empty \n");
	}

	| stmt {reset_tmp();}statements {
		alpha_log(LOG_DEBUG, "statements -> stmt \n");
	}
	;

stmt:
	expr SEMCOL {
		alpha_log(LOG_DEBUG, "stmt->expr \n");
		backpatch_tflst(qt,$1,tlist);
		backpatch_tflst(qt,$1,flist);
		$1 = bool_totmp($1, qt, st);
	}
	| ifstmt		{alpha_log(LOG_DEBUG, "stmt -> ifstmt \n");}//NOTHING HERE
	| whilestmt	{alpha_log(LOG_DEBUG, "stmt-> whilestmt \n");}//NOTHING HERE
	| forstmt		{alpha_log(LOG_DEBUG, "stmt -> forstmt \n");}//NOTHING HERE
	| returnstmt	{alpha_log(LOG_DEBUG, "stmt ->returnstmt");}
	| BRK SEMCOL {
		alpha_log(LOG_DEBUG, "stmt -> Break \n	");
		/* add_quad inserts the quad in the current index held by qt->qt_index
		 * Therefore, we save the index line before adding the quad so the correct
		 * line gets backpatched. */
		if(ins_loop_list(qt, brk_bp) == -1) YYABORT;
		if(add_quad(qt, jump, NULL, NULL, NULL) == NULL) YYABORT;
	}
	| CNT SEMCOL {
		alpha_log(LOG_DEBUG, "stmt -> Continue \n");
		iquad_t *q;
		if((q = add_quad(qt, jump, NULL,NULL,NULL)) == NULL) YYABORT;
		q->label = peek_loop_list(qt, cont_hd);
	}
	| block		{alpha_log(LOG_DEBUG, "stmt ->block \n");}//NOTHING HERE
	| funcdef		{alpha_log(LOG_DEBUG, "stmt -> funcdef \n");}//NOTHING HERE
	| SEMCOL		{alpha_log(LOG_DEBUG, "stmt -> ;\n");}
	;

expr:
	assignexpr {
		alpha_log(LOG_DEBUG, "expr - > assignexpr \n");
		$$ = $1;
	}

	| expr PLUS{
		$1 = transform(qt,$1,add,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,add,$4,st);
	}
	| expr MIN{
		$1 = transform(qt,$1,sub,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,sub,$4,st);
	}
	| expr MUL{
		$1 = transform(qt,$1,mul,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,mul,$4,st);
	}
	| expr DIV{
		$1 = transform(qt,$1,divd,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,divd,$4,st);
	}
	| expr MOD{
		$1 = transform(qt,$1,mod,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,mod,$4,st);
	}
	| expr GT{
		$1 = transform(qt,$1,if_greater,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,if_greater,$4,st);
	}
	| expr GTEQ{
		$1 = transform(qt,$1,if_greatereq,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,if_greatereq,$4,st);
	}
	| expr LT{
		$1 = transform(qt,$1,if_less,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,if_less,$4,st);
	}
	| expr LTEQ{
		$1 = transform(qt,$1,if_lesseq,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,if_lesseq,$4,st);
	}
	| expr EQ{
		$1 = transform(qt,$1,if_eq,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,if_eq,$4,st);
	}
	| expr NEQ{
		$1 = transform(qt,$1,if_neq,st);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,if_neq,$4,st);
	}
	| expr AND{
		$1 = transform(qt,$1,and,st);
		andorpatch(qt,$1,and);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,and,$4,st);
	}
	| expr OR{
		$1 = transform(qt,$1,or,st);
		andorpatch(qt,$1,or);
		} expr {
		alpha_log(LOG_DEBUG, "expr - >expr op expr \n");
		$$ = expropexpr(qt,$1,or,$4,st);
	}

	| term {
		alpha_log(LOG_DEBUG, "expr -> term \n");
		$$=$1;
		//DONE
	}
	;


term:
	OPPAR expr CLPAR {
		alpha_log(LOG_DEBUG, "term -> (expr) \n");
		$2 = bool_totmp($2, qt, st);
		$$ = $2;// like literally that's all it's amazing simple
	}

	| MIN expr %prec UMIN {//DONE
		alpha_log(LOG_DEBUG, "term -> -expr \n");
		$$ = minusexpr(qt,$2,st);
		//DONE
	}

	| NOT expr {
		alpha_log(LOG_DEBUG, "term -> not expr \n");
		$2 = reduce_tobool($2,qt);
		printf("do i go here?");fflush(stdout);
		transpose_tflst($2);
		$$ = $2;
	}

	| INC lvalue {
		alpha_log(LOG_DEBUG, "term -> ++lvalue \n");
		$2 = emit_iftblitem(qt, $2, st);
		$$ = inc_lvalue(qt,$2,st);
		if(!$$) YYABORT;
	}

	| lvalue INC {
		alpha_log(LOG_DEBUG, "term -> lvalue++ \n");
		$1 = emit_iftblitem(qt, $1, st);
		$$ = lvalue_inc(qt,$1,st);
		if(!$$) YYABORT;
	}

	| DEC lvalue {
		alpha_log(LOG_DEBUG, "term -> --lvalue \n");
		$2 = emit_iftblitem(qt, $2, st);
		$$ = dec_lvalue(qt,$2,st);
		if(!$$) YYABORT;
	}

	| lvalue DEC {
		alpha_log(LOG_DEBUG, "term -> lvalue-- \n");
		$1 = emit_iftblitem(qt, $1, st);
		$$ = lvalue_dec(qt,$1,st);
		if(!$$) YYABORT;
	}

	| primary {
		alpha_log(LOG_DEBUG, "term -> primary \n");
		$$ = $1;//That's all again...wtf why?Because primary on its own does nothing!!
	}
	;

assignexpr:
	lvalue ASS expr {
		alpha_log(LOG_DEBUG, "assignexpr -> lvalue = expr \n");
		$1 = emit_iftblitem(qt, $1, st);
		backpatch_tflst(qt,$3,tlist);
		backpatch_tflst(qt,$3,flist);
		$$ = lvalueassexpr(qt,$1,$3,st);
		if(!$$) YYABORT;
	}
	;

primary:
		lvalue {
			alpha_log(LOG_DEBUG, "primary -> lvalue \n");
			$$ = emit_iftblitem(qt, $1, st);
		}

		| call {
			alpha_log(LOG_DEBUG, "primary -> call \n");
			$$ = $1;
		}

		| objectdef {
			alpha_log(LOG_DEBUG, "primary -> objectdef\n");
			$$ = $1;
		}

		| OPPAR funcdef CLPAR {
			alpha_log(LOG_DEBUG, "primary -> (funcdef)\n");
			$$ = $2;
		}

		| const {
			alpha_log(LOG_DEBUG, "primary -> const\n");
			$$ = $1;
		}
		;

lvalue:
	ID {
		alpha_log(LOG_DEBUG, "lvalue -> id \n");
		$$ = IDmaker($1,st);
		if(!$$) YYABORT;
	}

	| LC ID {
		alpha_log(LOG_DEBUG, "lvalue -> local id \n");
		$$ = localIDmaker($2,st);
		if(!$$) YYABORT;
	}

	| GLBOP ID {
		alpha_log(LOG_DEBUG, "lvalue -> ::id \n");
		$$ = GlobalIDmaker($2,st);
		if(!$$) YYABORT;
	}

	| member {
		alpha_log(LOG_DEBUG, "lvalue ->member \n");
		$$ = $1;
	}
	;

member:
	lvalue DOT ID {
		alpha_log(LOG_DEBUG, "member -> lvalue.id \n");
		// Create index expression
		expr_t *index = constexpr_str($3);
		// Reduce lvalue to temporary if needed (for chained members)
		$1 = emit_iftblitem(qt, $1, st);
		// Create member
		if(($$ = member_expr(qt, $1, index)) == NULL) YYABORT;
	}

	| lvalue OPBRACKET expr CLBRACKET {
		alpha_log(LOG_DEBUG, "member -> lvalue[expr] \n");
		// Reduce expression to temporary
		expr_t *index = bool_totmp($3, qt, st);
		if(($$ = member_expr(qt, $1, index)) == NULL) YYABORT;
	}

	| call DOT ID {
		alpha_log(LOG_DEBUG, "member -> call.id\n");
		expr_t *index = constexpr_str($3);
		if(($$ = member_expr(qt, $1, index)) == NULL) YYABORT;
	}

	| call OPBRACKET expr CLBRACKET   {
		alpha_log(LOG_DEBUG, "member -> call[expr] \n");
		expr_t *index = bool_totmp($3, qt, st);
		if(($$ = member_expr(qt, $1, index)) == NULL) YYABORT;
	}
	;

call:
	call OPPAR elist CLPAR
	{
		alpha_log(LOG_DEBUG, "call -> call(elist) \n");
		//return call expr
		printparams($3,qt);
		add_quad(qt,call,$1,NULL,NULL);
		node_t *newnode = create_tmpnode(st);
		$$ =  nodexpr(var_e,newnode);
		add_quad(qt,getretval,$$,NULL,NULL);
	}
	| lvalue callsufix{
		alpha_log(LOG_DEBUG, "call -> lvalue callsufix \n");
		// Emit quads for table elem
		$1 = emit_iftblitem(qt, $1, st);

		// Print function parameters provided
		printparams($2.expr,qt);

		expr_t *callexp;
		switch($2.sfknd) {
			case norm: callexp = $1; break;
			case memb:
				// extract table member id from lvalue and call it
				printparams($1, qt);
				expr_t *index = constexpr_str($2.id);
				if((callexp = emit_iftblitem(qt, member_expr(qt, $1, index), st)) == NULL) YYABORT;
				break;
		}

		add_quad(qt,call,callexp,NULL,NULL);
		node_t *newnode = create_tmpnode(st);
		$$ =  nodexpr(var_e,newnode);
		add_quad(qt,getretval,$$,NULL,NULL);

		//printparams($1,qt);
		//TODO callsufix can be methodcal that is the same as lvalue(elist).id only that it is lvalue..id(elist) for some weird reason
	}
	| OPPAR funcdef CLPAR OPPAR elist CLPAR {
		alpha_log(LOG_DEBUG, "call -> (funcdef)(elist) \n");
		printparams($5,qt);
		add_quad(qt,call,$2,NULL,NULL);
		node_t *newnode = create_tmpnode(st);
		$$ =  nodexpr(var_e,newnode);
		add_quad(qt,getretval,$$,NULL,NULL);
	}
	;

callsufix:
	normcall {
		alpha_log(LOG_DEBUG, "callsufix->normcall \n");
		$$=$1;
	}
	| methodcal {
		alpha_log(LOG_DEBUG, "callsufix -> methodcal \n");
		$$=$1;
	}
	;

normcall:
	OPPAR elist CLPAR {
		alpha_log(LOG_DEBUG, "normcall -> (elist)\n");
		$$.expr = $2;
		$$.id = NULL;
		$$.sfknd = norm;
	}
	;

methodcal:
	DDOT ID OPPAR elist CLPAR {alpha_log(LOG_DEBUG, "methodcal -> ..id(elist) \n");
		$$.expr = $4;
		$$.id = $2;
		$$.sfknd = memb;
	}
	;

elist:					  {alpha_log(LOG_DEBUG, "elist->{0}\n"); $$ =NULL;}
	| expr elistoption {
		alpha_log(LOG_DEBUG, "elist->expr elistoption *\n");
		$1 = bool_totmp($1, qt, st);
		$1->next = $2;
		$$= $1;
	}
	;

elistoption:
	{alpha_log(LOG_DEBUG, "elistoption->{0}\n");
		$$=NULL;
	}
	| COMMA expr elistoption {
		alpha_log(LOG_DEBUG, "elistoption->,expr elistoption\n");
		$2 = bool_totmp($2, qt, st);
		$2->next = $3;
		$$ = $2;
	}
	;

objectdef:
	OPBRACKET elist CLBRACKET {
		alpha_log(LOG_DEBUG, "objectdef -> [elist] \n");
		node_t *tmp = create_tmpnode(st);
		$$ = nodexpr(newtable_e, tmp);

		add_quad(qt, tablecreate, $$, NULL, NULL);
		if(emit_tblsetelist(qt, $$, $2) == NULL) YYABORT;
	}

	| OPBRACKET indexed CLBRACKET {
		alpha_log(LOG_DEBUG, "objectdef -> [indexed]");
		node_t *tmp = create_tmpnode(st);
		$$ = nodexpr(newtable_e, tmp);

		add_quad(qt, tablecreate, $$, NULL, NULL);
		if(emit_tblsetindxel(qt, $$, $2.index, $2.value) == NULL) YYABORT;
	}


indexed:
	indexedelem indexoption {
		alpha_log(LOG_DEBUG, "indexed->indexedelem indexoption \n");
		$1.index->next = $2.index;
		$1.value->next = $2.value;
		$$ = $1;
	}
	;

indexoption:
	{
		alpha_log(LOG_DEBUG, "indexoption -> 0 \n");
		$$.index = NULL;
		$$.value = NULL;
	}

	| COMMA indexedelem indexoption {
		alpha_log(LOG_DEBUG, "indexoption -> ,indexedelem indexoption \n");
		$2.index->next = $3.index;
		$2.value->next = $2.value;
		$$ = $2;
	}
	;

indexedelem:
	OPBRACE expr COLON expr CLBRACE {
		alpha_log(LOG_DEBUG, "indexedelem -> {expr:expr} \n");
		$$.index = $2;
		$$.value = $4;
	}
	;

block:
	OPBRACE { advance_scope(st); }
	statements CLBRACE {
		alpha_log(LOG_DEBUG, "block -> [stmt]");
		deact_scope(st,st->curr_scope);
		retract_scope(st);
	}
	;

funcdef:
	FUNC funcdefoptions OPPAR {
		advance_scope(st);
		push_nestscope(st);
		constval_t t;
		t.str = strdup($2->name);
		$<expression>$ = create_expr(libfunc_e, $2, NULL, t, NULL);
		add_quad(qt,funcstart,$<expression>$,NULL,NULL);
		push_ffs(st);
		st->formalflag = 1;
	}

	idlist CLPAR {
		st->formalflag = 0;
		st->spaceflag++;
		printf("here too??");fflush(stdout);
		retract_scope(st);
		push_fls(st);
	}

	block {
		pop_nestscope(st);
		alpha_log(LOG_DEBUG, "funcdef->function[id](idlist)block \n");
		add_quad(qt,funcend,$<expression>4,NULL,NULL);
		$$ = $<expression>4;
		st->spaceflag--;
	}

funcdefoptions:
	{
		alpha_log(LOG_DEBUG, "funcdefoptions -> 0 \n");
		$$ = add_node(st,strdup(dfn),user_func,NULL,alpha_yylineno,st->curr_scope,1);
		dfn[2]++;
	}

	| ID {
		alpha_log(LOG_DEBUG, "funcdefoptions -> id \n");
		$$= find_node(st,$1,st->curr_scope);
		if($$!=NULL){
			alpha_log(LOG_ERROR, "id %s is in use\n", $1);
			YYABORT;
		}
		else if(is_lib_func(st,$1)){
			alpha_log(LOG_ERROR, "id %s is in use by library function\n", $1);
			YYABORT;
		}
		else{
			$$=add_node(st,$1,user_func,NULL,alpha_yylineno,st->curr_scope,1);
		}
	}
	;

const:
	 INT {
		alpha_log(LOG_DEBUG, "const->INT \n");
		$$ = constexpr_num($1);
	}

	| REAL {
		alpha_log(LOG_DEBUG, "const -> REAL");
		$$ = constexpr_num($1);
	}

	| STR {
		alpha_log(LOG_DEBUG, "const->string \n");
		$$ = constexpr_str($1);
	}

	| NIL {
		alpha_log(LOG_DEBUG, "const -> nil \n");
		$$ = empty_expr(nil_e);
	}

	| T {
		alpha_log(LOG_DEBUG, "const -> T \n");
		$$ = constexpr_bool(true);
	}

	| F {
		alpha_log(LOG_DEBUG, "const->F \n");
		$$ = constexpr_bool(false);
	}
	;

idlist:
	{
		alpha_log(LOG_DEBUG, "idlist->0 \n");
		/*to add shit here*/
	}

	| ID idlistoption {
		alpha_log(LOG_DEBUG, "idlist-> ID idlist \n");
		node_t *node = find_node(st,$1,st->curr_scope);
		if(node != NULL){
			alpha_log(LOG_ERROR, "id %s is in use\n", $1);
			YYABORT;
		}
		else if(is_lib_func(st,$1)){
			alpha_log(LOG_ERROR, "id %s is in use by library function\n", $1);
			YYABORT;
		}
		else{
			add_node(st,$1,formal_arg,NULL,alpha_yylineno,st->curr_scope,1);
		}
	}
	;

idlistoption:
	{alpha_log(LOG_DEBUG, "idlistoption->0\n"); }

	| COMMA ID idlistoption {
		alpha_log(LOG_DEBUG, "idlist -> , id idlist");
		node_t *node = find_node(st,$2,st->curr_scope);
		if(node != NULL){
			alpha_log(LOG_ERROR, "id %s is in use\n", $2);
			YYABORT;
		}
		else if(is_lib_func(st,$2)){
			alpha_log(LOG_ERROR, "id %s is in use by library function\n", $2);
			YYABORT;
		}
		else{
			add_node(st,$2,formal_arg,NULL,alpha_yylineno,st->curr_scope,1);
		}
	}
	;

ifstmt:
	ifstmtdef stmt {
		alpha_log(LOG_DEBUG, "ifstmt -> if (expr) stmt (no else) \n");
		backpatch_tflst(qt,$1,flist);
	}

	| ifstmtdef stmt ELSE
		{
			expr_t *jumpexpr = empty_expr(constbool_e);
			ins_bp_list(jumpexpr,add_quad(qt,jump,NULL,NULL,NULL),tlist);
			backpatch_tflst(qt,$1,flist);
			$1->next = jumpexpr;
			} stmt {
		alpha_log(LOG_DEBUG, "ifstmt -> if(expr) stmt else stmt \n");
		backpatch_tflst(qt,$1->next,tlist);
	}
	;

ifstmtdef:
	IF OPPAR expr CLPAR{
		$3 = reduce_tobool($3,qt);
		backpatch_tflst(qt,$3,tlist);
		$$ = $3;
	}
	;

whilestmt:
	WHILE OPPAR{
			//exactly when the loop starts we show to the next quad that is going to be generated
			//if no quads are generated we don't care because then there will also be no continue
			enter_loop(qt);
			$<intVal>$ = qt->qt_index;
		} expr
		{
			$4 = reduce_tobool($4,qt);
			backpatch_tflst(qt,$4,tlist);
			} CLPAR stmt {
		add_quad(qt,jump,NULL,NULL,NULL)->label = $<intVal>3;
		alpha_log(LOG_DEBUG, "whilestmt -> while(expr) stmt \n");
		leave_loop(qt);
		backpatch_tflst(qt,$4,flist);
	}
	;

forstmt:
	FOR OPPAR{
		//exactly when the loop starts we show to the next quad that is going to be generated
		//if no quads are generated we don't care because then there will also be no continue
	 	} elist SEMCOL{
	 		$<intVal>$= qt->qt_index;
	 		} expr SEMCOL {
	 			$7 = reduce_tobool($7,qt);
	 			$<intVal>$= qt->qt_index;
	 			enter_loop(qt);
	 			}elist CLPAR {
		add_quad(qt,jump,NULL,NULL,NULL)->label = $<intVal>6;
	 	backpatch_tflst(qt,$7,tlist);
	 	}stmt {
		alpha_log(LOG_DEBUG, "forstmt -> for(elist;expr;elist) stmt \n");
		leave_loop(qt);
		add_quad(qt,jump,NULL,NULL,NULL)->label = $<intVal>9;
		backpatch_tflst(qt,$7,flist);
	}
	;

returnstmt:
	RT SEMCOL {
		alpha_log(LOG_DEBUG, "returnstmt -> 0 \n");
		if(Return(qt,NULL,st) == -1) YYABORT;
		//check if we are inside a function (we already have that in the form of peek_nestscope
		//then emit a quad with return opcode
	}

	| RT expr SEMCOL {
		alpha_log(LOG_DEBUG, "returnstmt->return expr \n");
		$2 = bool_totmp($2, qt, st);
		if(Return(qt,$2,st) == -1) YYABORT;
		//same as about and emit a quad with return opcode and expr as return arg1
		//REMINDER returns are handled in the virtual machine...
	}
	;

%%

int yyerror(symtable_t *st, quadtbl_t *qt, char *str) {
	alpha_log(LOG_ERROR, "%s\n", str);
	return 1;
}

/*main here and other functions*/

