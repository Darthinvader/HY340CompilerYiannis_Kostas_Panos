CC=gcc
LEX=flex
CCFLAGS=
LEXFLAGS=

%.o: %.c
	$(CC) -c -o $@ $< $(CCFLAGS)

lex.%.c: %.l
	$(LEX) $(LEXFLAGS) -o $@ $<

default: al

alpha_yy: lex.alpha_yy.c

al: lex.alpha_yy.o
	$(CC) -o $@ $^ $(CCFLAGS)

debug: CCFLAGS+=-g
debug: LEXFLAGS+=-L
debug: al

.PHONY: clean
clean:
	@rm -rf  *.o al lex.alpha_yy.c
