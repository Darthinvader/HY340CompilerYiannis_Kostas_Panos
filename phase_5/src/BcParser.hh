#ifndef BCPARSER_H
#define BCPARSER_H

#include <iostream>

#include "BcParser.hh"
#include "Vm.hh"

namespace alpha {
	class BcParser {
		public:
			static void parseInstructions(std::ifstream& file, std::vector<Instruction *>& instrArray, std::vector<FunctionData *>& funcArray);
			static ProgramData* parseFile(std::ifstream& file);
		private:
			BcParser();
	};
}

#endif
