#include "MemCell.hh"

#define ALPHA_DEBUG 0

using namespace alpha;
MemCell::MemCell(std::string strVal){
	this->value = strVal;
	this->type =  MemCellType::str_m;
	ArIndex =0;
}

MemCell::MemCell(double numVal){
	this->value = numVal;
	this->type =  MemCellType::num_m;
	ArIndex =0;
}
MemCell::MemCell(long numVal){
	this->value = numVal;
	this->type =  MemCellType::num_m;
	ArIndex =0;
}
MemCell::MemCell(MemCellType type,long numVal){
	this->value = numVal;
	this->type = type;
	ArIndex =0;
}
MemCell::MemCell(bool boolVal){
	this->value = boolVal;
	this->type =  MemCellType::bool_m;
	ArIndex =0;
}
MemCell::MemCell(MemCellType type){
	this->type = type;
	if(type == MemCellType::table_m){
		value = (long)0;
	}
	ArIndex =0;
}
void MemCell::setMemCell(std::string strVal){
	this->value = strVal;
	this->type =  MemCellType::str_m;
	ArIndex =0;
}
void MemCell::setMemCell(double numVal){
	this->value = numVal;
	this->type =  MemCellType::num_m;
	ArIndex =0;
}
void MemCell::setMemCell(bool boolVal){
	this->value = boolVal;
	this->type =  MemCellType::bool_m;
	ArIndex =0;
}
void MemCell::setMemCell(long numVal){
	this->type = MemCellType::num_m;
	this->value = numVal;
	ArIndex =0;
}
void MemCell::setMemCell(MemCellType type){
	this->type = type;
	if(type == MemCellType::table_m){
		this->value = (long)1;
	}
	ArIndex =0;
}
void MemCell::setMemCell(MemCellType type,long numVal){
	this->type = type;
	this->value = numVal;
	ArIndex =0;
}

void MemCell::setMemCell(boost::variant<double, bool, std::string,long> value){
	this->value = value;
	if(value.which() == 0 || value.which() == 3){
		this->type = MemCellType::num_m;
	}
	else if(value.which() == 1){
		this->type = MemCellType::bool_m;
	}
	else{
		this->type =MemCellType::str_m;
	}
	ArIndex =0;
}

void MemCell::setMemCell(ArrayObject array){
	type = MemCellType::table_m;
	Arrays = array;
	ArIndex =0;
}
MemCellType MemCell::getType(){
	return this->type;
	ArIndex =0;
}

std::string MemCell::getTypeStr() {
  if (this->type == MemCellType::bool_m)return "bool";
  else if (this->type == MemCellType::func_m)return "function";
  else if (this->type == MemCellType::libfunc_m)return "library function";
  else if (this->type == MemCellType::nil_m)return "nil";
  else if (this->type == MemCellType::num_m)return "number";
  else if (this->type == MemCellType::str_m)return "string";
  else if (this->type == MemCellType::table_m)return "table";
  else return "undefined";
}

boost::variant<double, bool, std::string,long> MemCell::getValue(){
	return value;
}


double MemCell::getDoubleCell(){
	return boost::get<double>(this->value);
}
std::string MemCell::getStringCell(){
	return boost::get<std::string>(this->value);
}

long MemCell::getLongCell(){
	return boost::get<long>(this->value);
}

MemCell::ArrayObject MemCell::getArrays(){
	return Arrays;
}


void MemCell::add(std::string String,MemCell *cell){
	type = MemCellType::table_m;
	if(cell->getType()!=MemCellType::table_m){
		if(ALPHA_DEBUG == 1)std::cout <<"opcode of cell added is" << (int)cell->getType();fflush(stdout);
	}
	//MemCell *newcell = new MemCell(cell->type = )
	if(ArIndex==0){
		std::map<std::string,MemCell*> reg;
		Arrays = reg;
	}
	Arrays[String] = cell;
	ArIndex++;
	if(ALPHA_DEBUG == 1)std::cout << ArIndex;
}



MemCell* MemCell::clone(){
	MemCell *returner = new MemCell(type);
	if(type == MemCellType::undef_m){
		if(ALPHA_DEBUG == 1)std::cout << "value of cloner is undef \n";fflush(stdout);
	}
	if(type == MemCellType::table_m){
		int i =0;
		for(std::pair<std::string,MemCell *> myPair: this->Arrays){
			i++;
			if(ALPHA_DEBUG == 1)std::cout <<"first pair::m " << myPair.first <<"\n";fflush(stdout);
			//myPair.second->printer();
			returner->add(myPair.first,myPair.second->clone());
		}
		if(ALPHA_DEBUG == 1)std::cout << "\n \n number is :: " << i << "\n \n";
	}else{
		returner->setMemCell(value);
	}
	return returner;
}


void MemCell::printer(){
	if(type == MemCellType::table_m){
		std::cout<< "[";
		for(std::pair<std::string,MemCell *> myPair: Arrays){
			std::cout<< myPair.first;
			std::cout << ":";
			myPair.second->printer();
			std ::cout << ",";
		}
		std::cout << "]";
	}
	else if(type == MemCellType::num_m){
		std::cout<< boost::get<double>(value);
	}
	else if(type == MemCellType::str_m){
		std::cout << boost::get<std::string>(value) ;
	}
	else if(type == MemCellType::bool_m){
		if(boost::get<bool>(value) == true){
			std::cout << "TRUE";
		}
		else{
			std::cout << "FALSE";
		}
	}
	else if(type == MemCellType::func_m){
		std::cout << "Function:" + boost::get<long>(value);
	}
	else if(type == MemCellType::libfunc_m){
		std::cout << "LibFunction" + libFuncName(boost::get<long>(value));
	}
	else if(type == MemCellType::nil_m){
		std::cout << "nil";
	}
	else if(type == MemCellType::undef_m){
		std::cout << "variable undefined ERROR";
	}
}

std::string MemCell::libFuncName(int index){
	switch(index){
		case 0:
		return "print";
		case 1:
		return "input";
		case 2:
		return "objectmemberkeys";
		case 3:
		return "objecttotalmembers";
		case 4:
		return "objectcopy";
		case 5:
		return "totalarguments";
		case 6:
		return "argument";
		case 7:
		return "typeof";
		case 8:
		return "strtonum";
		case 9:
		return "sqrt";
		case 10:
		return "cos";
		case 11:
		return "sin";
	}
	return"ERROR Library function name doesnt exist";
}
MemCell *MemCell::CloneKeys(){
	if(type != MemCellType::table_m){
		MemCell *returner = new MemCell(MemCellType::table_m);
		int i=0;
		for(const auto myPair : Arrays){
			returner->add(std::to_string(i),new MemCell(myPair.first));
			i++;
		}
		return returner;
	}
	else{
		std::cout << "Cannot clone key not a table";
	}
	return NULL;
}
int MemCell::keyAmmount(){
	if(type != MemCellType::table_m){
		int i=0;
		for(const auto myPair : Arrays){
			i++;
		}
		return i;
	}
	else{
		std::cout << "Cannot count elements not a table";
	}
	return 0;
}

MemCell* MemCell::shallowClone(){
	if(type != MemCellType::table_m){
		std::cout << "Copy cannot succeed this is not an object or array";
		return NULL;
	}
	MemCell *returner = new MemCell(MemCellType::table_m);
	for(const auto myPair : Arrays){
		returner->add(myPair.first,myPair.second);
	}
	return returner;
}

void MemCell::addAll(MemCell *cell){
	for(const auto myPair : cell->getArrays()){
		add(myPair.first,myPair.second);
	}
}
