#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <regex>
#include <boost/variant.hpp>
#include <boost/algorithm/string.hpp>
#include "Vm.hh"
#define PI 3.14159265
using namespace alpha;

#ifndef ALPHA_DEBUG
#define ALPHA_DEBUG 0
#endif
//TODO
//change call to assign to return value immediately after calling library function and check for nulls and make them MemCell *(MemCellType::nil_m);
//TODO add to insertMemCellsToStack to change the intructionindex
long stackFunction::getIndex(){
	return index;
}
long stackFunction::getFormalCount(){
	return formalCount;
}
long stackFunction::getLocalCount(){
	return localCount;
}
void stackFunction::setIndex(long index){
	this->index = index;
}
void stackFunction::setFormalCount(long formals){
	this->formalCount = formals;
}
void stackFunction::setLocalCount(long locals){
	this->localCount = locals;
}
stackFunction::stackFunction(long index){
	this->index = index;
}



void FunctionData::setInstrIndex(long index){
	this->instrIndex = index;
}


long FunctionData::getIndex(){
	return index;
}
long FunctionData::getInstrIndex(){
	return instrIndex;
}
long FunctionData::getFormalCount(){
	return formalCount;
}
long FunctionData::getLocalCount(){
	return localCount;
}
FunctionData::FunctionData(long index, long instrIndex, long formalCount, long localCount){
	this->index = index;
	this->instrIndex = instrIndex;
	this->formalCount= formalCount;
	this->localCount = localCount;
}

FunctionData::FunctionData() {
	this->index = 0;
	this->instrIndex = 0;
	this->formalCount= 0;
	this->localCount = 0;
}

void FunctionData::setFormalCount(long count){
	this->formalCount = count;
}
void FunctionData::setLocalCount(long count){
	this->localCount = count;
}


ProgramData::ProgramData(std::vector<Instruction *> instructions,std::vector<double> doubleArray,std::vector<std::string> stringArray,int globals, std::vector<FunctionData *> functionArray){
	this->instructions = instructions;
	this->doubleArray = doubleArray;
	this->stringArray = stringArray;
	for(int i=0;i<globals;i++){
		MemCell *k= new MemCell(MemCellType::undef_m);
		globalsArray.push_back(k);
	}
	this->functionArray = functionArray;
}


Instruction *ProgramData::getInstruction(int index){
	return instructions[index];
}
MemCell *ProgramData::getStack(int index){
	return stack[index];
}
double ProgramData::getDouble(int index){
	return doubleArray[index];
}
std::string ProgramData::getString(int index){
	return stringArray[index];
}
MemCell *ProgramData::getGlobal(int index){
	return globalsArray[index];
}
FunctionData *ProgramData::getFunction(int index){
	return functionArray[index];
}
MemCell *ProgramData::getFormal(int formalIndex){
	return stack[sFunction[sFunction.size()-1]->getIndex() + formalIndex];
}
MemCell *ProgramData::getLocal(int localIndex){
	if(ALPHA_DEBUG == 1)std::cout << "\n \n" << "this is the local id" << sFunction[sFunction.size()-1]->getIndex() + sFunction[sFunction.size()-1]->getFormalCount() + localIndex << "\n";
	return stack[sFunction[sFunction.size()-1]->getIndex() + sFunction[sFunction.size()-1]->getFormalCount() + localIndex];
}

stackFunction  *ProgramData::getStackFunction(int index){
	return sFunction[index];
}

std::vector<MemCell *> ProgramData::ReverseFormals(std::vector<MemCell *> Flocals){
	std::vector<MemCell *> newFlocals;
	for(int i=Flocals.size()-1;i>=0;i--){
		newFlocals.push_back(Flocals[i]);
	}
	return newFlocals;

}
void ProgramData::insertMemCellsToStack(std::vector<MemCell *> Flocals,int FuncIndex){
	int StackIndex;
	Flocals = ReverseFormals(Flocals);
	if(sFunction.size() == 0){
		StackIndex = 0;
	}
	else{
		StackIndex = sFunction[sFunction.size()-1]->getIndex() +sFunction[sFunction.size()-1]->getLocalCount() + +sFunction[sFunction.size()-1]->getFormalCount() ;
	}
	stackFunction* sfunction = new stackFunction(StackIndex);


	sfunction->setFormalCount(functionArray[FuncIndex+12]->getFormalCount());
	sfunction->setLocalCount(functionArray[FuncIndex +12]->getLocalCount());
	sFunction.push_back(sfunction);
	if(Flocals.size()!= 0){
		stack.insert(stack.end(),Flocals.begin(),Flocals.end());
	}
	for(int i=0;i<sfunction->getLocalCount();i++){
		if(ALPHA_DEBUG == 1)std::cout<<"we have locals";
		stack.insert(stack.end(),new MemCell(MemCellType::undef_m));
	}
	if(ALPHA_DEBUG == 1)std::cout << "stack size is " << stack.size() << "\n";
	//TODO add
}
void ProgramData::cleanLastStack(){
	stackFunction *ff = sFunction[sFunction.size()-1];
	long ffsize = ff->getIndex() + ff->getLocalCount() + ff->getFormalCount();
	if(ffsize == 0){return;}
	stack.erase(stack.begin()+ ff->getIndex()-1,stack.begin()+ ff->getIndex() + ff->getLocalCount() + ff->getFormalCount() -1);
	sFunction.pop_back();
}


long ProgramData::InstrSize(){
	return instructions.size();
}

int ProgramData::getNumOfArgs(){
	if(sFunction.size()-2<0){
		return -1;
	}
	return sFunction[sFunction.size()-2]->getLocalCount();
}

MemCell *ProgramData::getLastFormal(int formalIndex){
	return stack[sFunction[sFunction.size()-2]->getIndex() + formalIndex];
}
void ProgramData::printInstr(){
	for (int i=0;i<instructions.size();i++){
		if(ALPHA_DEBUG == 1)std::cout<< i << " ";
		if(ALPHA_DEBUG == 1)instructions[i]->printer();
		if(ALPHA_DEBUG == 1)std::cout<< "\n";
	}
}


void Vm::MoveInstrIndex(){
	while((currInstrIndex[currInstrIndex.size()-1] < Everything->InstrSize()-1) && !executionFinished){
		currInstrIndex[currInstrIndex.size()-1]++;
		fetchNextInstuction();
		AnalyzeInstruction();
	}
}
void Vm::fetchNextInstuction(){
	//currInstr = Everything.getInstructions(currInstrIndex[currInstrIndex.size()-1]);
	currInstr = Everything->getInstruction(currInstrIndex[currInstrIndex.size()-1]);
}
void Vm::AnalyzeInstruction(){
	if(ALPHA_DEBUG == 1)std::cout << "curr line is:" << currInstrIndex[currInstrIndex.size() -1];
	line++;
	OpCode curropcode = currInstr->getOpCode();
	if(ALPHA_DEBUG == 1)std::cout << "Opcode read" << curropcode << "boolshit is :" << boolshit << "\n";fflush(stdout);
	if(boolshit ==0){
		if(curropcode != OpCode::tablegetelem && curropcode != OpCode::tablesetelem){
			Vm::NullifyElements();
		}
		switch(curropcode){
			case OpCode::assign :
				//printf("went to AnalyzeInstruction to assign");
				Vm::ExecuteAssign();
				break;
			case OpCode::sub :
				ExecuteSub();
				break;
			case OpCode::add :
				ExecuteAdd();
				break;
			case OpCode::mul :
				ExecuteMul();
				break;
			case OpCode::divd :
				ExecuteDiv();
				break;
			case OpCode::mod :
				ExecuteMod();
				break;
			case OpCode::jeq :
				ExecuteJeq();
				break;
			case OpCode::jne :
				ExecuteJne();
				break;
			case OpCode::jle :
				ExecuteJle();
				break;
			case OpCode::jge :
				ExecuteJge();
				break;
			case OpCode::jlt :
				ExecuteJlt();
				break;
			case OpCode::jgt :
				ExecuteJgt();
				break;
			case OpCode::ju :
				ExecuteJump();
				break;
			case OpCode::call :
				ExecuteCall();
				break;
			case OpCode::pusharg :
				ExecutePusharg();
				break;
			case OpCode::funcenter :
				ExecuteFuncenter();
				break;
			case OpCode::funcexit :
				ExecuteFuncexit();
				break;
			case OpCode::newtable :
				ExecuteNewtable();
				break;
			case OpCode::tablegetelem :
				ExecuteTablegetelem();
				break;
			case OpCode::tablesetelem :
				ExecuteTablesetelem();
				break;
			case OpCode::nop :
				std::cout << "ERROR nop found";
				executionFinished = true;
				break;
		}
	}
	else{
		if(curropcode == OpCode::funcenter){
			ExecuteFuncenter();
		}
		if(curropcode == OpCode::funcexit){
			ExecuteFuncexit();
		}
	}
}

void ProgramData::printalldoubles(){
	for(int i=0;i<doubleArray.size();i++){
		if(ALPHA_DEBUG == 1)std::cout << i << "number is:" << doubleArray[i] << "\n";
	}
}


Vm::Vm(ProgramData *Everything){
	if(ALPHA_DEBUG == 1)printf("1st \n");fflush(stdout);
	this->Everything = Everything;
	if(ALPHA_DEBUG == 1)printf("2nd");fflush(stdout);
	currInstrIndex.push_back(-1);
	if(ALPHA_DEBUG == 1)printf("3rd");fflush(stdout);
	Everything->printalldoubles();
	Everything->printInstr();
	boolshit = 0;
	line =0;
	executionFinished = false;
}

void Vm::ExecuteAssign(){
	//printf("executing assign \n");
	Vmarg *arg1= currInstr->getArg1();
	Vmarg *arg2 = currInstr->getArg2();
	MemCell *copy = GetCellCopy(arg2);
	if(arg1->getType() == VmArgType::retval_a){
		returnedValue.push_back(copy);
		currInstrIndex.pop_back();
		Everything->cleanLastStack();
	}
	else if(arg2->getType() == VmArgType::func_a && arg2->getValue()<=11){
		MemCell *original = GetCell(arg1);
		original->setMemCell(MemCellType::libfunc_m);
		original->setMemCell(arg2->getValue());
	}
	else{
		if(ALPHA_DEBUG == 1)std::cout <<"value is:" << arg2->getValue() <<"\n";
		MemCell *original = GetCell(arg1);
		if(ALPHA_DEBUG == 1)std::cout<< copy->getValue();
		original->setMemCell(copy->getType());
		if(copy->getType() == MemCellType::table_m){
			original->setMemCell(copy->getType());
			original->setMemCell(copy->getArrays());
		}
		else{
			original->setMemCell(copy->getType());
			original->setMemCell(copy->getValue());
		}
	}
	
}
void Vm::ExecuteAdd(){
	Vmarg *arg1= currInstr->getArg1();
	Vmarg *arg2 = currInstr->getArg2();
	Vmarg *arg3 = currInstr->getArg3();
	MemCell *copy1 = GetCellCopy(arg2);
	MemCell *copy2 = GetCellCopy(arg3);
	MemCell *original = GetCell(arg1);
	if(copy1->getType()!=MemCellType::num_m ||copy2->getType()!=MemCellType::num_m){
		std::cout << "ERROR cannot add non arithmetic \n";
		executionFinished = true;
		return;
	}
	double tmp = boost::get<double>(copy1->getValue()) + boost::get<double>(copy2->getValue());
	original->setMemCell(tmp);
}
void Vm::ExecuteSub(){
	Vmarg *arg1= currInstr->getArg1();
	Vmarg *arg2 = currInstr->getArg2();
	Vmarg *arg3 = currInstr->getArg3();
	MemCell *copy1 = GetCellCopy(arg2);
	MemCell *copy2 = GetCellCopy(arg3);
	MemCell *original = GetCell(arg1);
	if(copy1->getType()!=MemCellType::num_m ||copy2->getType()!=MemCellType::num_m){
		std::cout << "ERROR cannot add non arithmetic \n";
		return;
	}
	double tmp = boost::get<double>(copy1->getValue()) - boost::get<double>(copy2->getValue());
	original->setMemCell(tmp);
}

void Vm::ExecuteMul(){
	Vmarg *arg1= currInstr->getArg1();
	Vmarg *arg2 = currInstr->getArg2();
	Vmarg *arg3 = currInstr->getArg3();
	MemCell *copy1 = GetCellCopy(arg2);
	MemCell *copy2 = GetCellCopy(arg3);
	MemCell *original = GetCell(arg1);
	if(copy1->getType()!=MemCellType::num_m ||copy2->getType()!=MemCellType::num_m){
		std::cout << "ERROR cannot add non arithmetic \n";
		return;
	}
	double tmp = boost::get<double>(copy1->getValue()) * boost::get<double>(copy2->getValue());
	original->setMemCell(tmp);
}
void Vm::ExecuteDiv(){
	Vmarg *arg1= currInstr->getArg1();
	Vmarg *arg2 = currInstr->getArg2();
	Vmarg *arg3 = currInstr->getArg3();
	MemCell *copy1 = GetCellCopy(arg2);
	MemCell *copy2 = GetCellCopy(arg3);
	MemCell *original = GetCell(arg1);
	if(copy1->getType()!=MemCellType::num_m ||copy2->getType()!=MemCellType::num_m || boost::get<double>(copy2->getValue()) == 0){
		std::cout << "ERROR cannot add non arithmetic or division by 0 \n";
		executionFinished = true;
		return;
	}
	double tmp = boost::get<double>(copy1->getValue()) / boost::get<double>(copy2->getValue());
	original->setMemCell(tmp);
}
void Vm::ExecuteMod(){
	Vmarg *arg1= currInstr->getArg1();
	Vmarg *arg2 = currInstr->getArg2();
	Vmarg *arg3 = currInstr->getArg3();
	MemCell *copy1 = GetCellCopy(arg2);
	MemCell *copy2 = GetCellCopy(arg3);
	MemCell *original = GetCell(arg1);
	if(copy1->getType()!=MemCellType::num_m ||copy2->getType()!=MemCellType::num_m){
		std::cout << "ERROR cannot add non arithmetic \n";
		executionFinished = true;
		return;
	}
	int c1,c2;
	if(ALPHA_DEBUG == 1)std::cout << "modding here:: " << copy1->getValue() << "  " << copy2->getValue();fflush(stdout);
	c1 = std::round(boost::get<double>(copy1->getValue()));
	c2 = std::round(boost::get<double>(copy2->getValue()));
	if(boost::get<double>(copy1->getValue()) == boost::get<double>(copy2->getValue())){
		original->setMemCell(0.00);
	}
	else{
		double tmp = c1 % c2;
		original->setMemCell(tmp);
	}
}

bool Vm::Jeq(){
	Vmarg *arg1 = currInstr->getArg1();
  Vmarg *arg2 = currInstr->getArg2();
  MemCell *copy1 = GetCellCopy(arg1);
  MemCell *copy2 = GetCellCopy(arg2);
  bool JumpBool=false;//default value!
  MemCellType cpType1 = copy1->getType();
  MemCellType cpType2 = copy2->getType();
  if(cpType1== MemCellType::undef_m ||cpType2 == MemCellType::undef_m){
  	throwError("undef involded in equality",true);
  	executionFinished = true;
  }
  else if(cpType1 == MemCellType::nil_m || cpType2 == MemCellType::nil_m){
  	JumpBool = (cpType1 == MemCellType::nil_m && cpType2 == MemCellType::nil_m);
  }
  else if(cpType1 == MemCellType::bool_m || cpType2 == MemCellType::bool_m){
  	JumpBool = (toBool(copy1) == toBool(copy2));
  }
  else if(cpType1 != cpType2){
  	std::string errorMessage = "" + copy1->getTypeStr() + " == " + copy2->getTypeStr() + " is illegal! \n";
    throwError(errorMessage, true);
  	JumpBool = false;
  }
  else{
  	if(cpType1 == MemCellType::num_m){
  		double copy1tmp = boost::get<double>(copy1->getValue());
      double copy2tmp = boost::get<double>(copy2->getValue());
      if(ALPHA_DEBUG == 1)std::cout << "HERE IS EQUALITY COMPARING"<< copy1tmp << " and :"<< copy2tmp << "\n";
      JumpBool = (copy1tmp == copy2tmp);
  	}
  	else if(cpType1 == MemCellType::func_m||cpType1 == MemCellType::libfunc_m){
  		long copy1tmp = boost::get<long>(copy1->getValue());
      long copy2tmp = boost::get<long>(copy2->getValue());
      JumpBool = (copy1tmp == copy2tmp);
  	}
  	else if(cpType1 == MemCellType::str_m){
  		std::string copy1tmp = boost::get<std::string>(copy1->getValue());
      std::string copy2tmp = boost::get<std::string>(copy2->getValue());
      JumpBool = (copy1tmp == copy2tmp);
  	}
  	else if(cpType1== MemCellType::table_m){
  		JumpBool = (GetCell(arg1) == GetCell(arg2));
  	}
  }
  return JumpBool;
}
void Vm::ExecuteJeq() {
  Vmarg *label = currInstr->getArg3();
  bool JumpBool=this->Jeq();//default value!
  if (!executionFinished && JumpBool) {
    currInstrIndex[currInstrIndex.size() - 1] = label->getValue() -1;
  }
}

void Vm::ExecuteJne() {
	Vmarg *label = currInstr->getArg3();
  bool JumpBool=this->Jeq();//default value!
  if (!executionFinished && !JumpBool) {
    currInstrIndex[currInstrIndex.size() - 1] = label->getValue()-1;
  }
}

bool Vm::Jle(){
	Vmarg *arg1 = currInstr->getArg1();
  Vmarg *arg2 = currInstr->getArg2();
  MemCell *copy1 = GetCellCopy(arg1);
  MemCell *copy2 = GetCellCopy(arg2);
  bool JumpBool = false;
  if(copy1->getType() == MemCellType::num_m && copy1->getType() == MemCellType::num_m){
  	JumpBool = boost::get<double>(copy1->getValue()) <= boost::get<double>(copy2->getValue());
  }
  else{
  	throwError("ERROR cannot compare two non arithmetics", true);
  	executionFinished = true;
  }
  return JumpBool;
}

void Vm::ExecuteJle(){
	Vmarg *label = currInstr->getArg3();
	bool JumpBool = Jle();
	if (!executionFinished && JumpBool){
		currInstrIndex[currInstrIndex.size() - 1] = label->getValue()-1;
	}
}

void Vm::ExecuteJgt(){
	Vmarg *label = currInstr->getArg3();
	bool JumpBool = Jle();
	if (!executionFinished && !JumpBool){
		currInstrIndex[currInstrIndex.size() - 1] = label->getValue()-1;
	}
}

bool Vm::Jge(){
	Vmarg *arg1 = currInstr->getArg1();
  Vmarg *arg2 = currInstr->getArg2();
  MemCell *copy1 = GetCellCopy(arg1);
  MemCell *copy2 = GetCellCopy(arg2);
  bool JumpBool = false;
  if(copy1->getType() == MemCellType::num_m && copy1->getType() == MemCellType::num_m){
  	JumpBool = boost::get<double>(copy1->getValue()) >= boost::get<double>(copy2->getValue());
  }
  else{
  	throwError("ERROR cannot compare two non arithmetics", true);
  }
  return JumpBool;
}
void Vm::ExecuteJge(){
	Vmarg *label = currInstr->getArg3();
	bool JumpBool = Jge();
	if (!executionFinished && JumpBool){
		currInstrIndex[currInstrIndex.size() - 1] = label->getValue()-1;
	}
}
void Vm::ExecuteJlt(){
	Vmarg *label = currInstr->getArg3();
	bool JumpBool = Jge();
	if (!executionFinished && !JumpBool){
		currInstrIndex[currInstrIndex.size() - 1] = label->getValue()-1;
	}
}

void Vm::ExecuteJump(){
	Vmarg *label = currInstr->getArg1();
	currInstrIndex[currInstrIndex.size() - 1] = label->getValue()-1;
}
void Vm::ExecuteCall(){
	if((currInstr->getArg1()->getType() == VmArgType::func_a && currInstr->getArg1()->getValue() <=11) || GetCellCopy(currInstr->getArg1())->getType() == MemCellType::libfunc_m){
		//run the appropriate libfunction;
		Formals = Everything->ReverseFormals(Formals);
		int typer = currInstr->getArg1()->getValue();
		switch(typer){
			case 0:
				ExecutePrint();
				returnedValue.push_back(new MemCell(MemCellType::nil_m));
				break;
			case 1:
				returnedValue.push_back(ExecuteInput());
				break;
			case 2:
				returnedValue.push_back(objectMemberKeys());
				break;
			case 3:
				returnedValue.push_back(objectTotalMembers());
				break;
			case 4:
				returnedValue.push_back(objectCopy());//TODO add a small notification that returnvalue should return as itself when objectCopying;
				break;
			case 5:
				returnedValue.push_back(totalArguments());
				break;
			case 6:
				returnedValue.push_back(argument());
				break;
			case 7:
				returnedValue.push_back(typeOf());
				break;
			case 8:
				returnedValue.push_back(strtonum());
				break;
			case 9:
				returnedValue.push_back(ExecuteSqrt());
				break;
			case 10:
				returnedValue.push_back( ExecuteCos());
				break;
			case 11:
				returnedValue.push_back(ExecuteSin());
				break;
		}
	}
	else{
		long funcIndex;
		if(currInstr->getArg1()->getType() != VmArgType::func_a){
			MemCell *caller = GetCellCopy(currInstr->getArg1());
			funcIndex = boost::get<long>(caller->getValue()) -12;
		}
		else{
			funcIndex = currInstr->getArg1()->getValue() -12;
		}
		if(ALPHA_DEBUG == 1)std::cout << "Funcindex is::" << funcIndex <<"\n";
		if(ALPHA_DEBUG == 1)std::cout << "argtype:" << (int)currInstr->getArg1()->getType()<<"\n";
		Everything->insertMemCellsToStack(Formals,funcIndex);
		//std::cout << Everything->getFunction(funcIndex-12)->getInstrIndex() << "\n";
		currInstrIndex.push_back(Everything->getFunction(funcIndex)->getInstrIndex());
	}
	Formals.clear();
}
void Vm::ExecutePusharg(){
	MemCell *copy1 = GetCellCopy(currInstr->getArg1());
	Formals.push_back(copy1);

}
void Vm::ExecuteFuncenter(){
	boolshit++;
}
void Vm::ExecuteFuncexit(){
	if(boolshit>0 ){
		boolshit--;
	}
	else{
		returnedValue.push_back(new MemCell(MemCellType::nil_m));
		currInstrIndex.pop_back();
	}
}
void Vm::ExecuteNewtable(){
	MemCell *original = GetCell(currInstr->getArg1());
	original->setMemCell(MemCellType::table_m);
}
void Vm::ExecuteTablegetelem(){
	Vmarg *arg1= currInstr->getArg1();
	Vmarg *arg2 = currInstr->getArg2();
	Vmarg *arg3 = currInstr->getArg3();
	MemCell *original1 = GetCell(arg1);
	MemCell *original2 = GetCell(currInstr->getArg2());
	MemCell *original3 = GetCell(arg3);
	std::string stringers;
	if(original2 ->getType() == MemCellType::num_m){//in case its a number not a string (k[1] = 0 for example)
		double d = boost::get<double>(original2->getValue());
		std::string fromD = std::to_string(d);
		stringers = fromD;
		if(original1->getArrays().find(fromD)->second->getType()!=MemCellType::table_m){
			original3->setMemCell(original1->getArrays().find(fromD)->second->getValue());
		}
		else{
			original3->setMemCell(original1->getArrays().find(fromD)->second->getArrays());
		}
	}
	else{
		if(original2->getType()!= MemCellType::str_m){
			std::cout << "table get element accepts only strings and numbers";
			executionFinished = true;
			return;
		}
		else{
			std::string stringer = boost::get<std::string>(original2->getValue());
			stringers = stringer;
			if(original1->getArrays().find(stringer)->second->getType()!=MemCellType::table_m){
				original3->setMemCell(original1->getArrays().find(stringer)->second->getValue());
			}
			else{
				original3->setMemCell(original1->getArrays().find(stringer)->second->getArrays());
			}
		}
	}
	if(arraysPointer.size()==0){
		first = arg1;
	}
		AddToElements(stringers,original3);
}

void Vm::ExecuteTablesetelem(){
	MemCell *original = GetCell(currInstr->getArg1());
	MemCell *copy1 = GetCell(currInstr->getArg2());
	MemCell *copy2 = GetCell(currInstr->getArg3());
	original->add(boost::get<std::string>(copy1->getValue()),copy2);
	if(ALPHA_DEBUG == 1)original->printer();
	CombineElements();
	//boost::get<MemCell::ArrayObject>(original->getValue()).find(boost::get<std::string>(copy1->getValue()))->second->setMemCell(copy2->getValue());
}

MemCell *Vm::GetCellCopy(Vmarg *arg){
	VmArgType type = arg->getType();
	long index = arg->getValue();
	MemCell *newCell;
	switch(type){
		case VmArgType::global_a :
			if(ALPHA_DEBUG == 1)std::cout<<"read a global \n";
			newCell = Everything->getGlobal(index)->clone();
			break;
		case VmArgType::double_a :
			if(ALPHA_DEBUG == 1)std::cout<<"read a double \n";
			newCell = new MemCell(Everything->getDouble(index));
			break;
		case VmArgType::string_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a string \n";
			newCell = new MemCell(Everything->getString(index));
			break;
		case VmArgType::bool_a :
			if(ALPHA_DEBUG == 1)std::cout << "read a boolean \n";
			if(index == 0) newCell = new MemCell(false);
			else newCell = new MemCell(true);
			break;
		case VmArgType::func_a :
			if(ALPHA_DEBUG == 1)std::cout<<"read a function \n";
			newCell = new MemCell(MemCellType::func_m,index);
			break;
		case VmArgType::retval_a :
			if(ALPHA_DEBUG == 1)std::cout<<"read a retval \n";
			newCell = GetCell(arg);
			break;
		case VmArgType::nil_a :
			if(ALPHA_DEBUG == 1)std::cout<<"read a nil \n";
			newCell = new MemCell(MemCellType::nil_m);
			break;
		case VmArgType::label_a :
			std::cout << "label ERROR";
			executionFinished = true;
			newCell = new MemCell(MemCellType::nil_m);
			break;
		case VmArgType::formal_a :
			if(ALPHA_DEBUG == 1)std::cout<<"read a formal \n";
			newCell = Everything->getFormal(index)->clone();
			break;
		case VmArgType::local_a :
			if(ALPHA_DEBUG == 1)std::cout<<"read a local \n";
			newCell = Everything->getLocal(index)->clone();
			break;
		case VmArgType::int_a :
			std::cout << "int error";
			executionFinished = true;
			newCell = new MemCell(MemCellType::nil_m);
			break;
	}
	fflush(stdout);
	return newCell;
}


MemCell *Vm::GetCell(Vmarg *arg){
	VmArgType type = arg->getType();
	long index = arg->getValue();
	MemCell *newCell;
	switch(type){
		case VmArgType::global_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a global \n";
			newCell = Everything->getGlobal(index);
			break;
		case VmArgType::double_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a double \n";
			newCell = new MemCell(Everything->getDouble(index));
			break;
		case VmArgType::string_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a string \n";
			newCell = new MemCell(Everything->getString(index));
			break;
		case VmArgType::bool_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a boolean \n";
			if(index == 0) newCell = new MemCell(false);
			else newCell = new MemCell(true);
			break;
		case VmArgType::func_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a function \n";
			newCell = new MemCell(MemCellType::func_m,index);
			break;
		case VmArgType::retval_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a retval \n";
			newCell = returnedValue[returnedValue.size()-1];
			returnedValue.pop_back();
			break;
		case VmArgType::nil_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a nill \n";
			newCell = new MemCell(MemCellType::nil_m);
			break;
		case VmArgType::label_a :
			std::cout << "label ERROR";
			executionFinished=true;
			newCell = new MemCell(MemCellType::nil_m);
			break;
		case VmArgType::formal_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a formal \n";
			newCell = Everything->getFormal(index);
			break;
		case VmArgType::local_a :
			if(ALPHA_DEBUG == 1)std::cout <<"read a local \n";
			newCell = Everything->getLocal(index);
			break;
		case VmArgType::int_a :
			std::cout << "int error";
			executionFinished = true;
			newCell = new MemCell(MemCellType::nil_m);
			break;
	}
	return newCell;
}



void Vm::throwError(std::string message, bool execFlag) {
  std::cout << message << '\n';
  executionFinished = execFlag;
}

bool Vm::toBool(MemCell *var) {
  if(var->getType() == MemCellType::bool_m){
    return boost::get<bool>(var->getValue());
  }else if (var->getType() == MemCellType::num_m) {
    return (boost::get<double>(var->getValue()) != 0);
  }else if (var->getType() == MemCellType::func_m || var->getType() == MemCellType::libfunc_m || var->getType() == MemCellType::table_m) {
    return true;
  }else if (var->getType() == MemCellType::nil_m) {
    return false;
  }else if (var->getType() == MemCellType::str_m) {
    return (boost::get<std::string>(var->getValue()) != "");
  }
  return true;
}

ProgramData *Vm::getEverything(){
	return Everything;
}


void Vm::ExecutePrint(){
	for(int i=0;i<(int)Formals.size();i++){
		Formals[i]->printer();
	}
}

static inline bool isNumber(const std::string& str) {
	std::regex num_regex("[+-]?[0-9]+[.]?[0-9]*");
	return std::regex_match(str,num_regex);
}

MemCell* Vm::ExecuteInput() {
	std::string input;
	std::getline(std::cin, input, std::cin.widen('\n'));
	MemCell *cell;

	if(input == "nil") {
		cell = new MemCell(MemCellType::nil_m);
	} else if(input == "true") {
		cell = new MemCell(true);
	} else if(input == "false") {
		cell = new MemCell(false);
	} else if(isNumber(input)) {
		cell = new MemCell(stod(input));
	} else {
		if(input.empty()) cell = new MemCell(std::string(""));
		else if(input.size()>1 && input.front() == '\"' && input.back() == '\"') {
			std::string tmpstr = input.substr(1, input.length()-2);
			boost::replace_all(tmpstr, "\\\"", "\"");
			cell = new MemCell(tmpstr);
		}
		else cell = new MemCell(input);
	}

	return cell;
}

MemCell* Vm::objectMemberKeys(){
	MemCell *returner = NULL;
	if(Formals.size() == 0){
		std::cout << "Error not enought arguments for objectMemberKeys \n";
		executionFinished = true;
	}
	else{
		returner = Formals[0]->CloneKeys();
	}
	return returner;
}

MemCell *Vm::objectTotalMembers(){
	MemCell *returner = NULL;
	if(Formals.size() == 0){
		std::cout << "Error not enought arguments for objectTotalMembers \n";
		executionFinished = true;
	}
	else if(Formals[0]->getType()== MemCellType::table_m){
		returner = new MemCell((double)Formals[0]->keyAmmount());
	}
	return returner;
}

MemCell *Vm::objectCopy(){
	MemCell *returner = NULL;
	if(Formals.size() == 0){
		std::cout << "Error not enought arguments for objectCopy \n";
		executionFinished = true;
	}
	else if(Formals[0]->getType()== MemCellType::table_m){
		returner = Formals[0]->shallowClone();
	}
	return returner;
}

MemCell *Vm::totalArguments(){
	return new MemCell((double)Everything->getNumOfArgs());
}

MemCell *Vm::argument(){

	MemCell *returner = NULL;
	int k = (int)boost::get<double>(Formals[0]->getValue());
	int l = Everything->getNumOfArgs();
	if(Formals.size() == 0){
		std::cout << "Error not enought arguments for argument \n";
		executionFinished = true;
	}
	else if(Formals[0]->getType() == MemCellType::num_m){
		if(k >=0 && k<l){
			returner = Everything->getLastFormal(boost::get<double>(Formals[0]->getValue()));
		}
	}
	return returner;
}

MemCell *Vm::typeOf(){
	MemCell *returner = NULL;
	if(Formals.size() == 0){
		std::cout << "Error not enought arguments for typeOf \n";
		executionFinished = true;
	}
	else{
		switch(Formals[0]->getType()){
			case MemCellType::num_m:
			return new MemCell(std::string("Number"));
			break;
			case MemCellType::libfunc_m:
			return new MemCell(std::string("Library Function"));
			break;
			case MemCellType::bool_m:
			return new MemCell(std::string("boolean"));
			break;
			case MemCellType::func_m:
			return new MemCell(std::string("function"));
			break;
			case MemCellType::str_m:
			return new MemCell(std::string("string"));
			break;
			case MemCellType::table_m:
			return new MemCell(std::string("object"));
			break;
			case MemCellType::nil_m:
			return new MemCell(std::string("nil"));
			break;
			case MemCellType::undef_m:
			return new MemCell(std::string("undefined"));
			break;
		}
	}
	return returner;
}
MemCell *Vm::strtonum(){
	MemCell *returner = NULL;
	if(Formals.size() == 0){
		std::cout << "Error not enought arguments for strtonum \n";
		executionFinished = true;
	}
	else{
		if(Formals[0]->getType()!=MemCellType::str_m){
			std::cout << "Error not strtonum value is not string \n";
			executionFinished = true;
		}
		else{
			returner = new MemCell(std::stod(boost::get<std::string>(Formals[0]->getValue())));
		}
	}
	return returner;
}


MemCell *Vm::ExecuteSqrt(){
	MemCell *returner = NULL;
	if(Formals.size() == 0){
		std::cout << "Error not enought arguments for sqrt";
		executionFinished = true;
	}
	else{
		if(Formals[0]->getType()!=MemCellType::num_m){
			std::cout << "WARNIGN sqrt parameter is not number \n";
			executionFinished = true;
		}
		else if(boost::get<double>(Formals[0]->getValue())<0){
			std::cout << "WARNING sqrt parameter less than 0 \n";
			executionFinished = true;
		}
		else{
			returner = new MemCell(sqrt(boost::get<double>(Formals[0]->getValue())));
		}
	}
	return returner;
}

MemCell *Vm::ExecuteCos(){
	MemCell *returner = NULL;
	if(Formals.size() == 0){
		std::cout << "Error not enought arguments for cos";
		executionFinished = true;
	}
	else{
		if(Formals[0]->getType()!=MemCellType::num_m){
			std::cout << "WARNIGN sqrt parameter is not number \n";
		}
		else{
			returner = new MemCell(cos(boost::get<double>(Formals[0]->getValue()))*PI/180);
		}
	}
	return returner;
}

MemCell *Vm::ExecuteSin(){
	MemCell *returner = NULL;
	if(Formals.size() == 0){
		std::cout << "Error not enought arguments for sin";
		executionFinished = true;
	}
	else{
		if(Formals[0]->getType()!=MemCellType::num_m){
			std::cout << "WARNIGN sqrt parameter is not number \n";
			executionFinished = true;
		}
		else{
			returner = new MemCell(cos(boost::get<double>(Formals[0]->getValue()))*PI/180);
		}
	}
	return returner;
}

void Vm::AddToElements(std::string stringer,MemCell *cell){
	arraysPointer.push_back(cell);
	arrayStrings.push_back(stringer);
}

void Vm::CombineElements(){
	if(arraysPointer.size()<=1){return;}
	MemCell *prev = arraysPointer[arraysPointer.size()-1];
	MemCell *curr;
	for(int i=arraysPointer.size()-2;i>0;i--){
		curr = new MemCell(MemCellType::table_m);
		curr->add(arrayStrings[i],prev);
		curr->addAll(arraysPointer[i]);
		curr = prev;
		if(ALPHA_DEBUG == 1)std::cout << "did it happen?? \n";
	}
	curr = new MemCell(MemCellType::table_m);
	curr->add(arrayStrings[0],prev);
	curr->addAll(arraysPointer[0]);
	GetCell(first)->setMemCell(curr->getArrays());
	arraysPointer.clear();
}

void Vm::NullifyElements(){
	arraysPointer.clear();
}
