#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <regex>
#include <boost/algorithm/string.hpp>

#include "BcParser.hh"
#include "Vm.hh"

namespace alpha {
		void BcParser::parseInstructions(std::ifstream& file, std::vector<Instruction *>& instrArray, std::vector<FunctionData *>& funcArray) {
			std::regex instrRe("^(\\d+):(\\d+)(?: ((?:\\d+;\\d+ ?){1,3}))?$");
			std::smatch instrMa;
			std::string instrBuf, token;
			Instruction *instr;
			size_t funcIndex = 0;

			while(std::getline(file, instrBuf, std::cin.widen('\n'))) {
				std::regex_match(instrBuf, instrMa, instrRe);
				if(instrMa.size() == 4) {
					Instruction *instr = new Instruction((OpCode) stol(instrMa[2]));
					std::istringstream iss(instrMa[3]);

					// Set function instruction index if applicable
					if(instr->getOpCode() == OpCode::funcenter) {
						funcArray[funcIndex++]->setInstrIndex(stol(instrMa[1]));
					}

					if(iss >> token) instr->setArg1((VmArgType) stol(token.substr(0, token.find(";"))), stol(token.substr(token.find(";")+1, std::string::npos)));
					if(iss >> token) instr->setArg2((VmArgType) stol(token.substr(0, token.find(";"))), stol(token.substr(token.find(";")+1, std::string::npos)));
					if(iss >> token) instr->setArg3((VmArgType) stol(token.substr(0, token.find(";"))), stol(token.substr(token.find(";")+1, std::string::npos)));

					instrArray.push_back(instr);
				} else {
					std::cerr << "Malformed instruction \"" << instrBuf << "\" " << std::endl;
				}
			}
		}

	ProgramData* BcParser::parseFile(std::ifstream& prgFile) {
		std::vector<Instruction *> instrArray;
		std::vector<MemCell *> stack;
		std::vector<stackFunction *> sFunction;
		std::vector<double> doubleArray;
		std::vector<std::string> stringArray;
		std::vector<MemCell *> globalsArray;
		std::vector<FunctionData *> functionArray;

		long globCount, funcCount;
		std::string buffer;
		std::string tmp;

		// Read global count
		std::getline(prgFile, buffer, std::cin.widen('\n'));
		globCount = stol(buffer);

		// Read strings
		std::getline(prgFile, buffer, std::cin.widen('\n'));
		std::istringstream iss(buffer);
		while(iss >> tmp) {
			if(tmp[0] == '\"') {
				if(tmp.back() == '\"') {
					stringArray.push_back(tmp.substr(1,tmp.length()-2));
					boost::replace_all(stringArray.back(), "\\n", "\n");
					boost::replace_all(stringArray.back(), "\\t", "\t");
					boost::replace_all(stringArray.back(), "\\s", " ");
				} else
					stringArray.push_back(tmp.substr(1));
			} else {
				if(tmp.back() == '\"') {
					stringArray.back() = stringArray.back() + " " + tmp.substr(0,tmp.length()-1);
					boost::replace_all(stringArray.back(), "\\n", "\n");
					boost::replace_all(stringArray.back(), "\\t", "\t");
				} else
					stringArray.back() = stringArray.back() + " " + tmp;
			}
		}

		// Skip Ints
		std::getline(prgFile, buffer, std::cin.widen('\n'));

		// Read numbers
		std::getline(prgFile, buffer, std::cin.widen('\n'));
		iss.clear();
		iss.str(buffer);
		while(iss >> tmp) {
			doubleArray.push_back(stod(tmp));
		}

		// Read function count
		std::getline(prgFile, buffer, std::cin.widen('\n'));
		funcCount = stol(buffer);
		for(size_t i=0; i<funcCount; i++) functionArray.push_back(new FunctionData());

		size_t i;
		// Read local count for each function
		std::getline(prgFile, buffer, std::cin.widen('\n'));
		iss.clear();
		iss.str(buffer);
		i=0;
		while(iss >> tmp) {
			functionArray[i++]->setLocalCount(stol(tmp));
		}

		// Read formal count for each function
		std::getline(prgFile, buffer, std::cin.widen('\n'));
		iss.clear();
		iss.str(buffer);
		i=0;
		while(iss >> tmp) {
			functionArray[i++]->setFormalCount(stol(tmp));
		}

		// Parse instructions
		parseInstructions(prgFile, instrArray, functionArray);

		return new ProgramData(instrArray, doubleArray, stringArray, globCount, functionArray);
	}
}
