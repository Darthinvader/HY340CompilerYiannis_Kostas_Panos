#include <iostream>
#include <fstream>
#include <string>
#include <vector>

//#include "Instruction.hh"
#include "MemCell.hh"
#include "Vm.hh"
#include "Instruction.hh"
#include "BcParser.hh"
using namespace alpha;

int main(int argc, char *argv[]){
	if(argc != 2) {
		std::cerr << "Invalid arguments supplied" << std::endl;
		return 1;
	}
	std::ifstream inputFile;
	inputFile.open(argv[1]);
	if(!inputFile.is_open()) {
		std::cerr << "Could not open file " << argv[1] << std::endl;
		return 1;
	}

	ProgramData *prg;
	prg = BcParser::parseFile(inputFile);

	Vm *myVm = new Vm(prg);
	myVm->MoveInstrIndex();

	inputFile.close();
	return 0;
}
