//todo Opcode.cc
//temp: front slide 29
#ifndef MEMCELL_H
#define MEMCELL_H

#include <string>
#include <iostream>
#include <map>
#include <boost/variant.hpp>

namespace alpha {
	enum class MemCellType {
		num_m, str_m, bool_m,
		table_m,
		func_m, libfunc_m,
		nil_m, undef_m
	};
	class MemCell{
		public:
			typedef std::map <std::string,MemCell *> ArrayObject;
			MemCellType getType();
			std::string getTypeStr();
			boost::variant<double, bool, std::string,long> getValue();
			void setMemCell(std::string strVal);
			void setMemCell(double numVal);
			void setMemCell(bool boolVal);
			void setMemCell(long numVal);
			void setMemCell(MemCellType type);
			void setMemCell(MemCellType type,long numVal);
			void setMemCell(boost::variant<double, bool, std::string,long> value);
			void add(std::string String,MemCell *cell);
			double getDoubleCell();
			std::string getStringCell();
			void setMemCell(ArrayObject array);
			void addAll(MemCell *cell);
			long getLongCell();
			ArrayObject getArrays();
			void printer();
			int keyAmmount();
			std::string libFuncName(int index);
			MemCell *CloneKeys();
			MemCell* clone();
			MemCell* shallowClone();
			bool operator==(MemCell &other);
			MemCell& operator=(MemCell &other);
			double strtonum();
			MemCell(std::string strVal);
			MemCell(MemCellType type);
			MemCell(MemCellType type,long numVal);
			MemCell(long numVal);
			MemCell(double numVal);
			MemCell(bool boolVal);
			~MemCell();
		private:
			boost::variant<double, bool, std::string,long> value;
			MemCellType type;
			ArrayObject Arrays;
			int ArIndex;
	};

};

#endif
