#ifndef VM_H
#define VM_H

#include<string>
#include<vector>

#include "Instruction.hh"
#include "MemCell.hh"

namespace alpha {
	class stackFunction{
		public:
			long getIndex();
			long getFormalCount();
			long getLocalCount();
			void setIndex(long index);
			void setFormalCount(long formals);
			void setLocalCount(long locals);
			stackFunction(long index);
		private :
			long index;
			long formalCount;
			long localCount;
	};
	class FunctionData {
		public:
			long getIndex();
			long getInstrIndex();
			long getFormalCount();
			long getLocalCount();
			void setIndex(long index);
			void setInstrIndex(long index);
			void setFormalCount(long count);
			void setLocalCount(long count);
			FunctionData(long index, long instrIndex, long formalCount, long localCount);
			FunctionData();

		private:
			long index;
			long instrIndex;
			long formalCount;
			long localCount;
	};

	class ProgramData {
		public:
			Instruction *getInstruction(int index);
			void printInstr();
			long InstrSize();
			MemCell *getStack(int index);
			double getDouble(int index);
			std::string getString(int index);
			MemCell *getGlobal(int index);
			FunctionData *getFunction(int index);
			MemCell *getFormal(int formalIndex);
			MemCell *getLocal(int localIndex);
			stackFunction *getStackFunction(int index);
			int getNumOfArgs();
			void insertMemCellsToStack(std::vector<MemCell *> Flocals,int FuncIndex);
			MemCell *getLastFormal(int formalIndex);
			ProgramData(std::vector<Instruction *> instructions,std::vector<double> doubleArray,std::vector<std::string> stringArray,int globals, std::vector<FunctionData *> functionArray);
			void cleanLastStack();
			void printalldoubles();
			std::vector<MemCell *> ReverseFormals(std::vector<MemCell *> Flocals);
		private:
			std::vector<Instruction *> instructions;
			std::vector<MemCell *> stack;
			std::vector<stackFunction *> sFunction;
			std::vector<double> doubleArray;
			std::vector<std::string> stringArray;
			std::vector<MemCell *> globalsArray;
			std::vector<FunctionData *> functionArray;
	};

  class Vm{
    public:
	  Vm(ProgramData *Everything);
	  ~Vm();
	  void LoadEverything();//This will house the programm data loading
	  void MoveInstrIndex();
	  void fetchNextInstuction();
	  void AnalyzeInstruction();
	  void ExecuteAssign();
	  void ExecuteAdd();
	  void ExecuteSub();
	  void ExecuteMul();
	  void ExecuteDiv();
	  void ExecuteMod();
	  void ExecuteJeq();
	  void ExecuteJne();
	  void ExecuteJle();
	  void ExecuteJgt();
	  void ExecuteJge();
	  void ExecuteJlt();
	  void ExecuteJump();
	  void ExecuteCall();
	  void ExecutePusharg();
	  void ExecuteFuncenter();
	  void ExecuteFuncexit();
	  void ExecuteNewtable();
	  void ExecuteTablegetelem();
	  void ExecuteTablesetelem();
	  void ExecutePrint();
	  MemCell* ExecuteInput();
	  bool Jeq();
	  bool Jle();
	  bool Jge();
    bool toBool(MemCell *var);
    void throwError(std::string message, bool execFlag);
	  ProgramData *getEverything();
	  MemCell* objectMemberKeys();
	  MemCell *objectTotalMembers();
	  MemCell *objectCopy();
	  MemCell *GetCellCopy(Vmarg *arg);
	  MemCell *GetCell(Vmarg *arg);
	  MemCell *argument();
	  MemCell *totalArguments();
	  MemCell *typeOf();
	  MemCell *strtonum();
	  MemCell *ExecuteSqrt();
	  MemCell *ExecuteCos();
	  MemCell *ExecuteSin();
	  MemCell * GetArrayCopy(MemCell *cell);
	  void AddToElements(std::string stringer,MemCell *cell);
	  void CombineElements();
	  void NullifyElements();
    private:
    	int line;
   		ProgramData *Everything;
	  	Instruction *currInstr;
	  	std::vector<int> currInstrIndex;
	  	std::vector<MemCell *> currFunction;
	  	OpCode currOpcode;
	  	std::vector<MemCell*> returnedValue;
	  	std::vector<MemCell *> Formals;
	  	int boolshit;
	  	bool executionFinished;
	  	Vmarg *first;
	  	std::vector<MemCell *> arraysPointer;
	  	std::vector<std::string> arrayStrings;
	};
};
#endif
