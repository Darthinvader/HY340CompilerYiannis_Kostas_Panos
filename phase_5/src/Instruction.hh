#ifndef INSTR_H
#define INSTR_H

namespace alpha {
	enum OpCode {//make enum to int a thing
		assign,
		add, sub,
		mul, divd, mod,
		jeq, jne, jle, jge, jlt, jgt, ju,
		call, pusharg, funcenter, funcexit,
		newtable, tablegetelem, tablesetelem,
		nop
	};
	//enum classes do not count numbers from 0 they have their own system
	//thats why i'm removing enum class opcode for enum opcode
	/*
	 * We are not differentiating between
	 * user and library functions.
	 */
	enum class VmArgType {
		global_a,
		string_a,int_a,double_a,bool_a,
		func_a,local_a,formal_a,
		retval_a,nil_a,label_a
	};
	class Vmarg{
		public:
			void printer();
			Vmarg(VmArgType type,long value);
			~Vmarg();
			VmArgType getType();
			long getValue();
			void setType(VmArgType type);
			void setValue(long value);
		private:
			VmArgType type;
			long value;
	};
/*
	struct VmArg {
		VmArgType type;
		long value;
	};//we are still uougkani kai grafoume c?
*/
	class Instruction {
		public:
			void printer();
			void setOpCode(OpCode opcode);
			void setArg1(VmArgType type,long value);
			void setArg2(VmArgType type,long value);
			void setArg3(VmArgType type,long value);
			void setLabel(VmArgType type,long value);
			OpCode getOpCode();
			Vmarg *getArg1();
			Vmarg *getArg2();
			Vmarg *getArg3();
			Vmarg *getLabel();
			Instruction();
			Instruction(OpCode opcode);
			~Instruction();
		private:
			OpCode opcode;
			Vmarg *arg1;
			Vmarg *arg2;
			Vmarg *arg3;
			Vmarg *label;
			int o1;
			int a1;
			int a2;
			int a3;
			int l1;
	};
};
#endif

