#include "Instruction.hh"
#include <iostream>
using namespace alpha;

Vmarg::Vmarg(VmArgType type,long value){
	this->type = type;
	this->value = value;
}
VmArgType Vmarg::getType(){
	return type;
}
long Vmarg::getValue(){
	return value;
}
void Vmarg::setType(VmArgType type){
	this->type = type;
}
void Vmarg::setValue(long value){
	this->value = value;
}

void Vmarg::printer(){
	std::cout << (int)type << ";" << value;
}
void Instruction::setOpCode(OpCode opcode){
	this->opcode =opcode;
};
OpCode Instruction::getOpCode(){
	return opcode;
};

Instruction::Instruction(){
	opcode = (OpCode)0;
	o1 =0;
	a1 =0;
	a2=0;
	a3=0;
	l1=0;
}

Instruction::Instruction(OpCode opcode){
	this->opcode = opcode;
	o1 =1;
	a1 =0;
	a2=0;
	a3=0;
	l1=0;

}

void Instruction::setArg1(VmArgType type,long value){
	this->arg1 = new Vmarg(type,value);
	a1 =1;
}
void Instruction::setArg2(VmArgType type,long value){
	this->arg2 = new Vmarg(type,value);
	a2=1;
}
void Instruction::setArg3(VmArgType type,long value){
	this->arg3 = new Vmarg(type,value);
	a3 = 1;
}
void Instruction::setLabel(VmArgType type,long value){
	this->label = new Vmarg(type,value);
	l1 =1;
}
Vmarg *Instruction::getArg1(){
	return arg1;
}
Vmarg *Instruction::getArg2(){
	return arg2;
}
Vmarg *Instruction::getArg3(){
	return arg3;
}
Vmarg *Instruction::getLabel(){
	return label;
}
void Instruction::printer(){
	if(a1==1){
		arg1->printer();
	}
	if(a2==1){
		arg2->printer();
	}
	if(a3==1){
		arg3->printer();
	}
	if(l1==1){
		label->printer();
	}
}
