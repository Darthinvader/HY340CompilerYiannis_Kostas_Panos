%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRBUF_INITSZ 32

enum yytokentype{
	IF,ELSE,WHILE,FOR,FUNC,RT,BRK,CNT,AND,NOT,OR,LC,T,F,NIL,INT,REAL,ID,OPBRACE,CLBRACE,OPBRACKET,
	CLBRACKET,OPPAR,CLPAR,SEMCOL,COMMA,COLON,GLBOP,DOT,DDOT,ASS,PLUS,MIN,MUL,DIV,MOD,EQ,NEQ,INC,DEC,
	GT,LT,GTEQ,LTEQ,STR,INVAL
};

int numberOfToken = 0;
char *str_buf;
size_t str_sz;
uint8_t str_valid;

struct alpha_token_t{           //this struct has 4 field:
	int line;                   //the line in was in:
	enum yytokentype tokenType; //the tokentype (see enum below)
	char fullyWrittenType[25];  //the written type fully
	char *tokenString;          //the token itself written in plain text
};

void multi_line_comment(void);
struct alpha_token_t makeToken(int line,enum yytokentype tokenType,char *fullyWrittenType,char *tokenString, size_t tokenSize);

static inline void add_to_buf(char c) {
	if(((str_sz+1) % STRBUF_INITSZ == 1) && ((str_sz+1) / STRBUF_INITSZ > 0)){
		// double str_buf size as needed
		str_buf = realloc(str_buf, STRBUF_INITSZ*(1+(str_sz / STRBUF_INITSZ)));
	}
	str_buf[str_sz++] = c;
	return;
}

%}

integer [0-9]+
/* finds integers (1,2,4,32 etc.) [0-9] means it find one number between 0-9 the plus on the end means it finds a continuous string of 0-9 numbers(eg. 1245642343223414) */
real    {integer}\.{integer}
/* finds a real number (3.14159) */
id      [a-zA-Z][a-zA-Z_0-9]*
/* finds a string that contains as the first character a letter and all the other numbers and letter */
space   [\r \t\v\n]
/* finds all spaces and tabs and newlines */
/* alpha_yytext is the current text */
/* alpha_yyleng is the current text length */
/* alpha_yylineno is the currnt line number */
%option prefix="alpha_yy"
%option yylineno
%option noyywrap

%x string

%%

if       {return IF;}
else     {return ELSE;}
while    {return WHILE;}
for      {return FOR;}
function {return FUNC;}
return   {return RT;}
break    {return BRK;}
continue {return CNT;}
and      {return AND;}
not      {return NOT;}
or       {return OR;}
local    {return LC;}
true     {return T;}
false    {return F;}
nil      {return NIL;}

"="  {return ASS;}
"+"  {return PLUS;}
"-"  {return MIN;}
"*"  {return MUL;}
"/"  {return DIV;}
"%"  {return MOD;}
"==" {return EQ;}
"!=" {return NEQ;}
"++" {return INC;}
"--" {return DEC;}
">=" {return GTEQ;}
">"  {return GT;}
"<=" {return LTEQ;}
"<"  {return LT;}

"{"  {return OPBRACE;}
"}"  {return CLBRACE;}
"["  {return OPBRACKET;}
"]"  {return CLBRACKET;}
"("  {return OPPAR;}
")"  {return CLPAR;}
";"  {return SEMCOL;}
","  {return COMMA;}
"::" {return GLBOP;}
":"  {return COLON;}
".." {return DDOT;}
"."  {return DOT;}

{integer} {return INT;}
{real}    {return REAL;}
{id}      {return ID;}
{space}   {}

"\"" {
	str_buf = malloc(STRBUF_INITSZ*sizeof(char));
	str_sz = 0;
	str_valid = 1;
	BEGIN(string);
}

<string>{
	"\"" {
		add_to_buf('\0');
		BEGIN(INITIAL);
		return STR;
	}

	"\n" {
		fprintf(stderr, "al: found newline before end of string\n");
		add_to_buf('\0');
		BEGIN(INITIAL);
		str_valid = 0;
		return STR;
	}

	"\\\"" {
		add_to_buf('\"');
	}

	"\\n" {
		add_to_buf('\n');
	}

	"\\\\" {
		add_to_buf('\\');
	}

	"\\t" {
		add_to_buf('\t');
	}

	"\\". {
		fprintf(stderr, "al: warning: illegal escape sequence \\%c\n", yytext[1]);
		add_to_buf('\\');
		add_to_buf(yytext[1]);
		str_valid = 0;
	}

	[^\\\n\"]+ {
		char *yptr = yytext;
		while(*yptr)
			add_to_buf(*yptr++);
	}
}

"/*" { multi_line_comment(); return -2; }
"//" {
	char current_char;
	while((current_char = input()) != EOF && (current_char != '\n')) { ; };
	return -2;
}

<<EOF>> { return -1;}

. {return INVAL;}

%%

struct alpha_token_t tokenScanner(){
	int calle = alpha_yylex();
	numberOfToken++;
	/*IF,ELSE,WHILE,FOR,FUNC,RT,BRK,CNT,AND,NOT,OR,LC,T,F,NIL,INT,REAL,ID,OPBRACE,CLBRACE,OPBRACKET,
	CLBRACKET,OPPAR,CLPAR,SEMCOL,COMMA,COLON,GLBOP,DOT,DDOT,ASS,PLUS,MIN,MUL,DIV,MOD,EQ,NEQ,INC,DEC,
	GT,LT,GTEQ,LTEQ,STR*/

	if(calle == -2){
		return makeToken(alpha_yylineno, -2, "COMMENT", "Comment was here", 18);
	}
	if(calle == -1){
		return makeToken(-1, -1, "EOF", "EOF", 4);
	}
	if (calle >= IF && calle < NIL){
		return makeToken(alpha_yylineno, calle, "KEYWORD", alpha_yytext, alpha_yyleng);
	}
	if(calle == INT){
		return makeToken(alpha_yylineno, calle, "INTEGER", alpha_yytext, alpha_yyleng);
	}
	if(calle == REAL){
		return makeToken(alpha_yylineno, calle, "REAL", alpha_yytext, alpha_yyleng);
	}
	if(calle == ID){
		return makeToken(alpha_yylineno, calle, "ID", alpha_yytext, alpha_yyleng);
	}
	if(calle >= OPBRACE && calle <= CLPAR){
		return  makeToken(alpha_yylineno, calle, "PUNCTUATIONMARK", alpha_yytext, alpha_yyleng);
	}
	if(calle >= SEMCOL && calle <= LTEQ){
		return makeToken(alpha_yylineno, calle, "OPERATOR", alpha_yytext, alpha_yyleng);
	}
	if(calle == STR){
		struct alpha_token_t token;
		if(str_valid) {
			token = makeToken(alpha_yylineno, calle, "STRING", str_buf, str_sz);
		} else{
			token = makeToken(alpha_yylineno, calle, "INVAL_STRING", str_buf, str_sz);
		}
		free(str_buf);
		return token;
	}
	if(calle == INVAL){
		return makeToken(alpha_yylineno, calle, "INVALID", alpha_yytext, alpha_yyleng);
	}
	fprintf(stderr, "al: Unknown or no token found.\n");
	return makeToken(0,0,"ERROR", "", 1);
}

struct alpha_token_t makeToken(int line, enum yytokentype tokenType, char *fullyWrittenType, char *tokenString, size_t tokenSize){
	struct alpha_token_t token;
	token.tokenString = malloc((tokenSize)*sizeof(char));
	strcpy(token.tokenString, tokenString);
	token.line = line;
	token.tokenType = tokenType;
	strcpy(token.fullyWrittenType, fullyWrittenType);
	return token;
}

void multi_line_comment(void){
	int current_char;
	int prev_char;
	int commentDepth = 1;
	int start_line = yylineno;
	prev_char = 'a';
    /* init */
	while(((current_char = input()) != EOF) && commentDepth > 0){
		if(prev_char == '/' && current_char == '*'){
			commentDepth++;
			prev_char = 'a';	/* avoid '/' from being used again */
		}
		else if(prev_char == '*' && current_char == '/'){
			commentDepth--;
			prev_char = 'a';	/* avoid '*' from being used again */
		}
		else{
			prev_char = current_char;
			continue;
		}
	}

	if(commentDepth > 0){
		fprintf(stderr, "l%d error: reached EOF without finding equal nummber of closing comment symbol(s)\n", start_line);
		exit(-1);
	}
}
    /* maybe return the multiline comment who knows */

int main(int argc, char *argv[]){

	if (argc == 2){
		yyin = fopen(argv[1], "r");
		if(yyin == NULL) {
			perror("al: ");
			return EXIT_FAILURE;
		}
	}
	else if(argc > 2){
		fprintf(stderr, "Usage:\n\t%s filename\n\t%s", argv[0], argv[0]);
		return EXIT_SUCCESS;
	}
	else{
		yyin = stdin;
	}
	struct alpha_token_t token;
	token = tokenScanner();
	while(token.tokenType != -1){
		printf("%i %d \"%s\" %s \n", token.line, numberOfToken, token.tokenString, token.fullyWrittenType);
		//output = (char *)realloc(output,outputSize*sizeof(char)+1);
		//sprintf(output,"%i %d \"%s\" %s \n", token.line , numberOfToken , token.tokenString , token.fullyWrittenType);
		//fwrite(output,1,outputSize,fp);
		//fputs(output,fp); //activate this only to print in file(might cause segmentations not fully test)
		token = tokenScanner();
	}
	if (argc == 2){
		if(fclose(yyin) != 0) {
			perror("al: ");
			return EXIT_FAILURE;
		}
	}

	return 0;
}
